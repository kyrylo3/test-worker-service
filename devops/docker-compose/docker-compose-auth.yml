# Example docker compose file to run a service and its dependencies with authentication and
# authorization enabled.
# Prerequisite - Secrets must have been setup for all services by following instructions in
# https://confluence.eng.vmware.com/display/ATLAS/Secrets+Management+Guide#SecretsManagementGuide-SecretsForTestingEnvironments
#
# Value for variables are loaded from .env file but can be overridden by setting up identical shell environment variables
# before running docker-compose.
version: '2.3'

services:

  test-worker-service:
    # "docker-compose up --build" will build service image using local changes and tag it with specified image tag
    # "docker-compose pull" followed by "docker-compose up --no-build" will skip build and start service with specified image tag
    build: ../../server
    image: skyscraper-docker-local.artifactory.eng.vmware.com/atlas/prototype/starter-testing/test-worker-service:${SERVICE_TAG}
    ports:
      - "9080:9080"
    user: "1000"
    volumes:
      # Mount host's jacoco results directory into the container for collecting coverage data
      - ../../functional-tests/build/jacoco:/tmp/jacoco-results
    environment:
      - SERVER_PORT=9080
      - SPRING_PROFILES_ACTIVE=local
      # Environment variables related to secret loading from S3 bucket. See .env
      - DISABLE_CUSTOM_SERVER_SECRETS=${DISABLE_CUSTOM_SERVER_SECRETS}
      - SECRETS_BUCKET_NAME=${SECRETS_BUCKET_NAME}
      - AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
      - AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
      - AWS_REGION=${AWS_REGION}
      - CSP_CLIENT_ID=test-worker-service-dev
      - MESSAGE_BROKER_PORT=9088
      - MESSAGE_BROKER_HOST=message-broker
      - MESSAGE_SERVICE_HOST=test-worker-service
      - ATLAS_MESSAGES_AUTH_MODE=SERVICE
      - ATLAS_DATA_URL=http://data-fabric-service:9090
      - ATLAS_DATA_ENABLE_SCHEMA_INIT=true
      - ATLAS_DATA_AUTH_ENABLED=true
      - DISABLE_AUTHZ=false
      - PERMIT_ALL_PATHS=/management/**,/api/noauth/sample-resources/**
      - ATLAS_AUTHZ_SERVICE_URL=http://policy-service:9094
      - ATLAS_ORCHESTRATION_SCHEDULER_HOST=scheduler-service
# set the Authentication mode to SERVICE for communication between executor-client and scheduler-service using Service Token
      - ATLAS_ORCHESTRATION_AUTH_MODE=SERVICE
      # Collect code coverage data for functional tests
      - ENABLE_CODE_COVERAGE=true
      - COVERAGE_RESULT_DIR=/tmp/jacoco-results
      - COVERAGE_DATA_FILE=ft.exec
    depends_on:
      message-broker:
        condition: service_healthy
      data-fabric-service:
        condition: service_healthy
      scheduler-service:
        condition: service_healthy
      policy-service:
        condition: service_healthy


  message-broker:
    image: skyscraper-docker-local.artifactory.eng.vmware.com/atlas/message-fabric/message-broker:${MESSAGE_BROKER_TAG}
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:9088/management/readiness"]
      start_period: 30s
      interval: 10s
      timeout: 5s
      retries: 30
    environment:
      - SERVER_PORT=9088
      - SPRING_PROFILES_ACTIVE=local
      # Environment variables related to secret loading from S3 bucket. See .env
      - DISABLE_CUSTOM_SERVER_SECRETS=${DISABLE_CUSTOM_SERVER_SECRETS}
      - SECRETS_BUCKET_NAME=${SECRETS_BUCKET_NAME}
      - AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
      - AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
      - CSP_CLIENT_ID=message-broker-dev
      - AWS_REGION=${AWS_REGION}
      - PERMIT_ALL_PATHS=/management/**
      - ATLAS_MESSAGES_AUTH_MODE=SERVICE
    ports:
      - "9088:9088"

  data-fabric-service:
    image: skyscraper-docker-local.artifactory.eng.vmware.com/atlas/data-fabric/data-fabric-service:${DATA_FABRIC_SERVICE_TAG}
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:9090/management/readiness"]
      start_period: 30s
      interval: 5s
      timeout: 5s
      retries: 30
    environment:
      - SERVER_PORT=9090
      - SPRING_PROFILES_ACTIVE=mariadb
      - MESSAGE_BROKER_HOST=message-broker
      - MESSAGE_BROKER_PORT=9088
      - MESSAGE_SERVICE_HOST=data-fabric-service
      - SPRING_DATASOURCE_URL=jdbc:mariadb://mariadb:3306/db
      - SPRING_DATASOURCE_USERNAME=root
      - SPRING_DATASOURCE_PASSWORD=password
      # Environment variables related to secret loading from S3 bucket. See .env
      - DISABLE_CUSTOM_SERVER_SECRETS=${DISABLE_CUSTOM_SERVER_SECRETS}
      - SECRETS_BUCKET_NAME=${SECRETS_BUCKET_NAME}
      - AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
      - AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
      - AWS_REGION=${AWS_REGION}
      - CSP_CLIENT_ID=data-fabric-service-dev
      - PERMIT_ALL_PATHS=/management/**
      # Remove MESSAGES_AUTH_MODE once datafabric moves to atlas framework 3.0.2
      - MESSAGES_AUTH_MODE=SERVICE
      - ATLAS_MESSAGES_AUTH_MODE=SERVICE
    ports:
      - "9090:9090"
    depends_on:
      message-broker:
        condition: service_healthy
      mariadb:
        condition: service_healthy

  mariadb:
    image: harbor-repo.vmware.com/dockerhub-proxy-cache/library/mariadb:10.3
    container_name: mariadb
    healthcheck:
      test: [ "CMD", "mysqladmin" ,"ping", "-h", "localhost" ]
      timeout: 20s
      retries: 10
    ports:
      - "3306:3306"
    environment:
      - SERVER_PORT=3306
      - MYSQL_DATABASE=db
      - MYSQL_USER=data-fabric
      - MYSQL_PASSWORD=password
      - MYSQL_ROOT_PASSWORD=password


  scheduler-service:
    image: skyscraper-docker-local.artifactory.eng.vmware.com/atlas/orchestration-fabric/scheduler-service:${SCHEDULER_SERVICE_TAG}
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:9092/management/readiness"]
      start_period: 30s
      interval: 5s
      timeout: 5s
      retries: 30
    ports:
      - "9092:9092"
      - "8284:8284"
    environment:
      - SERVER_PORT=9092
      # Environment variables related to secret loading from S3 bucket. See .env
      - CSP_CLIENT_ID=scheduler-service-dev
      - DISABLE_CUSTOM_SERVER_SECRETS=${DISABLE_CUSTOM_SERVER_SECRETS}
      - SECRETS_BUCKET_NAME=${SECRETS_BUCKET_NAME}
      - AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
      - AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
      - AWS_REGION=${AWS_REGION}
      - SPRING_PROFILES_ACTIVE=local
      - MESSAGE_BROKER_PORT=9088
      - MESSAGE_BROKER_HOST=message-broker
      - MESSAGE_SERVICE_HOST=scheduler-service
      - ATLAS_DATA_URL=http://data-fabric-service:9090
      - JAVA_TOOL_OPTIONS="-agentlib:jdwp=transport=dt_socket,address=8284,server=y,suspend=n"
      - ATLAS_MESSAGES_AUTH_MODE=SERVICE
      - ATLAS_DATA_AUTH_MODE=SERVICE
      - ATLAS_DATA_AUTH_ENABLED=true
      - PERMIT_ALL_PATHS=/management/**
    depends_on:
      message-broker:
        condition: service_healthy
      data-fabric-service:
        condition: service_healthy


  policy-service:
    image: skyscraper-docker-local.artifactory.eng.vmware.com/atlas/service-fabric-group/policy-service:${POLICY_SERVICE_TAG}
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:9094/management/readiness"]
      start_period: 30s
      interval: 5s
      timeout: 5s
      retries: 30
    environment:
      - SERVER_PORT=9094
      - SPRING_PROFILES_ACTIVE=local
      # Environment variables related to secret loading from S3 bucket. See .env
      - DISABLE_CUSTOM_SERVER_SECRETS=${DISABLE_CUSTOM_SERVER_SECRETS}
      - SECRETS_BUCKET_NAME=${SECRETS_BUCKET_NAME}
      - AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
      - AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
      - AWS_REGION=${AWS_REGION}
      - CSP_CLIENT_ID=policy-service-dev
      - MESSAGE_BROKER_PORT=9088
      - MESSAGE_BROKER_HOST=message-broker
      - MESSAGE_SERVICE_HOST=policy-service
      - ATLAS_MESSAGES_AUTH_MODE=SERVICE
      - PERMIT_ALL_PATHS=/management/**
    ports:
      - "9094:9094"
    user: "1000"
    depends_on:
      message-broker:
        condition: service_healthy


  # Service monitor reports the ready status of other containers to help understand if all of them are running.
  # If new services are added to docker-compose, add them below for status reporting.
  # Environment variable for the new service has to start with 'MONITOR_'
  service-monitor:
    build: service-monitor
    entrypoint: [ "python3", "service-monitor.py" ]
    environment:
      - MONITOR_MESSAGE_BROKER=message-broker:9088
      - MONITOR_DATA_FABRIC=data-fabric-service:9090
      - MONITOR_SCHEDULER_SERVICE=scheduler-service:9092
      - MONITOR_POLICY_SERVICE=policy-service:9094
      - MONITOR_SERVICE=test-worker-service:9080
