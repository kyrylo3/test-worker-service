#!/usr/bin/env python3

# The script reports the ready status of other containers to help understand if all of them are running.
# For a service to be monitored, please add it to the docker-compose file.

import json
import urllib.request
import time
import os
import logging

__author__ = "VMware Inc."
__copyright__ = "Copyright 2020 VMware, Inc. All rights reserved."

LOG_FORMAT = "%(asctime)s  %(message)s"
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)
logger = logging.getLogger(__name__)

NOT_READY_STATUS = "NOT_READY"
SERVICE_NAME_PREFIX = "MONITOR_"


def check_status(services):
    service_statuses = {}
    while True:
        for service in services:
            try:
                with urllib.request.urlopen(f"http://{service}/management/readiness") as url:
                    data = json.loads(url.read().decode())
                    service_statuses[service] = data["status"]
            except Exception:
                service_statuses[service] = NOT_READY_STATUS
        logger.info(format_result(service_statuses))
        time.sleep(10)


def format_result(service_statues):
    formatted_statuses = ""
    for service in service_statues:
        formatted_statuses += ("\n\t{:<40s}{:>15s}".format(service, service_statues.get(service), ))
    return formatted_statuses


def init_services():
    services_added = []
    for service in os.environ.keys():
        if service.startswith(SERVICE_NAME_PREFIX):
            services_added.append(os.environ.get(service))
    return services_added


if __name__ == "__main__":
    services = init_services()
    if services:
        check_status(services)
    else:
        logger.info("There are no configured services to be checked.")
