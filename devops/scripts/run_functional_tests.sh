#!/bin/bash -x
#
# Start service and its dependencies in docker-compose and run functional tests. It supports two modes:
# precommit: build service image using the specified tag and start all services.
# postcommit: pull down the specified service image tag from registry and start all services.
# Dependent service images are always pulled down from registry.
#
# This script should be executed from project root directory.

# Flag that determines whether functional tests are executed with or without auth.
# DISABLE_CUSTOM_SERVER_SECRETS=1
#   Secrets are not loaded from S3. Uses docker-compose.yml which has all auth capabilities turned
#   off. This is useful if you are not ready to turn on auth just yet.
# DISABLE_CUSTOM_SERVER_SECRETS=0
#   Secrets are loaded from test bucket in S3 for both tester as well as services. Uses
#   docker-compose-auth.yml to start services with auth support. This is the target state to reach.
if [[ -z "${DISABLE_CUSTOM_SERVER_SECRETS}" ]]; then
    export DISABLE_CUSTOM_SERVER_SECRETS=1
fi

# save logs from all the containers
function collect_service_logs() {
    echo "Collecting service logs"
    pushd .
    cd devops/docker-compose
    for service in $(docker-compose config --services); do
      docker-compose logs --no-color "${service}" >"${LOG_ROOT}/${service}.log" 2>&1
    done
    popd
}

# Set Default env vars required
set_default_env_vars() {
    echo "Using DISABLE_CUSTOM_SERVER_SECRETS : ${DISABLE_CUSTOM_SERVER_SECRETS}"
    if [[ "${DISABLE_CUSTOM_SERVER_SECRETS}" -ne 1 ]]; then

        # Set default SECRETS_BUCKET_NAME if the flag is unset
        if [[ -z "${SECRETS_BUCKET_NAME}" ]]; then
            export SECRETS_BUCKET_NAME=com.vmware.skyscraper.jenkins.secrets
        fi
        echo "Using S3 secrets bucket name ${SECRETS_BUCKET_NAME}"

        # Set default AWS_REGION if the flag is unset
        if [[ -z "${AWS_REGION}" ]]; then
            export AWS_REGION=us-west-2
        fi
        echo "Using AWS region ${AWS_REGION}"
    fi
}

validate_env_vars() {
    for required_var_name in "${@}"; do
        # Ref: http://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html
        if [[ -z "${!required_var_name+x}" ]] ; then
            echo >&2 "Error: Missing required env vars: ${required_var_name}"
            exit 1
        fi
    done
}

# Loaded from S3 and exported as environment variables for service to consume.
SAAS_TEST_SERVER_REQUIRED_SECRET_KEYS=(
    # Loaded from saas-test-fw-secrets-us-west-2-dev
    "SAAS_TEST_FW_CSP_CLIENT_REFRESH_TOKEN"
    "SAAS_TEST_FW_CSP_CLIENT_REFRESH_TOKEN_RO"
    "SAAS_TEST_FW_CSP_CLIENT_REFRESH_TOKEN_RW"
    "SAAS_TEST_FW_CSP_CLIENT_REFRESH_TOKEN_CUSTOM"
)

function main() {
    if [[ $# -lt 1 ]]; then
        echo "Usage: $0 <precommit/postcommit> [service image tag]"
        exit 1
    fi

    local test_mode=$1
    if [[ "${test_mode}" != "precommit" && "${test_mode}" != "postcommit" ]]; then
        echo "Invalid test mode ${test_mode}. Allowed values: precommit, postcommit"
        exit 1
    fi

    echo "Running service integration tests"
    if ! ./gradlew -PrunIT server:integrationTest; then
        echo "Service integration tests failed"
        exit 1
    fi

    echo "Running service functional tests"

    if [[ -n "$2" ]]; then
        export SERVICE_TAG=$2
        echo "Using test-worker-service image tag ${SERVICE_TAG}"
    else
        echo "Using default image tags from docker-compose .env file"
    fi

    # Flag that determines, which functional test suit is used.
    # If none is provided - "api-smoke.xml" is used as default one.
    if [[ -z "${TEST_SUITE}" ]]; then
        export TEST_SUITE="api-smoke.xml"
    fi
    
    if [[ ! -e "functional-tests/suites/$TEST_SUITE" ]] ; then
        echo "Test suite not found, cannot run functional tests: $TEST_SUITE"
        exit 1
    fi

    # Check flag that determines, where the log files should be stored
    # If none is provided - "./functional-tests/build/logs" is used as default path.
    if [[ -z "${LOG_ROOT}" ]]; then
        rm -rf $(pwd)/functional-tests/build
        export LOG_ROOT=$(pwd)/functional-tests/build/logs
    fi
    mkdir -p "${LOG_ROOT}"

    test_result=0

    # Set Default env vars
    set_default_env_vars
    docker_compose_file=""

    if [[ "${DISABLE_CUSTOM_SERVER_SECRETS}" -eq 0 ]]; then
        echo "Loading test environment secrets from S3"

        # Remove echo to avoid printing secrets
        set +x

        if [[ -z "${AWS_ACCESS_KEY_ID}" || -z "${AWS_SECRET_ACCESS_KEY}" ]]; then
            echo "AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY for us-west-2 ply account must be set as env variables"
            echo "These are found in LastPass item 'Shared-Skyscraper/skylab-ply-secrets/SkyLab PLY - S3 Secrets RO Access Key'"
            exit 1
        fi

        # Load SaasTestFramework secrets from S3
        # Refer to [https://confluence.eng.vmware.com/display/VQE/Getting+Started+Guide]
        # for how to load custom test secrets instead of default.
        eval $(aws s3 cp s3://${SECRETS_BUCKET_NAME}/saas-test-fw-secrets-us-west-2-dev --region ${AWS_REGION} - | sed -e 's/\r$//' -e 's/^/export /') > /dev/null

        set -x

        #  Read-in different types of required secret-keys into
        #  environment variables specified in SERVER_REQUIRED_SECRET_KEYS and then
        #  validate that required secrets have all been set
        validate_env_vars "${SAAS_TEST_SERVER_REQUIRED_SECRET_KEYS[@]}"

        # use alternate compose file that enables auth for services
         docker_compose_file="-f docker-compose-auth.yml"
    else
         echo "Not loading secrets from S3. If secrets are required, they should be already set in environment"
    fi

    # Start services in docker-compose in detached mode
    compose_options="--detach"
    if [[ "${test_mode}" == "precommit" ]]; then
        # For precommit, we build the service image. This requires service fat jar to be available, so build it too
        compose_options="${compose_options} --build"
        if ! ./gradlew server:build -x test; then
            echo "Service build failed"
            exit 1
        fi
    else
        # For postcommit, use the pipeline-built image. Need to pull them down explicitly
        compose_options="${compose_options} --no-recreate"
        echo "Pulling down service images"
        (cd devops/docker-compose && docker-compose ${docker_compose_file} pull)
    fi

    echo "Starting services in docker-compose. Making jacoco result dir world writable"
    # "docker-compose up" will mount this dir in service container for code coverage. Make sure it
    # has all permissions so container can write into it
    mkdir -p functional-tests/build/jacoco && chmod -R 777 functional-tests/build/jacoco
    (cd devops/docker-compose && docker-compose ${docker_compose_file} up ${compose_options})

    # Wait for service to become ready
    retries=${MAX_RETRIES:-20}
    retry_delay=${RETRY_DELAY:-15}
    n=1
    ready=0
    until [[ ${n} -gt ${retries} ]]; do
        echo "Waiting for service to become available"
        if curl --silent http://localhost:9080/management/readiness; then
            ready=1
            break
        fi
        n=$((n + 1))
        sleep "${retry_delay}"
    done

    if [[ ${ready} -ne 1 ]]; then
        echo "Service did not become available within timeout period. Check service log in ${LOG_ROOT} for error details"
        collect_service_logs
        exit 1
    fi

    # Run functional tests and capture output to log file
    # Running test using TestNg for auth enabled mode
    if [[ "${DISABLE_CUSTOM_SERVER_SECRETS}" -eq 0 ]]; then
      echo "Starting functional tests"
      TEST_REPO_ROOT=$(pwd) ./gradlew -PrunFt -PsuiteName="${TEST_SUITE}" functional-tests:test >"${LOG_ROOT}/build.log" 2>&1
    else
      # Running test using TestNg for auth disabled mode using local spring profile for functional test application
      echo "Starting functional tests in auth disabled mode"
      TEST_REPO_ROOT=$(pwd) SPRING_PROFILES_ACTIVE=local ./gradlew -PrunFt -PsuiteName="${TEST_SUITE}" functional-tests:test >"${LOG_ROOT}/build.log" 2>&1
    fi

    test_result=$?

    if [[ ${test_result} -ne 0 ]]; then
        echo "Tests failed. Check test reports in functional-tests/build/reports/tests/test for details"
    else
        echo "Tests succeeded"
    fi

    # Stop docker-compose but don't remove yet
    # On exit, wait for jacoco code coverage data save to complete in service container
    echo "Stopping services in docker-compose"
    (cd devops/docker-compose && docker-compose ${docker_compose_file} stop --timeout 180)

    # coverage data file from container is named ft.exec to avoid getting overwritten by "gradlew functional-tests:test"
    # Rename it to standard name "test.exec" and generate report
    mv functional-tests/build/jacoco/ft.exec functional-tests/build/jacoco/test.exec
    ./gradlew functional-tests:jacocoTestReport

    collect_service_logs

    # Remove all containers
    (cd devops/docker-compose && docker-compose ${docker_compose_file} down)

    # Exit with test-run status
    exit ${test_result}
}

main "$@"
