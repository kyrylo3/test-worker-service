#!/usr/bin/env python3
"""
Script for upgrading an existing service to newer starter version
"""
from urllib import request
from urllib.error import HTTPError
from datetime import datetime
import argparse
import tempfile
import traceback
import logging
import subprocess
import json
import os
import atexit
import stat
import shutil
import filecmp
import sys
import re

__author__ = "VMware Inc."
__copyright__ = "Copyright 2020 VMware, Inc. All rights reserved."

GITLAB_HOST = "git@gitlab.eng.vmware.com"
LOG_FORMAT = "%(asctime)s %(levelname)s - %(message)s"
CMD_TIMEOUT_SECONDS = 240
STARTER_UPGRADE_BRANCH = "starter-upgrade-branch-DONT-DELETE"
STARTER_VERSION_TAG_SUFFIX = "-service-starter"
VERSION_REGEX = r"^(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)$"
MANIFEST_FILE = "devops/starter/manifest.json"
STARTER_CHANGELOG_URL = "http://go.vmware.com/atlas-starter-ver"
ONBOARDING_URL = "http://go.vmware.com/upgrade-onboard"

# Telemetry configuration
SC_URL = "https://%s.vmware.com/sc/api/collectors/%s/instances/%s/batch"
COLLECTOR_ID = "atlas_service_starter"
INSTANCE_ID = "service-starter.upgrade-script"
HOST = "scapi"

logger = logging.getLogger(__name__)
developer_branch = ""

PYTHON_MIN_MAJOR_VERSION = 3
PYTHON_MIN_MINOR_VERSION = 6
GIT_MIN_MAJOR_VERSION = 2
GIT_MIN_MINOR_VERSION = 13
GIT_MIN_PATCH_VERSION = 7


class InvalidInputError(RuntimeError):
    """Base class for user input exceptions during the upgrade."""
    pass


def parse_cmd_args():
    """
    Parse command line arguments.
    :return: args dictionary
    """
    parser = argparse.ArgumentParser(
            description="Upgrade the service to newer version of starter.")
    parser.add_argument("-v", "--starter-version", required=True, type=str, dest="starter_version",
                        help="Starter version to upgrade to. E.g., 0.6.0. See " + STARTER_CHANGELOG_URL +
                             " for valid versions and upgrade notes")
    parser.add_argument("--verbose", required=False, action="store_true", default=False, dest="verbose",
                        help="Use verbose logging")

    #### Hidden options

    # Option that is used after the script is already auto-updated to show where service-starter
    # is cloned by the previous call of the script. It is used only by the script itself.
    parser.add_argument("--starter-dir", required=False, type=str, dest="starter_dir", help=argparse.SUPPRESS)

    # Option to skip auto update
    parser.add_argument("--skip-update", required=False, action='store_true', default=False, dest="skip_update",
                        help=argparse.SUPPRESS)

    # Upgrade service to a specified starter branch instead of a released starter version. "-v" should be set to latest
    # released starter version since it's a mandatory option.
    # This option should be used for testing service upgrade to latest master prior to publishing a new starter release.
    parser.add_argument("-b", "--branch", required=False, type=str, dest="branch", help=argparse.SUPPRESS)

    # Option to skip sending telemetry data, for example by testing
    parser.add_argument("--skip-telemetry", required=False, action='store_true', default=False, dest="skip_telemetry",
                        help=argparse.SUPPRESS)

    args = parser.parse_args()

    # Initialize logging
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    else:
        logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

    return args


def exec_cmd(cmd, timeout_seconds=CMD_TIMEOUT_SECONDS, cwd=None, shell=False, ignore_error=False):
    """
    Execute a command and return its standard output.
    :param cmd: a list of arguments that form the command
    :param timeout_seconds: timeout for the command
    :param cwd: optional working directory for the command. Defaults to script's working directory
    :param shell: whether to execute in shell or not
    :param ignore_error: whether to ignore process error
    :return: standard output of command
    """
    logger.debug("Executing " + str(cmd))
    process = None
    try:
        process = subprocess.run(cmd, shell=shell, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding="utf-8",
                                 timeout=timeout_seconds, cwd=cwd)
    except subprocess.TimeoutExpired:
        logger.error("Command output: {}".format(process.stdout))
        raise RuntimeError("Cmd '{}' timed out".format(" ".join(cmd)))
    except FileNotFoundError:
        raise RuntimeError("Cmd '{}' not available in PATH".format(cmd[0]))

    if not ignore_error and process.returncode != 0:
        logger.error("Command output: {}".format(process.stdout))
        raise RuntimeError("cmd '{}' failed".format(" ".join(cmd)))

    if process.stdout:
        out = process.stdout.strip()
        logger.debug(out)
        return out
    else:
        return None


def delete_root_dir_contents():
    """
    Delete the contents of the repo root directory, excluding the devops/scripts dir (we don't want to self delete).
    :return:
    """
    # the method must be invoked from the root project dir
    # delete everything but the path 'devops/scripts' to avoid the command line error:
    # fatal: Unable to read current working directory: No such file or directory
    # create temp file that will protect 'devops/scripts' from deleting.
    open("devops/scripts/tmp_file", "a").close()
    exec_cmd(["git", "rm", "-fr", "."])
    os.unlink("devops/scripts/tmp_file")


def generate_service_from_starter(manifest_dict, target_starter_version, starter_tmp_dir):
    """
    Generate service from starter using original initialization configuration from manifest.
    :param manifest_dict: starter manifest containing original initialization configuration
    :param target_starter_version: starter version to use for generating service
    :param starter_tmp_dir: temp directory where starter repo has been cloned
    :return:
    """
    logger.info("Generating service from starter version {}".format(target_starter_version))

    # Sanitize original command line arguments
    new_args = []
    original_args = manifest_dict["command_line_arguments"]
    i = 0
    while i < len(original_args):
        # Remove '--cloned_dir' since it's only used by initialize_starter during its auto-update.
        # Remove a few others too to avoid duplication, since we'll add these later
        if original_args[i] in ["--cloned_dir", "--serviceuuid", "-d", "-b"]:
            i += 2
        # Skip these boolean arguments to avoid duplication. We'll add them later
        elif original_args[i] in ["--skip-update", "--upgrade"]:
            i += 1
        else:
            new_args.append(original_args[i])
            i += 1

    # Run initialize starter script from current directory with sanitized cmd line arguments
    initializer_script = os.path.join(starter_tmp_dir, "buildSrc", "initialize_starter.py")
    initialize_starter_cmd = [sys.executable, initializer_script]
    initialize_starter_cmd += new_args

    # Ask for upgrade in the current directory and reuse existing serviceuuid
    initialize_starter_cmd += ["-d", ".", "--upgrade", "--serviceuuid", manifest_dict["serviceuuid"]]

    # Skip auto update since we will run latest version of the script from cloned repo
    initialize_starter_cmd += ["--skip-update"]

    # Add option to not send telemetry, so that the database is not polluted by the upgrade data
    initialize_starter_cmd += ["--skip-telemetry"]

    # Clone service starter at target version tag and initialize from it
    initialize_starter_cmd += ["-b", target_starter_version]

    exec_cmd(initialize_starter_cmd, timeout_seconds=CMD_TIMEOUT_SECONDS*3)


def clone_starter(branch=None):
    """
    Clone the HEAD of starter repo to a temporary directory and return its location.
    :return:
    """
    logger.info("Cloning service-starter project")
    clone_cmd = ["git", "clone"]
    # Git clone the starter repo to temporary dir. We manually create this dir here and delete in exit handler because
    # tempfile.TemporaryDirectory() has a bug in windows - https://bugs.python.org/issue26660
    starter_tmp_dir = os.path.join(tempfile.gettempdir(), next(tempfile._get_candidate_names()))
    clone_cmd += [GITLAB_HOST +":atlas/service-starter.git", starter_tmp_dir]
    exec_cmd(clone_cmd)
    if branch:
        checkout_cmd = ["git", "checkout", branch]
        exec_cmd(checkout_cmd, cwd=starter_tmp_dir)
    return starter_tmp_dir


def version_compare(src_dict, tgt_dict):
    """
    Compare two version dictionaries containing major, minor, patch keys and their values. Values can be string typed.
    semver is not standard python module, so we implement our own semver comparison.
    :param src_dict:
    :param tgt_dict:
    :return:
    """
    # convert values from string to int
    src = {k: int(v) for k, v in src_dict.items()}
    tgt = {k: int(v) for k, v in tgt_dict.items()}
    if src == tgt:
        return 0
    elif src["major"] > tgt["major"] or \
            (src["major"] == tgt["major"] and src["minor"] > tgt["minor"]) or \
            (src["major"] == tgt["major"] and src["minor"] == tgt["minor"] and src["patch"] > tgt["patch"]):
        return 1
    else:
        return -1


def validate_target_starter_version(target_version, current_version, starter_tmp_dir):
    """
    Validate that starter upgrade target version format is correct, is greater than or equal to current version and is
    a valid version in starter repo.
    :param target_version: starter version to upgrade to
    :param current_version: starter version that a service is currently at
    :param starter_tmp_dir: temp dir where starter has been cloned
    :return:
    """
    pattern = re.compile(VERSION_REGEX)
    target_match = pattern.match(target_version)
    if not target_match:
        raise InvalidInputError("Invalid target starter version: '{}'".format(target_version))

    source_match = pattern.match(current_version)
    if not source_match:
        raise InvalidInputError("Invalid current starter version in manifest: '{}'".format(current_version))

    if version_compare(source_match.groupdict(), target_match.groupdict()) > 0:
        raise InvalidInputError(
            "Current starter version '{}' higher than that of target starter version '{}'. Cannot downgrade".format(
                current_version, target_version))

    # validate that tag with the specified version exists
    version_tag = target_version + STARTER_VERSION_TAG_SUFFIX
    out = exec_cmd(["git", "tag", "--list", version_tag], cwd=starter_tmp_dir)
    if out and version_tag in out:
        logger.info("Verified '{}' is a valid starter version".format(target_version))
    else:
        err = "'{}' is not a valid starter version. \nSee {} for valid versions".format(target_version,
                                                                                        STARTER_CHANGELOG_URL)
        raise InvalidInputError(err)


def validate_service_repo_clean_state():
    """
    Validate that current workspace is clean and prepare it for upgrade.
    :return:
    """
    logger.info("Validating and preparing local workspace")
    out = exec_cmd(["git", "status", "--porcelain"])
    if out:
        raise InvalidInputError("Local git workspace has uncommitted changes. Run the script in a clean workspace")

    branch = exec_cmd(["git", "rev-parse", "--abbrev-ref", "HEAD"])
    if branch and branch != "master":
        logger.debug("Switching to master branch")
        exec_cmd(["git", "checkout", "master"])
        exec_cmd(["git", "pull", "--rebase"])


def is_upgrade_branch_exists():
    """
    Check if the permanent upgrade branch exists in remote origin.
    :return: true if exists, false otherwise
    """
    # This will output a single ref if upgrade branch exists in remote
    out = exec_cmd(["git", "ls-remote", "--heads", "-q", "origin", STARTER_UPGRADE_BRANCH])
    return True if out and STARTER_UPGRADE_BRANCH in out else False


def delete_dir(tmp_dir):
    """
    Recursively delete specified directory. Will remove readonly files by making them writable.
    Needed on windows because autodeletion of temp directory is buggy.
    :param tmp_dir: directory to delete
    :return:
    """
    if os.path.isdir(tmp_dir):
        shutil.rmtree(tmp_dir, onerror=remove_readonly)


def remove_readonly(func, path, excinfo):
    """
    Handler to retry readonly file deletion after making it writable. Needed on Windows to delete temp directory.
    :param func:
    :param path:
    :param excinfo:
    :return:
    """
    os.chmod(path, stat.S_IWRITE)
    func(path)


def load_manifest_dict(manifest_path, post_upgrade=False):
    """
    Load service manifest created at the time of generation from starter or during previous upgrade.
    :param manifest_path: path to manifest file
    :param post_upgrade: true if manifest is being reloaded after upgrade
    :return: dictionary containing manifest properties
    """
    if not os.path.exists(MANIFEST_FILE):
        if post_upgrade:
            err = "Missing {} file. Please report a bug in Jira project AA".format(MANIFEST_FILE)
        else:
            err = "Missing {} file. Follow first time onboarding instructions in {}".format(MANIFEST_FILE,
                                                                                            ONBOARDING_URL)
        raise RuntimeError(err)

    with open(manifest_path) as json_file:
        manifest = json.load(json_file)
        # verify that required properties are available
        requiredprops = ["starter_version", "command_line_arguments", "serviceuuid"]
        for x in requiredprops:
            if not manifest.get(x):
                raise RuntimeError("Manifest {} is missing a required property {}".format(MANIFEST_FILE, x))

        return manifest


def delete_branch(branch, remote=False):
    """
    Delete specified branch locally, and optionally in origin also.
    :return:
    """
    exec_cmd(["git", "branch", "-D", branch], ignore_error=True)
    if remote:
        exec_cmd(["git", "push", "origin", "--delete", branch], ignore_error=True)


def get_developer_branch(version):
    """
    Get the name of developer branch to use for upgrade process. This is of the format
    <userid>/upgrade-to-starter-<version>.
    :param version: starter version being upgraded to
    :return: developer branch name
    """
    userid = exec_cmd(["git", "config", "user.email"])
    if userid.endswith("@vmware.com"):
        userid = userid.replace("@vmware.com", "")
    branch_name = "{}/upgrade-to-starter-{}".format(userid, version)
    logger.info("Using developer branch name {} for performing upgrade".format(branch_name))
    return branch_name


def setup_upgrade_branch(first_time):
    """
    Setup the upgrade branch to prepare for upgrade. During first time upgrade, it is created as an orphan branch.
    Subsequently it is expected to already exist.
    :param first_time: True if first time upgrade
    :return:
    """
    branch_cmd = ["git", "checkout"]

    # If first time - delete the upgrade branch locally if it exists and create it as an orphan branch
    if first_time:
        delete_branch(STARTER_UPGRADE_BRANCH)
        branch_cmd.append("--orphan")

    branch_cmd.append(STARTER_UPGRADE_BRANCH)

    # Create or checkout upgrade branch
    exec_cmd(branch_cmd)

    # rebase upgrade branch to latest from origin in case local upgrade branch is stale
    if not first_time:
        exec_cmd(["git", "pull", "--rebase"])

    # clear the project directory
    delete_root_dir_contents()


def auto_update(starter_tmp_dir):
    """
    Auto update self (the upgrade script) to latest version from master branch of starter repo and restart the upgrade
    with the original command line arguments.
    :param starter_tmp_dir: tmp dir where starter repo has already been cloned
    :return:
    """
    logger.info('Starter upgrade script {} is out of date. Automatically executing latest version.'.format(sys.argv[0]))
    new_args = sys.argv[1:]
    new_args.append("--starter-dir")
    new_args.append(starter_tmp_dir)

    upgrade_script = os.path.join(starter_tmp_dir, "devops", "scripts", "upgrade_starter.py")

    new_args.insert(0, sys.executable)
    new_args.insert(1, upgrade_script)
    out = exec_cmd(new_args, timeout_seconds=CMD_TIMEOUT_SECONDS*4)
    # Print upgrade script output on success (failure will throw and log output automatically)
    print(out)
    sys.exit()


def prepare_telemetry_data(manifest_dict, args_dict):
    """
    Prepares the telemetry data.
    :param manifest_dict: dictionary containing manifest properties
    :param args_dict: command line data during the upgrade
    :return: telemetry_data - the parsed manifest and command line data
    """
    telemetry_data = {
        '@table': 'service_starter_upgrade',
        'time_of_upgrade': datetime.now().isoformat(),
        'service_name':  manifest_dict.get("servicename", manifest_dict.get("libraryname")),
        'current_version': manifest_dict.get("starter_version"),
        'target_version':  args_dict.get("starter_version"),
        'archetype':  manifest_dict.get("archetype"),
        'features': manifest_dict.get("features"),
        'upgrade_command_line': args_dict.get("command_line_arguments"),
        'initialize_command_line': manifest_dict.get("command_line_arguments"),
    }
    return telemetry_data


def log_telemetry_exception(upgrade_ex, debug_message):
    """
    Logs a debug message in case of exception or error in sending telemetry data.
    If there is no upgrade exception, ensures that the upgrade itself was successful.
    :param upgrade_ex: The upgrade exception
    :param debug_message: The debug message to be logged
    :return: standard output of command
    """
    if upgrade_ex is None:
        logger.debug("Upgrade is successful. This is informational. {}".format(debug_message))
    else:
        logger.debug(debug_message)


def send_telemetry(telemetry_data, args_dict, exception_info):
    """
    Sends telemetry data to SuperCollider.
    :param telemetry_data: the parsed manifest and command line data
    :param args_dict: command line data during the upgrade
    :param exception_info: empty if execution is successful, otherwise will contain message and part of the stacktrace
    of the occurred exception
    """
    # If the option to skip telemetry is chosen, no data is sent
    if args_dict.get("skip_telemetry"):
        logger.debug("Skipping sending telemetry!")
        return
    try:
        # Add to the telemetry data the success status and the thrown exception, if any
        telemetry_data['successful'] = not exception_info
        if exception_info:
            telemetry_data['exception'] = exception_info

        # SC endpoint
        sc_url = SC_URL % (HOST, COLLECTOR_ID, INSTANCE_ID)

        # push the payload to Super Collider
        logger.debug("Sending telemetry to url %s" % sc_url)
        json_payload = json.dumps(telemetry_data)
        req = request.Request(sc_url, headers={"Content-Type": "application/json"}, data=bytes(json_payload, encoding="utf-8"))
        response = request.urlopen(req)
        if response.status == 200 or response.status == 201:
            logger.debug("Sent telemetry to Super Collider")
    except HTTPError as err:
        debug_message = "Failed to send telemetry to Super Collider: [%d]: %s" % (err.status, err)
        log_telemetry_exception(exception_info, debug_message)
    except Exception as e:
        debug_message = "Failed to send telemetry to Super Collider: %s" % e
        log_telemetry_exception(exception_info, debug_message)


def upgrade(manifest_dict, args_dict):
    """
    Upgrades an existing service to newer starter version
    :param manifest_dict: dictionary containing manifest properties
    :param args_dict: command line data during the upgrade
    :return: standard output of command
    """
    # Keep a copy of original command line
    args_dict["command_line_arguments"] = sys.argv.copy()
    args_dict["command_line_arguments"].pop(0)

    # Check that there are no uncommitted changes in workspace
    validate_service_repo_clean_state()

    starter_tmp_dir = ""
    starter_branch = args_dict["branch"] if "branch" in args_dict and args_dict["branch"] else None

    # This is when upgrade_starter is invoked normally
    if not args_dict["starter_dir"]:
        starter_tmp_dir = clone_starter(starter_branch)
        if args_dict["skip_update"]:
            logger.info("Skipping auto update of script")
        elif not filecmp.cmp(sys.argv[0], os.path.join(starter_tmp_dir, "devops", "scripts", "upgrade_starter.py")):
            auto_update(starter_tmp_dir)
    else:
        # This is when upgrade_starter has invoked itself after auto update
        starter_tmp_dir = args_dict["starter_dir"]

    # Setup exit handler to cleanup tmp dir only if we are in the original invocation (ie., we skipped auto update).
    # Doing this in auto updated script invocation will lead to "in use" error on Windows because it is a
    # child process of the original invocation (which is still running)
    if not args_dict["starter_dir"]:
        atexit.register(delete_dir, starter_tmp_dir)

    current_version = manifest_dict["starter_version"]
    target_version = args_dict["starter_version"]
    args_dict["old_version"] = current_version

    validate_target_starter_version(target_version, current_version, starter_tmp_dir)
    developer_branch = get_developer_branch(target_version)

    # If starter upgrade branch doesn't exist in origin, we do first time upgrade
    first_time = False

    starter_branch = args_dict["branch"] if "branch" in args_dict and args_dict["branch"] else None
    target_version_or_branch = starter_branch if starter_branch else target_version
    if not is_upgrade_branch_exists():
        first_time = True
        logger.info("Upgrading for the first time to version {}".format(target_version_or_branch))
    else:
        logger.info("Upgrading service from {} to {}".format(current_version, target_version_or_branch))

    try:
        setup_upgrade_branch(first_time)

        # Generate service in the current directory using options in the manifest
        tag_or_branch = starter_branch if starter_branch else target_version + STARTER_VERSION_TAG_SUFFIX
        generate_service_from_starter(manifest_dict, tag_or_branch, starter_tmp_dir)

        # verify that the manifest contains the new version now
        new_manifest_dict = load_manifest_dict(MANIFEST_FILE, post_upgrade=True)
        if new_manifest_dict["starter_version"] != target_version:
            raise RuntimeError("{} contains incorrect version {}. Expected {}. This is a starter bug."
                               .format(MANIFEST_FILE,new_manifest_dict["starter_version"], target_version))
    except RuntimeError:
        # switch to master and then fail
        exec_cmd(["git", "checkout", "master"], ignore_error=True)
        raise

    # Commit to upgrade branch and push it to origin
    exec_cmd(["git", "add", "-A"])
    exec_cmd(["git", "commit", "-m", "Generate service at starter version {}".format(target_version_or_branch)])
    exec_cmd(["git", "push", "-u", "origin", STARTER_UPGRADE_BRANCH])

    # Create a new developer branch from master
    delete_branch(developer_branch, remote=True)
    exec_cmd(["git", "checkout", "master"])
    exec_cmd(["git", "pull", "--rebase"])
    exec_cmd(["git", "checkout", "-b", developer_branch])

    # Merge upgrade branch to developer branch locally
    merge_cmd = ["git", "merge", STARTER_UPGRADE_BRANCH]
    if first_time:

        # use "--allow-unrelated-histories" to enable merging from an unrelated, orphan branch
        merge_cmd.append("--allow-unrelated-histories")
        merge_msg = "First time service upgrade to starter version {}".format(target_version_or_branch)
    else:
        merge_msg = "Upgrade service from starter version {} to {}".format(current_version, target_version_or_branch)

    merge_cmd += ["-m", merge_msg]

    # Ignore command error that may happen due to merge conflicts
    output = exec_cmd(merge_cmd, ignore_error=True)

    # Bug AA-9494 - if devops/feature-branch/version.yml doesn't exist, upgrade fails. This can happen if upgrade is
    # done from a version before devops/feature-branch/version.yml was added in starter.
    # The version.yml file can be changed by service dev for testing feature branch or hotfix. However, it should not be
    # changed by service dev during upgrade. Any change to this file causes feature branch precommit pipeline to be
    # triggered.
    logger.debug("Bugfix AA-9129 and AA-9494: Restoring feature-branch version.yml")
    staged_files = exec_cmd(["git", "diff", "--name-only", "--cached"])
    if staged_files and "devops/feature-branch/version.yml" in staged_files:
        logger.info("The devops/feature-branch/version.yml should not be updated during upgrade. Unstaging the file.")
        exec_cmd(["git", "restore", "--staged", "devops/feature-branch/version.yml"])
        exec_cmd(["git", "restore", "devops/feature-branch/version.yml"])
        exec_cmd(["git", "status"])

    # This prints any merge conflict messages
    print(output)

    print(
        '''
        Service upgraded to starter version {} in branch {}.
    
        Follow these steps to move forward:
          1. Resolve any merge conflicts carefully
          2. Test the changes thoroughly and fix any issues
          3. Finish merge ("git add ." followed by "git commit")
          4. git push origin {}
          5. Create an MR to merge branch {} to master. ** DO NOT SQUASH COMMITS **
        '''.format(target_version_or_branch, developer_branch, developer_branch, developer_branch)
    )

    if first_time:
        print(
            '''
        Follow these steps if you want to abandon the first time upgrade:
          1. git merge --abort
          2. Then execute:
              git push origin --delete {}
              git checkout master
              git branch -D {} {}
        '''.format(STARTER_UPGRADE_BRANCH, STARTER_UPGRADE_BRANCH, developer_branch)
    )
    else:
        print(
            '''
        Follow these steps if you want to abandon the upgrade:
          1. If there are merge conflicts execute:
              git merge --abort
          2. Then execute:
              git checkout {}
              git reset --hard HEAD~1
              git push -f origin {}
              git checkout master
              git branch -D {}
            '''.format(STARTER_UPGRADE_BRANCH, STARTER_UPGRADE_BRANCH, developer_branch)
            )


def ask_confirmation(current_version, target_version):
    """
    Ask user confirmation about changelog and upgrade assessment
    :param current_version: current starter version
    :param target_version: target starter version
    :return:
    """
    msg = f"""Please review changes and upgrade notes for all versions between {current_version} and {target_version} at {STARTER_CHANGELOG_URL}. 
Confirm that you are prepared to apply the changes, including any manual steps as required by upgrade notes. [y/n] """
    answer = input(msg)
    if answer not in ["y", "Y", "YES", "Yes", "yes"]:
        sys.exit()


def check_python():
    """
    Verify if current python version is >= 3.6
    :return:
    """
    python_version = sys.version  # e.g.: '3.8.5 (default, Sep  3 2020, 21:29:08) [MSC v.1916 64 bit (AMD64)]'
    logger.debug("check_python: current version is {}".format(python_version))

    matcher = re.match(r"(\d+)\.(\d+).(\d+)\s+.*", python_version)

    if not matcher:
        logger.warning("check_python: Unable to parse python version. "
                       "Python must be >= {}.{} otherwise starter won't work. Current version is: {}".format(
                       PYTHON_MIN_MAJOR_VERSION, PYTHON_MIN_MINOR_VERSION, python_version))

        return

    target_version = {"major": PYTHON_MIN_MAJOR_VERSION, "minor": PYTHON_MIN_MINOR_VERSION, "patch": 0}
    src_version = {"major": int(matcher.group(1)), "minor": int(matcher.group(2)), "patch": int(matcher.group(3))}

    logger.debug("check_python:  target_version->{}".format(target_version))
    logger.debug("check_python:  src_version->{}".format(src_version))

    if version_compare(src_version, target_version) < 0:
        error_str = "Python version {major}.{minor} or higher must be used. Current version is: {current}".format(
            major=PYTHON_MIN_MAJOR_VERSION, minor=PYTHON_MIN_MINOR_VERSION, current=python_version)

        raise RuntimeError(error_str)

    logger.info("check_python: OK")


def check_git():
    """
    Verify if current git version is >= 2.13.7
    :return:
    """
    git_version = exec_cmd(["git", "--version"])  # e.g.: 'git version 2.29.2.windows.2'

    matcher = re.match(r"git version (\d+)\.(\d+).(\d+).*", git_version)

    if not matcher:
        logger.warning("check_git: Unable to parse git version. "
                       "Git version must be >= {}.{}.{} otherwise starter won't work. Current version is: {}".format(
                       GIT_MIN_MAJOR_VERSION, GIT_MIN_MINOR_VERSION, GIT_MIN_PATCH_VERSION, git_version))
        return

    target_version = {"major": GIT_MIN_MAJOR_VERSION, "minor": GIT_MIN_MINOR_VERSION, "patch": GIT_MIN_PATCH_VERSION}
    src_version = {"major": int(matcher.group(1)), "minor": int(matcher.group(2)), "patch": int(matcher.group(3))}

    if version_compare(src_version, target_version) < 0:
        raise RuntimeError("Git version must be >= {major}.{minor}.{build}. Current version is: {current}".format(
            major=GIT_MIN_MAJOR_VERSION, minor=GIT_MIN_MINOR_VERSION, build=GIT_MIN_PATCH_VERSION, current=git_version
        ))

    logger.info("check_git: OK")


def check_git_email():
    git_cmd = ["git", "config", "user.email"]
    git_out = exec_cmd(git_cmd)

    logger.debug("check_git_email: git_out->\n{}\n---".format(git_out))

    if not git_out or not git_out.endswith("@vmware.com"):
        raise RuntimeError("""
        Cannot obtain current user's email from git.
        Please run  command: 
            $git config user.email <your vmware email> 
        under your service's root folder. Replace '<your vmware email>' with your real vmware email, 
        e.g. ptimotin@vmware.com
        """)

    logging.info("check_git_email: OK")


def check_ssh():
    """
    Make sure that ssh connection is properly set up.
    :return:
    """
    logger.info("check_ssh: Verifying ssh connection to GitLab")

    try:
        ssh_cmd = ["ssh", "-T", GITLAB_HOST]
        ssh_out = exec_cmd(ssh_cmd)  # Welcome to GitLab, @username!

        logger.debug("check_ssh: ssh_out ->\n{}\n---".format(ssh_out))

        if not ssh_out or "welcome to gitlab" not in ssh_out.lower():
            raise RuntimeError("Wrong ssh check")

        logger.info("check_ssh: OK")

    except:
        logger.error("check_ssh: connect ssh failed with error message - {}".format(sys.exc_info()[1]))

        raise RuntimeError("Cannot ssh to {}. Have you setup ssh keys ? Please refer to GitLab manual - {} ".format(
            GITLAB_HOST, "https://gitlab.eng.vmware.com/help/ssh/README.md"))


def check_prerequisites(args):
    """
    Verify upgrading script prerequisites.
    :param args: Arguments passed to script.
    :return:
    """
    check_python()
    check_ssh()
    check_git()
    check_git_email()


if __name__ == "__main__":
    args = parse_cmd_args()
    check_prerequisites(args)
    args_dict = vars(args)
    manifest_dict = load_manifest_dict(MANIFEST_FILE)
    # Ask user confirmation if this is a direct invocation (and not from auto-update)
    if not args_dict.get("starter_dir"):
        ask_confirmation(manifest_dict["starter_version"], args_dict["starter_version"])
    logger.info("Arguments: " + str(sys.argv))
    telemetry_data = prepare_telemetry_data(manifest_dict, args_dict)
    try:
        upgrade(manifest_dict, args_dict)
        send_telemetry(telemetry_data, args_dict, None)
    except Exception as ex:
        if ex.__class__ is not InvalidInputError:
            send_telemetry(telemetry_data, args_dict, traceback.format_exc())
        raise ex
