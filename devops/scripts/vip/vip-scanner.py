#!/usr/bin/env python3
"""
Script for cloning and checking out a stable commit of vip-scanner repo.
"""
import logging
import subprocess
import sys
import re

__author__ = "VMware Inc."
__copyright__ = "Copyright 2021 VMware, Inc. All rights reserved."

GITLAB_HOST = "git@gitlab.eng.vmware.com"
GITLAB_REPO = "git@gitlab.eng.vmware.com:g11n-vip/vip-scanner.git"
GIT_STABLE_SHA = "3c841ad4caf3331cb66de554e63c45d239af2271"
GIT_STABLE_BRANCH = "master"
LOG_FORMAT = "%(asctime)s %(levelname)s - %(message)s"
CMD_TIMEOUT_SECONDS = 240

logger = logging.getLogger(__name__)

PYTHON_MIN_MAJOR_VERSION = 3
PYTHON_MIN_MINOR_VERSION = 6
GIT_MIN_MAJOR_VERSION = 2
GIT_MIN_MINOR_VERSION = 13
GIT_MIN_PATCH_VERSION = 7

def exec_cmd(cmd, timeout_seconds=CMD_TIMEOUT_SECONDS, cwd=None, shell=False, ignore_error=False):
    """
    Execute a command and return its standard output.
    :param cmd: a list of arguments that form the command
    :param timeout_seconds: timeout for the command
    :param cwd: optional working directory for the command. Defaults to script's working directory
    :param shell: whether to execute in shell or not
    :param ignore_error: whether to ignore process error
    :return: standard output of command
    """
    logger.debug("Executing " + str(cmd))
    process = None
    try:
        process = subprocess.run(cmd, shell=shell, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding="utf-8",
                                 timeout=timeout_seconds, cwd=cwd)
    except subprocess.TimeoutExpired:
        logger.error("Command output: {}".format(process.stdout))
        raise RuntimeError("Cmd '{}' timed out".format(" ".join(cmd)))
    except FileNotFoundError:
        raise RuntimeError("Cmd '{}' not available in PATH".format(cmd[0]))

    if not ignore_error and process.returncode != 0:
        logger.error("Command output: {}".format(process.stdout))
        raise RuntimeError("cmd '{}' failed".format(" ".join(cmd)))

    if process.stdout:
        out = process.stdout.strip()
        logger.debug(out)
        return out
    else:
        return None

def version_compare(src_dict, tgt_dict):
    """
    Compare two version dictionaries containing major, minor, patch keys and their values. Values can be string typed.
    semver is not standard python module, so we implement our own semver comparison.
    :param src_dict:
    :param tgt_dict:
    :return:
    """
    # convert values from string to int
    src = {k: int(v) for k, v in src_dict.items()}
    tgt = {k: int(v) for k, v in tgt_dict.items()}
    if src == tgt:
        return 0
    elif src["major"] > tgt["major"] or \
            (src["major"] == tgt["major"] and src["minor"] > tgt["minor"]) or \
            (src["minor"] == tgt["minor"] and src["patch"] > tgt["patch"]):
        return 1
    else:
        return -1

def check_python():
    """
    Verify if current python version is >= 3.6
    :return:
    """
    python_version = sys.version  # e.g.: '3.8.5 (default, Sep  3 2020, 21:29:08) [MSC v.1916 64 bit (AMD64)]'
    logger.debug("check_python: current version is {}".format(python_version))

    matcher = re.match(r"(\d+)\.(\d+).(\d+)\s+.*", python_version)

    if not matcher:
        logger.warning("check_python: Unable to parse python version. "
                       "Python must be >= {}.{} otherwise starter won't work. Current version is: {}".format(
            PYTHON_MIN_MAJOR_VERSION, PYTHON_MIN_MINOR_VERSION, python_version))

        return

    target_version = {"major": PYTHON_MIN_MAJOR_VERSION, "minor": PYTHON_MIN_MINOR_VERSION, "patch": 0}
    src_version = {"major": int(matcher.group(1)), "minor": int(matcher.group(2)), "patch": int(matcher.group(3))}

    logger.debug("check_python:  target_version->{}".format(target_version))
    logger.debug("check_python:  src_version->{}".format(src_version))

    if version_compare(src_version, target_version) < 0:
        error_str = "Python version {major}.{minor} or higher must be used. Current version is: {current}".format(
            major=PYTHON_MIN_MAJOR_VERSION, minor=PYTHON_MIN_MINOR_VERSION, current=python_version)

        raise RuntimeError(error_str)

    logger.info("check_python: OK")

def check_git():
    """
    Verify if current git version is >= 2.13.7
    :return:
    """
    git_version = exec_cmd(["git", "--version"])  # e.g.: 'git version 2.29.2.windows.2'

    matcher = re.match(r"git version (\d+)\.(\d+).(\d+).*", git_version)

    if not matcher:
        logger.warning("check_git: Unable to parse git version. "
                       "Git version must be >= {}.{}.{} otherwise starter won't work. Current version is: {}".format(
            GIT_MIN_MAJOR_VERSION, GIT_MIN_MINOR_VERSION, GIT_MIN_PATCH_VERSION, git_version))
        return

    target_version = {"major": GIT_MIN_MAJOR_VERSION, "minor": GIT_MIN_MINOR_VERSION, "patch": GIT_MIN_PATCH_VERSION}
    src_version = {"major": int(matcher.group(1)), "minor": int(matcher.group(2)), "patch": int(matcher.group(3))}

    if version_compare(src_version, target_version) < 0:
        raise RuntimeError("Git version must be >= {major}.{minor}.{build}. Current version is: {current}".format(
            major=GIT_MIN_MAJOR_VERSION, minor=GIT_MIN_MINOR_VERSION, build=GIT_MIN_PATCH_VERSION, current=git_version
        ))

    logger.info("check_git: OK")

def check_ssh():
    """
    Make sure that ssh connection is properly set up.
    :return:
    """
    logger.info("check_ssh: Verifying ssh connection to GitLab")

    try:
        ssh_cmd = ["ssh", "-T", GITLAB_HOST]
        ssh_out = exec_cmd(ssh_cmd)  # Welcome to GitLab, @username!

        logger.debug("check_ssh: ssh_out ->\n{}\n---".format(ssh_out))

        if not ssh_out or "welcome to gitlab" not in ssh_out.lower():
            raise RuntimeError("Wrong ssh check")

        logger.info("check_ssh: OK")

    except:
        logger.error("check_ssh: connect ssh failed with error message - {}".format(sys.exc_info()[1]))

        raise RuntimeError("Cannot ssh to {}. Have you setup ssh keys ? Please refer to GitLab manual - {} ".format(
            GITLAB_HOST, "https://gitlab.eng.vmware.com/help/ssh/README.md"))

def clone_repo():
    """
    Clone the vip-scanner repo and checkout to a stable commit SHA.
    :return:
    """
    logger.info("clone_repo: Cloning vip-scanner repo to a stable branch")

    try:
        git_clone = ["git", "clone", "-b", GIT_STABLE_BRANCH , GITLAB_REPO]
        git_clone_out = exec_cmd(git_clone, cwd="./build")

        git_checkout_commit = ["git", "checkout", GIT_STABLE_SHA]
        git_checkout_commit_out = exec_cmd(git_checkout_commit, cwd="./build/vip-scanner")

        logger.debug("clone_repo: git clone ->\n{}\n---".format(git_clone_out))
        logger.debug("clone_repo: git checkout ->\n{}\n---".format(git_checkout_commit_out))
        logger.info("clone_repo: OK")

    except:
        logger.error("clone_repo: cloning failed with error message - {}".format(sys.exc_info()[1]))

        raise RuntimeError("Cannot clone repository")

def install_dependencies():
    """
    Make sure that the correct dependencies are installed for the script to run.
    :return:
    """
    logger.info("install_dependencies: Installing required dependencies for script")

    try:
        install_dependencies = ["pip3", "install", "requests"]
        install_dependencies_out = exec_cmd(install_dependencies)

        logger.debug("install_dependencies: installing ->\n{}\n---".format(install_dependencies_out))
        logger.info("install_dependencies: OK")

    except:
        logger.error("install_dependencies: failed with - {}".format(sys.exc_info()[1]))

        raise RuntimeError("Cannot install dependencies")

def main():
    check_python()
    check_ssh()
    check_git()
    clone_repo()
    install_dependencies()

if __name__ == "__main__":
    main()
