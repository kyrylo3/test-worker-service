#!/usr/bin/env python3
"""
Locust load generator script.
See README.md for more information on how to run locust tests locally and in k8s.
"""
import base64
import http.client as http_client
import json
import locust.stats
import logging
import os
import random
import uuid

from datetime import datetime
from datetime import timedelta
from locust import HttpUser, TaskSet, task, between

http_client.HTTPConnection.debuglevel = 0

locust.stats.CSV_STATS_INTERVAL_SEC = 15  # default is 2 seconds


class UserBehavior(TaskSet):
    """
    Representation of a user behavior. This is essentially a set of load generation tasks that a locust user/client runs
    continuously until asked to stop.

    A task is defined as a method in this class with @task decorator, along with a number indicating relative weights.
    If task_a() has a weight of 3 and task_b() has a weight of 1, this means task_a() would be 3 times more likely to be
    executed than task_b().

    The weights are currently setup to represent a read-heavy workload but that can be changed as desired.
    """

    # Ideal number of resources per locust client (user)
    # We will start deleting after reaching this size
    ideal_size = 200

    # Maximum number of resources per locust client (user)
    max_size = 400

    use_csp_auth = False
    csp_host = ''
    csp_refresh_token = ''
    auth_expiration = ()
    access_token = ()

    # TODO CHANGEME - change these to match the service APIs to be tested.
    PROTECTED_RESOURCE_PATH = "C:/Program Files/Git/api/testworker/sample-resources"
    SAMPLE_RESOURCE_PATH = "/api/noauth/sample-resources"

    def __init__(self, parent):
        super().__init__(parent)
        self.sample_resource_ids = []
        self.protected_resource_ids = []

    def on_start(self):
        """
        This method is invoked when a locust user/client starts executing the tasks in this class.
        :return:
        """

        # Use CSP authentication if environment variables are set
        if "CSP_HOST" in os.environ.keys() and "CSP_REFRESH_TOKEN" in os.environ.keys():
            self.csp_host = os.environ["CSP_HOST"]
            self.csp_refresh_token = os.environ["CSP_REFRESH_TOKEN"]
            self.use_csp_auth = True
            logging.info("Using CSP host {} for authentication".format(self.csp_host))
        else:
            logging.info("Using dummy auth token for authentication")

        # Get auth token and store it
        val = self.get_auth()
        logging.info("{}".format(val))

    @staticmethod
    def rand():
        return random.randint(1, 1000000)

    @staticmethod
    def uuid():
        return uuid.uuid4()

    def get_auth(self):
        """
        Get auth token and store it.
        :return:
        """
        if self.use_csp_auth:
            return self.csp_auth()
        else:
            return self.dummy_auth()

    def dummy_auth(self):
        """
        Get a dummy, hard coded auth token for testing. This will work if a service is not running with "csp-prod"
        spring profile.
        :return: auth and content type headers
        """
        if not self.auth_expiration or self.token_expired():
            iat = datetime.now()
            exp = datetime.now() + timedelta(hours=10)

            header = json.dumps({
                "alg": "HS256",
                'kid': 'signing_2',
                "typ": "JWT"
            }, separators=(',', ':'))

            payload = json.dumps({
                'sub': 'vmware.com:3d9d92e9-09f1-4138-b226-1d348d368c67',
                'azp': 'locust',
                'domain': 'vmware.com',
                'context': 'b4580ed1-376b-4ea6-8345-01ee42e2521c',
                'iss': 'https://localhost/locust',
                'perms': [
                    'testservice:user'
                ],
                'context_name': '89523824-5cb1-4dc7-9478-fadca581a393',
                'exp': round(exp.timestamp() * 1000),
                'iat': round(iat.timestamp() * 1000),
                'jti': '19b1b0f3-3224-484b-b4bb-5975ac45c30f',
                'acct': 'kvody@vmware.com',
                'username': 'kvody'
            }, separators=(',', ':'))

            # get the token without signature
            encoded_header = base64.b64encode(header.encode("utf-8"))
            encoded_payload = base64.b64encode(payload.encode("utf-8"))
            encoded_jwt = encoded_header.decode("utf-8") + "." + encoded_payload.decode("utf-8") + "."
            self.auth_expiration = exp
            self.access_token = encoded_jwt
        return {
            "Authorization": "Bearer %s" % self.access_token,
            "Content-Type": "application/json"
        }

    def csp_auth(self):
        """
        Get a real CSP auth token for testing. CSP_HOST and CSP_REFRESH_TOKEN environment variables must be
        set correctly for this to work.
        :return: auth and content type headers
        """
        if not self.auth_expiration or self.token_expired():
            resp = self.client.post("https://{}/csp/gateway/am/api/auth/api-tokens/authorize".format(self.csp_host),
                                    data="refresh_token={}".format(self.csp_refresh_token),
                                    headers={"accept": "application/json",
                                             "content-type": "application/x-www-form-urlencoded"})
            resp_json = resp.json()
            self.auth_expiration = datetime.now() + timedelta(seconds=int(resp_json["expires_in"]) - 30)
            self.access_token = resp_json["access_token"]

        return {
            "Authorization": "Bearer %s" % self.access_token,
            "Content-Type": "application/json"
        }

    def token_expired(self):
        """
        Check if auth token has expired.
        :return:
        """
        return True if datetime.now() > self.auth_expiration else False

    ##########################
    # TODO CHANGEME - the following methods demonstrate generating request body and exercising the protected
    #  resource and sample resource APIs in service starter. Replace these with resources and APIs that need to be
    #  tested in your service
    ##########################

    def get_resource_body(self):
        """
        Return the resource object with just a name field to be used in request body.
        :return: resource object
        """
        return {
            "name": "locust-resource-%s" % self.uuid()
        }

    @task(10)
    def delete_protected_resources(self):
        """
        Delete a resource protected by auth.
        :return:
        """
        # If number of resources below ideal size, don't delete
        if not self.protected_resource_ids or len(self.protected_resource_ids) < self.ideal_size:
            return
        for x in range(0, 100):
            if not self.protected_resource_ids:
                break
            protected_resource_id = random.choice(self.protected_resource_ids)
            if not protected_resource_id:
                break

            self.client.delete("%s/%s" % (self.PROTECTED_RESOURCE_PATH, protected_resource_id), headers=self.get_auth(),
                               name="protectedresources/&lt;ID&gt;")
            self.protected_resource_ids.remove(protected_resource_id)
            logging.debug("deleted protected_resource_id: %s", protected_resource_id)

    @task(10)
    def delete_sample_resources(self):
        """
        Delete a resource not protected by auth.
        :return:
        """
        # If number of resources below ideal size, don't delete
        if not self.sample_resource_ids or len(self.sample_resource_ids) < self.ideal_size:
            return
        for x in range(0, 100):
            if not self.sample_resource_ids:
                break
            sample_resource_id = random.choice(self.sample_resource_ids)
            if not sample_resource_id:
                break

            self.client.delete("%s/%s" % (self.SAMPLE_RESOURCE_PATH, sample_resource_id),
                               name="sampleresources/&lt;ID&gt;")
            self.sample_resource_ids.remove(sample_resource_id)
            logging.debug("deleted sample_resource_id: %s", sample_resource_id)

    @task(10)
    def add_protected_resource(self):
        """
        Add a resource protected by auth.
        :return:
        """
        # Don't add if number of resources is at max size or if we get lucky
        if len(self.protected_resource_ids) >= self.max_size and random.randint(1, 50) > 10:
            return
        resp = self.client.post(self.PROTECTED_RESOURCE_PATH, json=self.get_resource_body(), headers=self.get_auth(),
                                name="protectedresources")
        if not resp.ok:
            logging.error("error adding protected resource: %s", resp.text)
            return
        resp_json = resp.json()
        protected_resource_id = resp_json["id"]
        logging.debug("added protected_resource_id: %s", protected_resource_id)
        self.protected_resource_ids.append(protected_resource_id)

    @task(10)
    def add_sample_resource(self):
        """
        Add a resource not protected by auth.
        :return:
        """
        # Don't add if number of resources is at max size or if we get lucky
        if len(self.sample_resource_ids) >= self.max_size and random.randint(1, 50) > 10:
            return
        resp = self.client.post(self.SAMPLE_RESOURCE_PATH, json=self.get_resource_body(), name="sampleresources")
        if not resp.ok:
            logging.error("error adding sample resource: %s", resp.text)
            return
        resp_json = resp.json()
        sample_resource_id = resp_json["id"]
        logging.debug("added sample_resource_id: %s", sample_resource_id)
        self.sample_resource_ids.append(sample_resource_id)

    @task(50)
    def get_protected_resources(self):
        """
        Get all protected resources.
        :return:
        """
        self.client.get("%s" % self.PROTECTED_RESOURCE_PATH, headers=self.get_auth(), name="protectedresources")

    @task(50)
    def get_sample_resources(self):
        """
        Get all resources not protected by auth.
        :return:
        """
        self.client.get("%s" % self.SAMPLE_RESOURCE_PATH, name="sampleresources")

    @task(5000)
    def get_protected_resource(self):
        """
        Get a specific resource protected by auth.
        :return:
        """
        if not self.protected_resource_ids:
            return
        protected_resource_id = random.choice(self.protected_resource_ids)
        resp = self.client.get("%s/%s" % (self.PROTECTED_RESOURCE_PATH, protected_resource_id), headers=self.get_auth(),
                               name="protectedresources/&lt;ID&gt;")
        if len(self.protected_resource_ids) > self.max_size or not resp.ok:
            self.protected_resource_ids.remove(protected_resource_id)

    @task(5000)
    def get_sample_resource(self):
        """
        Get a specific resource not protected by auth.
        :return:
        """
        if not self.sample_resource_ids:
            return
        sample_resource_id = random.choice(self.sample_resource_ids)
        resp = self.client.get("%s/%s" % (self.SAMPLE_RESOURCE_PATH, sample_resource_id),
                               name="sampleresources/&lt;ID&gt;")
        if len(self.sample_resource_ids) > self.max_size or not resp.ok:
            self.sample_resource_ids.remove(sample_resource_id)


class ApiUser(HttpUser):
    """
    Representation of a user/locust client. It specifies the user behavior that should be exercised and some
    configuration properties.
    """
    tasks = [UserBehavior]
    # min / max wait in seconds.  1 == 1s, .001 == 1ms
    wait_time = between(.5 if not 'MIN_WAIT' in os.environ.keys() else float(os.environ["MIN_WAIT"]), \
                        10 if not 'MAX_WAIT' in os.environ.keys() else float(os.environ["MAX_WAIT"]))
