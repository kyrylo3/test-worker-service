#!/bin/bash
#
# Script for deploying locust helm chart to a kubernetes namespace.
# Run this from project root directory.
# deploy_locust_k8s.sh -n <service namespace> [-c <csp host> -r <csp refresh token for test user> -w <worker count>]
# Example:
# deploy_locust_k8s.sh -n vmc-pdx-interface -c console-stg.cloud.vmware.com -r refresh-token-for-test-user
#

WORKER_COUNT=5

# Parse command line arguments
parse_args=()
    while [[ $# -gt 0 ]]; do
        key="$1"
        case $key in
        -n|--namespace)
            NAMESPACE="$2"
            shift # past argument
            shift # past value
            ;;
        -c|--csp-host)
            CSP_HOST="$2"
            shift # past argument
            shift # past value
            ;;
        -r|--refresh-token)
            CSP_REFRESH_TOKEN="$2"
            shift # past argument
            shift # past value
            ;;
        -w|--workers)
            WORKER_COUNT="$2"
            shift # past argument
            shift # past value
            ;;
        -h|--help|*)
            cat <<EOF
Usage: $0 -n <service namespace> [-c <csp host>] [-r <csp refresh token for test user>] [-w <worker count>]
-n/--namespace            Namespace where service is running.
-c/--csp-host             CSP host, if CSP authentication is desired. If unspecified, dummy auth tokens are used.
-r/--refresh-token        CSP refresh token for test user. Required if CSP host is specified.
-w/--workers              Number of locust workers to create (default: 5).
EOF
            exit 1
            ;;
        esac
    done


set -- parse_args

if [[ -z "${NAMESPACE}" ]]; then
    echo "Service namespace is required"
    exit 1
fi

if [[ -n "${CSP_HOST}" && -z "${CSP_REFRESH_TOKEN}" ]]; then
    echo "CSP refresh token is required when CSP host is specified"
    exit 1
fi

if [[ -n "${CSP_HOST}" && -n "${CSP_REFRESH_TOKEN}" ]]; then
    echo "Using CSP host ${CSP_HOST} for authentication"
else
    echo "Not using CSP authentication"
fi

SERVICE_NAME=test-worker-service
LOCUST_NAME=locust-${SERVICE_NAME}

# Create or update configmap with locust script
kubectl -n ${NAMESPACE} create configmap ${LOCUST_NAME} --from-file devops/locust/ -o yaml --dry-run | kubectl apply -f -

if [[ $? -ne 0 ]]; then
    echo "Locust configmap creation failed"
    exit 1
fi

EXTRA_ENV_OPTION=
# If CSP details are specified, add them as extra environment variables in helm override file
if [[ -n "${CSP_HOST}" && -n "${CSP_REFRESH_TOKEN}" ]]; then
    cat <<EOF >${TMPDIR}/env.yaml
extraEnvs:
  - name: CSP_HOST
    value: ${CSP_HOST}
  - name: CSP_REFRESH_TOKEN
    value: ${CSP_REFRESH_TOKEN}
EOF
    EXTRA_ENV_OPTION="-f ${TMPDIR}/env.yaml"
fi

# Pull down and install the latest stable locust chart, configuring it to use test script from configmap
helm repo add deliveryhero https://charts.deliveryhero.io/
helm upgrade --install --namespace ${NAMESPACE} --debug --wait --timeout 600s \
    ${LOCUST_NAME} deliveryhero/locust \
    --set loadtest.locust_locustfile_configmap=${LOCUST_NAME} \
    --set image.tag=1.5.3 \
    --set master.resources.limits.cpu=1 \
    --set master.resources.limits.memory=500Mi \
    --set master.resources.requests.cpu=100m \
    --set master.resources.requests.memory=500Mi \
    --set worker.resources.limits.cpu=1 \
    --set worker.resources.limits.memory=500Mi \
    --set worker.resources.requests.cpu=500m \
    --set worker.resources.requests.memory=500Mi \
    --set loadtest.locust_locustfile=locust_tests.py \
    --set loadtest.locust_host=http://${SERVICE_NAME}:8080 \
    --set worker.replicas=${WORKER_COUNT} \
    ${EXTRA_ENV_OPTION}

if [[ $? -ne 0 ]]; then
    echo "Locust chart install failed"
    exit 1
fi

LOCUST_MASTER_POD=$(kubectl -n ${NAMESPACE} get pod -l component=master -l release=${LOCUST_NAME} \
 -o=jsonpath='{.items[0].metadata.name}')

cat << EOF
Instructions for starting tests:
-------------------------------
# Connect to locust master via port-forwarding
kubectl -n ${NAMESPACE} port-forward ${LOCUST_MASTER_POD} 8089

# Start the locust swarm. Here, rate of initialization is 1 client per second (spawn_rate) and 2 workers total (user_count)
curl http://localhost:8089/swarm --data "spawn_rate=1&user_count=2"
# Alternatively, open locust UI at http://localhost:8089 and start a new test with number of users=2 and spawn rate=1.

Instructions for stopping the tests:
-----------------------------------
- Port-forward to locust master pod and open the UI http://localhost:8089. Click on the Stop button to stop tests.
OR
- Delete the locust deployment in Kubernetes via:
helm delete --purge ${LOCUST_NAME}

EOF
