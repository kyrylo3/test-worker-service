# test-worker-service

## Introduction

Include short description, links to any relevant documents (confluence page for service, etc.), and any other general information that is relevant to the project.
 
## Consumers
If the project generates artifacts for consumers, e.g., SDK, include relevant sub-sections as described below, otherwise remove this section.

### Add Dependency to your build
If the project publishes SDK, describe how to add dependencies in consuming service.

```groovy
// example build.gradle changes

repositories {
    maven { url 'https://build-artifactory.eng.vmware.com/artifactory/atlas-release-maven' }
}

dependencies {
    implementation "com.vmware.atlas:<library name>:<library version>"
}
```

### Public APIs
If the project produces public APIs (public to internal services and/or customer), describe how to view latest API contract. Ideally this should be a Swagger/OpenAPI documentation page.

### Quick Start Guide
Write up a quick start guide for using the consumer artifacts. Create code examples in [service starter](https://gitlab.eng.vmware.com/atlas/service-starter) � include one for each of the common flows that are expected - and provide a link here.
 
## Developers
This section contains instructions for developers of the project.

### Build
```bash
gradlew clean build
```

### Test
```bash
gradlew test
# If integration tests exist ...
gradlew integrationTest
```

### Run service
Service can be started with bootRun task of gradle. Include any special configuration that needs to be done as well.
```bash
gradlew bootRun
```

### Run service + dependencies
Use docker-compose to test locally with dependent services.

To create a local docker image with "test" as the tag:
```bash
./gradlew dockerBuild -Pimage_tag=test -Pdocker_registry=skyscraper-docker-local.artifactory.eng.vmware.com
```
This creates an image `skyscraper-docker-local.artifactory.eng.vmware.com/atlas/prototype/starter-testing/test-worker-service:test`.

To use docker-compose to run services:
```bash
cd devops/docker-compose

# Pull the latest build tags specified in .env file ("up" doesn't do this like you might expect)
docker-compose pull

# Option 1: build the service locally, start service and dependencies. Uses dependency tags from .env file. 
# Note that this builds service docker image using an already built service jar. You should build jar if needed before this.
docker-compose up --build

# Option 2: Run service and dependencies by using the latest tags deployed in k8s, as defined in .env file.
docker-compose up

# Option 3: run service and dependencies by setting the image tags explicitly (e.g. SERVICE_TAG=test).
SERVICE_TAG=test docker-compose up

# Option 4: run only specific services listed in docker-compose.yml.
# For e.g., run service in IDE and only dependencies in docker-compose.
docker-compose up [SERVICE...]

# Option 5: run docker-compose with auth. Be sure to enable secrets management before doing this:
# https://confluence.eng.vmware.com/pages/viewpage.action?spaceKey=ATLAS&title=Secrets+Management+Guide
DISABLE_CUSTOM_SERVER_SECRETS=0 AWS_ACCESS_KEY_ID=[...] AWS_SECRET_ACCESS_KEY=[...] docker-compose -f docker-compose-auth.yml up
```

### Run functional tests
Run the following command to build service image locally, tag it as `test`, bring up the service and dependencies in docker-compose and run the functional tests.
```bash
devops/scripts/run_functional_tests.sh precommit test
```

Run the tests with auth. Be sure to enable [secrets management](https://confluence.eng.vmware.com/pages/viewpage.action?spaceKey=ATLAS&title=Secrets+Management+Guide) before doing this.
```bash
DISABLE_CUSTOM_SERVER_SECRETS=0 AWS_ACCESS_KEY_ID=[...] AWS_SECRET_ACCESS_KEY=[...] devops/scripts/run_functional_tests.sh precommit test
```

### Code Coverage
Jacoco is used for collecting code coverage. JUnit-based tests and functional tests both collect coverage data. HTML report is generated automatically after test run.
* JUnit tests: HTML report will be available in `server/build/reports/jacoco` (and any other top-level gradle module) after running `gradlew build` locally.
* Functional tests: HTML report will be available in `functional-tests/build/reports/jacoco` after running `devops/scripts/run_functional_tests.sh precommit test` locally.

In precommit and postcommit pipelines, SonarQube will collect and generate coverage report. Login to https://vmc-sonarqube.svc.eng.vmware.com and search by your project name to view coverage reports.

#### Merged Coverage Report
You can merge coverage data from JUnit and functional tests and generate a merged report for local analysis. Run the following commands.
```bash
cd <project root>
# Run JUnit tests
./gradlew build

# Run functional tests
devops/scripts/run_functional_tests.sh precommit test

# Generate merged report in build/reports/jacoco
./gradlew jacocoReport
```

In pipelines, SonarQube will automatically merge coverage data, and the report on https://vmc-sonarqube.svc.eng.vmware.com will reflect that.

### Run Load tests for APIs
Load testing for service APIs is done using [Locust](https://locust.io). The test scenarios for generating load are written in Python in a simple and concise manner and can be executed against service running locally or in Kubernetes. Read [Locust documentation](https://docs.locust.io/en/stable/index.html) for more details.

The test script is in `devops/locust/locust_tests.py`.

#### Local Testing
Follow [installation instructions](https://docs.locust.io/en/stable/installation.html) for installing Locust on your local machine. Install the package for python 3. Python 2 is reaching end of life.

```
# Install a stable version of locust
pip3 install locust==1.5.3

# Run the locust script against locally running service with one client for 40 seconds. Uses dummy auth token by default
$ locust -L INFO -f locust_tests.py --users=1 --host=http://localhost:8080 --run-time=40 --headless --only-summary --csv=output

# Define CSP_HOST and CSP_REFRESH_TOKEN env variables for using CSP authentication
$ CSP_HOST=console-stg.cloud.vmware.com CSP_REFRESH_TOKEN=token locust -L INFO -f locust_tests.py --users=1 --host=http://localhost:8080 --run-time=40 --headless --only-summary --csv=output
```

#### Testing in Kubernetes

To deploy locust in k8s and start workload, following these steps from service root directory.
```
# One time step - update the helm repo index so that it can pull latest locust chart down below.
$ helm repo update

# Run script to deploy locust to k8s and follow instructions printed by the script.
# If csp host and refresh token are not specified, dummy token is used for authentication.
$ devops/locust/deploy_locust_k8s.sh -n <service namespace> [-t <tiller namespace> -c <csp host> -r <csp refresh token for test user> -w <worker count>]
```

There are a couple of ways to stop the locust workload:
1. Port-forward to locust master pod and open the UI `http://localhost:8089`. Click on the Stop button to stop tests.
2. Delete the locust deployment in Kubernetes.
   ```bash
    $ helm --tiller-namespace vmc-pdx-default delete --purge locust-test-worker-service

### Deploy
* Service: When changes are merged, CD pipeline will build docker image, push it to jfrog saas-artifactory and deploy to k8s using helm chart.

* Library/SDK/Client bindings: When changes are merged, CD pipeline will build the artifact and publish to artifactory.
   ```
   Release versions : https://build-artifactory.eng.vmware.com/artifactory/atlas-release-maven
   Snapshot versions : https://build-artifactory.eng.vmware.com/artifactory/atlas-snapshot-maven
   ```
   
### Change management
The recommended workflow with GitLab is as follows:
1. Clone this project
   ```bash
   git clone git@gitlab.eng.vmware.com:atlas/prototype/starter-testing/test-worker-service
   ```
2. Create a branch
   ```bash
    git checkout -b <userid>/<branch name>
   ```
3. Make changes and commit
4. Push commit to origin
    ```bash
    git push origin <userid>/<branch name>
    ```
5. Create merge request in GitLab by clicking on the link in the previous command's output.
6. Address review comments. Repeat steps 3 and 4 for additional changes made. It is recommended to create new commits instead of amending previous commit.
7. When the merge request is approved, merge the change from the merge request webpage in GitLab. Be sure to select the options to:
   * remove source branch
   * squash commits

### Code style
* IntelliJ IDEA users: Import `config/intellij-code-style.xml` into IDE
* Eclipse users: Import `config/eclipse-code-style.xml` into IDE

## Support
* How to contact the developers (slack channels, e-mail aliases, etc.)
* How to report bugs and feature enhancements (Jira project name, component name, etc.)
