# Define a list of required secret keys, one per line. These are loaded from
# S3 and exported as environment variables for service to consume.
SERVER_REQUIRED_SECRET_KEYS=(
# Loaded from atlas-test-worker-service-secrets-${AWS_REGION}
    "CSP_CLIENT_SECRET"
    "CSP_CLIENT_ID"
)
