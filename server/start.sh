#!/usr/bin/env bash

validate_env_vars() {
    for required_var_name in "${@}"; do
        # Ref: http://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html
        if [[ -z "${!required_var_name+x}" ]] ; then
            echo >&2 "Error: Missing required env vars: ${required_var_name}"
            exit 1
        fi
    done
}

# Assume role in AWS to get temporary credentials
assume_role() {
    ROLE_ARN="${1}"
    AWS_REGION="${2}"
    SESSION_NAME="${3}"

    echo "assume-role into ${ROLE_ARN}"
    CRED=$(aws sts assume-role --role-arn "${ROLE_ARN}" --role-session-name ${SESSION_NAME} --region ${AWS_REGION} --output json)
    export AWS_ACCESS_KEY_ID=$(echo $CRED | jq -r .Credentials.AccessKeyId)
    export AWS_SECRET_ACCESS_KEY=$(echo $CRED | jq -r .Credentials.SecretAccessKey)
    export AWS_SESSION_TOKEN=$(echo $CRED | jq -r .Credentials.SessionToken)
}

prepare_to_load_secrets() {
    # If AWS_ACCESS_KEY_ID is unset, then use AWS instance profile to get access to the S3 bucket for secrets
    if [[ -z "${AWS_ACCESS_KEY_ID+x}" ]] ; then
        echo "Pulling s3 secrets with instance profiles."
        # Required vars to assume role to access secrets stored in s3
        ASSUME_ROLE_VARS=(
            "AWS_REGION"
            "AWS_ARN"
            "AWS_CONTROL_ACCOUNT_ID"
            "AWS_LINKED_ACCOUNT_ID"
            "SECRETS_ACCESS_ROLE_SEGMENT"
            "SKYLAB_ENV"
        )

        # Validate that ASSUME ROLE VARS are set
        validate_env_vars "${ASSUME_ROLE_VARS[@]}"

        AWS_CONTROL_ACCOUNT_ROLE_NAME="svc.control-${SECRETS_ACCESS_ROLE_SEGMENT}-${SKYLAB_ENV}"
        AWS_LINKED_ACCOUNT_ROLE_NAME="svc.${SECRETS_ACCESS_ROLE_SEGMENT}-${SKYLAB_ENV}"

        CONTROL_ROLE_ARN="arn:${AWS_ARN}:iam::${AWS_CONTROL_ACCOUNT_ID}:role/${AWS_CONTROL_ACCOUNT_ROLE_NAME}"
        LINKED_ROLE_ARN="arn:${AWS_ARN}:iam::${AWS_LINKED_ACCOUNT_ID}:role/${AWS_LINKED_ACCOUNT_ROLE_NAME}"

        # Using the IAM role name as the session name.
        assume_role "${CONTROL_ROLE_ARN}" "${AWS_REGION}" "${AWS_CONTROL_ACCOUNT_ROLE_NAME}"
        assume_role "${LINKED_ROLE_ARN}" "${AWS_REGION}" "${AWS_LINKED_ACCOUNT_ROLE_NAME}"

        # Required vars to access secrets stored in s3
        S3_ACCESS_VARS=(
            "SECRETS_BUCKET_NAME"
        )
    else
        echo "Pulling s3 secrets without instance profiles."
        # Required vars to access secrets stored in s3
        S3_ACCESS_VARS=(
            "SECRETS_BUCKET_NAME"
            "AWS_ACCESS_KEY_ID"
            "AWS_SECRET_ACCESS_KEY"
            "AWS_REGION"
        )
    fi

    # Validate that S3 ACCESS VARS are set
    validate_env_vars "${S3_ACCESS_VARS[@]}"
}

# Set default=1 if the flag is unset
if [[ -z "${DISABLE_CUSTOM_SERVER_SECRETS}" ]]
then
    export DISABLE_CUSTOM_SERVER_SECRETS=1
fi

if [[ "${DISABLE_CUSTOM_SERVER_SECRETS}" -ne 1 ]]
then
    prepare_to_load_secrets

    # Load service secrets from S3
    eval $(aws s3 cp s3://${SECRETS_BUCKET_NAME}/atlas-test-worker-service-secrets-${AWS_REGION} --region ${AWS_REGION} - | sed -e 's/\r$//' -e 's/^/export /') > /dev/null

    # Load secrets for AppDynamics.
    [[ "${ENABLE_APPD_AGENT}" -eq "1" ]] && eval $(aws s3 cp s3://${SECRETS_BUCKET_NAME}/appdynamics-credentials --region ${AWS_REGION} - | sed -e 's/\r$//' -e 's/^/export /') > /dev/null

    #  Read-in different types of required secret-keys into
    #  environment variables specified in SERVER_REQUIRED_SECRET_KEYS and then
    #  validate that required secrets have all been set
    . ./required-secret-keys.sh
    validate_env_vars "${SERVER_REQUIRED_SECRET_KEYS[@]}"
else
    echo "Not loading secrets from S3. If secrets are required, they should be already set in environment"
fi

JVM_OPTS="${JVM_OPTS:-} -Dfile.encoding=UTF-8 -Dsun.jnu.encoding=UTF-8"

# NOTE: properties need to be placed before the -javaagent options or else they won't work
if [ -r "/opt/vmware/conf/logback.xml" ]
then
  JVM_OPTS="${JVM_OPTS} -Dlogging.config=/opt/vmware/conf/logback.xml"
fi

# Enable the AppDynamics agent if ENABLE_APPD_AGENT=1
if [[ "${ENABLE_APPD_AGENT}" -eq "1" ]] && [[ -n "${APPD_OPTS+x}" ]] && [[ -n "${APPDYNAMICS_AGENT_ACCOUNT_ACCESS_KEY+x}" ]]
then
    echo "Enabling AppDynamics agent"
    JVM_OPTS="${JVM_OPTS} -javaagent:${SVC_HOME}/appd-agent/javaagent.jar ${APPD_OPTS}"
else
    echo "Skip enabling AppDynamics agent."
fi

# Set light weight java profiler
if [[ "${ENABLE_LJP}" = "true" ]]
then
    echo "Lightweight Java Profiler enabled, dump rate = ${LJP_DUMP_INTERVAL:-300} seconds, sample rate = ${LJP_SAMPLE_RATE:-500} ms."
    STACK_DIR=${JAVA_STACK_DIR:-/opt/vmware/stackdumps}
    [[ -d "${STACK_DIR}" ]] || mkdir "${STACK_DIR}"
    JVM_OPTS="${JVM_OPTS:-} -agentpath:/opt/vmware/lib/liblagent.so=file=${STACK_DIR}/currstate,interval=${LJP_DUMP_INTERVAL:-300},rate=${LJP_SAMPLE_RATE:-500}"
    # existance of /tmp/profile is neccessary for ljp to profile.  Removing the file will cause profiling to stop
    touch /tmp/profile
fi

if [[ -n "${HEAP_DUMP_DIR}" ]]
then
    JVM_OPTS="${JVM_OPTS:-} -XX:+HeapDumpOnOutOfMemoryError  -XX:HeapDumpPath=${HEAP_DUMP_DIR} -XX:ErrorFile=${HEAP_DUMP_DIR}/hs_err.log"
fi


# Attach jacoco agent if asked. This should only be used when running functional tests
if [[ "${ENABLE_CODE_COVERAGE}" == "true" ]] && [[ -d "${COVERAGE_RESULT_DIR}" ]] && [[ -n "${COVERAGE_DATA_FILE}" ]]
then
    # Download jacoco agent. Keep this version in sync with jacoco toolVersion in root build.gradle
    if ! curl -s -S --output /tmp/jacocoagent.jar https://build-artifactory.eng.vmware.com/maven/org/jacoco/org.jacoco.agent/0.8.5/org.jacoco.agent-0.8.5-runtime.jar
    then
        echo "Failed to download Jacoco agent"
        exit 1
    fi
    coverage_file="${COVERAGE_RESULT_DIR}/${COVERAGE_DATA_FILE}"
    echo "Enabling code coverage. Coverage data will be saved to ${coverage_file} when JVM exits gracefully"
    JVM_OPTS="${JVM_OPTS:-} -javaagent:/tmp/jacocoagent.jar=destfile=${coverage_file},append=false"
fi

set -x
exec java ${JVM_OPTS} -jar ./app.jar
