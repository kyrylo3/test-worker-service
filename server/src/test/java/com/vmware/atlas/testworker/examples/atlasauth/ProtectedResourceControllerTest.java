/*******************************************************************************
 * Copyright 2019 VMware, Inc.  All rights reserved.
 *  -- VMware Confidential
 ******************************************************************************/
package com.vmware.atlas.testworker.examples.atlasauth;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vmware.atlas.testworker.TestApplication;
import com.vmware.atlas.testworker.UriPaths;
import com.vmware.atlas.testworker.examples.service.SampleService.SampleResource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Unit test cases for sample controller with authentication enabled. Since this test does not interact with CSP to
 * generate an auth token but uses a hard coded token, it runs without "csp-prod" profile to turn off certain types
 * of token validation (e.g., CSP public key verification, expiration check, service definition id whitelisting check)
 */
@SpringBootTest(classes = {TestApplication.class})
@AutoConfigureMockMvc
@ActiveProfiles("authz-local")
public class ProtectedResourceControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @ParameterizedTest
    @MethodSource("com.vmware.atlas.testworker.examples.atlasauth.AuthzTestDataProvider#provideInputOutput")
    public void testCreate(String jwtToken, HttpStatus status) throws Exception {
        if (status != HttpStatus.OK) {
            mockMvc.perform(post(UriPaths.PROTECTED_RESOURCE_URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken)
                                .content("{\"name\": \"create-test\"}"))
                .andExpect(status().is(status.value()));
            return;
        }

        // Request without authentication and expect Unauthorized
        mockMvc.perform(post(UriPaths.PROTECTED_RESOURCE_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\": \"create-test\"}"))
                .andExpect(status().isUnauthorized());

        // Request with authentication
        mockMvc.perform(post(UriPaths.PROTECTED_RESOURCE_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken)
                .content("{\"name\": \"create-test\"}"))
                .andExpect(status().is(status.value()))
                .andExpect(jsonPath("$.name", equalTo("create-test")));
    }

    @ParameterizedTest
    @MethodSource("com.vmware.atlas.testworker.examples.atlasauth.AuthzTestDataProvider#provideInputOutput")
    public void testGet(String jwtToken, HttpStatus status) throws Exception {
        if (status != HttpStatus.OK) {
            mockMvc.perform(get(UriPaths.PROTECTED_RESOURCE_URI + "/" + "TEST-UID")
                                .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken))
                .andExpect(status().is(status.value()));
            return;
        }

        MvcResult result = mockMvc.perform(post(UriPaths.PROTECTED_RESOURCE_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken)
                .content("{\"name\": \"get-test\"}"))
                .andExpect(status().is(status.value()))
                .andReturn();

        SampleResource created =
                objectMapper.readValue(result.getResponse().getContentAsString(), SampleResource.class);

        result = mockMvc.perform(get(UriPaths.PROTECTED_RESOURCE_URI + "/" + created.getId())
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken))
                .andExpect(status().is(status.value()))
                .andReturn();

        SampleResource resource =
                objectMapper.readValue(result.getResponse().getContentAsString(), SampleResource.class);
        assertThat(resource).isNotNull();
        assertThat(resource.getId()).isEqualTo(resource.getId());
        assertThat(resource.getName()).isEqualTo(resource.getName());
    }

    @ParameterizedTest
    @MethodSource("com.vmware.atlas.testworker.examples.atlasauth.AuthzTestDataProvider#provideInputOutput")
    public void testGetAll(String jwtToken, HttpStatus status) throws Exception {
        if (status != HttpStatus.OK) {
            mockMvc.perform(get(UriPaths.PROTECTED_RESOURCE_URI)
                                .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken))
                .andExpect(status().is(status.value()));
            return;
        }

        mockMvc.perform(post(UriPaths.PROTECTED_RESOURCE_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken)
                .content("{\"name\": \"getall-test-1\"}"))
                .andExpect(status().is(status.value()));
        mockMvc.perform(post(UriPaths.PROTECTED_RESOURCE_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken)
                .content("{\"name\": \"getall-test-2\"}"))
                .andExpect(status().is(status.value()));
        Set<String> expectedNames = new HashSet<>();
        expectedNames.add("getall-test-1");
        expectedNames.add("getall-test-2");

        MvcResult result = mockMvc.perform(get(UriPaths.PROTECTED_RESOURCE_URI)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken))
                .andExpect(status().is(status.value()))
                .andReturn();

        List<SampleResource> resources = objectMapper.readValue(result.getResponse().getContentAsString(),
                new TypeReference<List<SampleResource>>(){});
        assertThat(resources).isNotNull();

        // Check for >= since we may get resources created in other tests in this class
        assertThat(resources.size()).isGreaterThanOrEqualTo(2);
        Set<String> gotNames = resources.stream().map(SampleResource::getName).collect(Collectors.toSet());
        assertThat(gotNames).containsAll(expectedNames);
    }

    @ParameterizedTest
    @MethodSource("com.vmware.atlas.testworker.examples.atlasauth.AuthzTestDataProvider#provideInputOutput")
    public void testDelete(String jwtToken, HttpStatus status) throws Exception {
        if (status != HttpStatus.OK) {
            mockMvc.perform(delete(UriPaths.PROTECTED_RESOURCE_URI + "/" + "TEST-UID")
                                .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken))
                .andExpect(status().is(status.value()));
            return;
        }

        MvcResult result = mockMvc.perform(post(UriPaths.PROTECTED_RESOURCE_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken)
                .content("{\"name\": \"delete-test\"}"))
                .andExpect(status().is(status.value()))
                .andReturn();

        SampleResource created =
                objectMapper.readValue(result.getResponse().getContentAsString(), SampleResource.class);

        result = mockMvc.perform(delete(UriPaths.PROTECTED_RESOURCE_URI + "/" + created.getId().toString())
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken))
                .andExpect(status().is(status.value()))
                .andReturn();

        SampleResource resource =
                objectMapper.readValue(result.getResponse().getContentAsString(), SampleResource.class);
        assertThat(resource).isNotNull();
        assertThat(resource.getId()).isEqualTo(resource.getId());
        assertThat(resource.getName()).isEqualTo(resource.getName());
    }
}
