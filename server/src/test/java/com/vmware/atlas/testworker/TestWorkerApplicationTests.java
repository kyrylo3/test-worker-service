package com.vmware.atlas.testworker;

import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import com.vmware.symphony.cache.ignite.IgniteSpringAutoConfiguration;
import com.vmware.symphony.webmvc.error.RestErrorHandlingAutoConfiguration;

@SpringBootTest(classes = {TestApplication.class})
@TestPropertySource(properties = {
    "atlas.data.enable-schema-init=false",
    "cmf.autoconfig.rest-error-handling.enabled=false",
    "atlas.orchestration.scheduler-mode=DISABLED",
    "cmf.autoconfig.ignite.enabled=true"
})
@ImportAutoConfiguration({
        RestErrorHandlingAutoConfiguration.class,
        IgniteSpringAutoConfiguration.class
})
public class TestWorkerApplicationTests {

    @Test
    public void contextLoads() {
    }
}
