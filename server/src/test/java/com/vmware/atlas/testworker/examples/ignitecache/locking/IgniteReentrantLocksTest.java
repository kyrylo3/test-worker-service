/*
 * Copyright (c) 2021 VMware, Inc. All rights reserved. VMware Confidential
 */

package com.vmware.atlas.testworker.examples.ignitecache.locking;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteLock;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;

import com.vmware.symphony.cache.ignite.IgniteSpringAutoConfiguration;

/**
 * The test demonstrates the usage ignite reentrant locks. The reentrant lock provides the functionality similar to
 * functionality similar to @see java.util.concurrent.ReentrantLock. Please check @see
 * <a href="https://ignite.apache.org/releases/latest/javadoc/org/apache/ignite/IgniteLock.html">ReentrantLock</a> for
 * more details The reentrant locks are thread scoped, the thread which procures/owns the lock is responsible for
 * release the lock. Please check
 *
 * @see IgniteSemaphoreLocksTest for details regarding the
 * distributed locks that can be controlled beyond thread or JVM scoped.Use this if the scope of the lock needs to be a
 * thread. See * {@link IgniteSemaphoreLocksTest} for locking across nodes/threads.
 */
@SpringBootTest(classes = IgniteReentrantLocksTest.ReentrantLocksConfiguration.class)
public class IgniteReentrantLocksTest {

    @Autowired
    private Ignite ignite;
    private static final String LOCK_KEY_PATTERN = "lock.%s.%s";

    /**
     * This test demonstrates procuring ignite reentrant lock.
     */
    @Test
    public void testReentrantLocks() {
        String taskId = UUID.randomUUID().toString();
        String lockKey = String.format(LOCK_KEY_PATTERN, "add-host", taskId);
        boolean isProcessed = false;
        //this call will create a reentrant lock key that is fail over safe, and fair if it is not present in ignite cache.
        //Please refer to Ignite java docs (https://ignite.apache.org/releases/latest/javadoc/org/apache/ignite/IgniteLock.html),
        // for more details on failoverSafe and fair properties.
        Lock igniteLock = ignite.reentrantLock(lockKey, true, true, true);
        //this will lead to else block if the lock cannot be procured immediately
        if (igniteLock.tryLock()) {
            try {
                isProcessed = true;
                //execution conditional logic under lock in an exclusive fashion.
            } finally {
                //release the lock
                igniteLock.unlock();
            }
        } else {
            // take alternative step if the lock cannot be procured.
        }
        assertThat(isProcessed).isTrue();

    }

    /**
     * This test demonstrates procuring ignite reentrant lock with waiting time, in case the lock is not immediately
     * available.
     */
    @Test
    public void testReentrantLocksWaitedProcurement() {
        String taskId = UUID.randomUUID().toString();
        String lockKey = String.format(LOCK_KEY_PATTERN, "add-host", taskId);
        boolean isProcessed = false;
        //this call will create a reentrant lock key that is fail over safe, and fair if it is not present in ignite cache.
        //Please refer to Ignite java docs for more details or failoverSafe and fair properties.
        IgniteLock igniteLock = ignite.reentrantLock(lockKey, true, true, true);
        //this will lead to else block if the lock cannot be procured in 1 sec.
        if (igniteLock.tryLock(1, TimeUnit.SECONDS)) {
            try {
                isProcessed = true;
                //execute conditional logic under lock in an exclusive fashion.
            } finally {
                //release the lock.
                igniteLock.unlock();
            }
        } else {
            //take alternative step if the lock cannot be procured.
        }
        assertThat(isProcessed).isTrue();

    }

    @Configuration
    @ImportAutoConfiguration(IgniteSpringAutoConfiguration.class)
    static class ReentrantLocksConfiguration {

    }
}
