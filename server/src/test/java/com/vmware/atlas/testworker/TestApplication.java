/*******************************************************************************
 * Copyright 2019 VMware, Inc.  All rights reserved.
 *  -- VMware Confidential
 ******************************************************************************/
package com.vmware.atlas.testworker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.vmware.atlas.vip.EnableLocalization;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import com.vmware.atlas.library.auth.EnableAtlasAuthentication;
import com.vmware.atlas.message.client.annotation.EnableMessaging;
import com.vmware.atlas.feature.client.annotation.EnableFeatureClient;

// Component scan exclude filters ensure that beans in example packages that connect to other services
// don't fail unit tests
@ComponentScan(excludeFilters = {
        @ComponentScan.Filter(type = FilterType.REGEX, pattern = "com.vmware.atlas.testworker.examples.dataclient..+"),
        @ComponentScan.Filter(type = FilterType.REGEX, pattern = "com.vmware.atlas.testworker.examples.telemetryclient..+"),
})
@EnableAtlasAuthentication
@EnableMessaging
@EnableLocalization
@EnableCaching
@EnableFeatureClient
@SpringBootApplication
public class TestApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }
}
