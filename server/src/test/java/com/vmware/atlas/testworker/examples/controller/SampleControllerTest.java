/*******************************************************************************
 * Copyright 2019 VMware, Inc.  All rights reserved.
 *  -- VMware Confidential
 ******************************************************************************/
package com.vmware.atlas.testworker.examples.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vmware.atlas.testworker.TestApplication;
import com.vmware.atlas.testworker.UriPaths;
import com.vmware.atlas.testworker.examples.service.SampleService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Unit test cases for sample controller with no authentication.
 */
@SpringBootTest(classes = {TestApplication.class})
@AutoConfigureMockMvc
@ActiveProfiles("authz-local")
public class SampleControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void testCreate() throws Exception {
        mockMvc.perform(post(UriPaths.SAMPLE_RESOURCE_URI)
        .contentType(MediaType.APPLICATION_JSON)
        .content("{\"name\": \"create-test\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo("create-test")));
    }

    @Test
    public void testGet() throws Exception {
        MvcResult result = mockMvc.perform(post(UriPaths.SAMPLE_RESOURCE_URI)
        .contentType(MediaType.APPLICATION_JSON)
        .content("{\"name\": \"get-test\"}"))
                .andReturn();

        SampleService.SampleResource created =
                objectMapper.readValue(result.getResponse().getContentAsString(), SampleService.SampleResource.class);

        result = mockMvc.perform(get(UriPaths.SAMPLE_RESOURCE_URI + "/" + created.getId()))
                .andExpect(status().isOk())
                .andReturn();

        SampleService.SampleResource resource =
                objectMapper.readValue(result.getResponse().getContentAsString(), SampleService.SampleResource.class);
        assertThat(resource).isNotNull();
        assertThat(resource.getId()).isEqualTo(resource.getId());
        assertThat(resource.getName()).isEqualTo(resource.getName());
    }

    @Test
    public void testGetAll() throws Exception {
        mockMvc.perform(post(UriPaths.SAMPLE_RESOURCE_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\": \"getall-test-1\"}"));
        mockMvc.perform(post(UriPaths.SAMPLE_RESOURCE_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\": \"getall-test-2\"}"));
        Set<String> expectedNames = new HashSet<>();
        expectedNames.add("getall-test-1");
        expectedNames.add("getall-test-2");

        MvcResult result = mockMvc.perform(get(UriPaths.SAMPLE_RESOURCE_URI))
                .andExpect(status().isOk())
                .andReturn();

        List<SampleService.SampleResource> resources = objectMapper.readValue(result.getResponse().getContentAsString(),
                new TypeReference<List<SampleService.SampleResource>>(){});
        assertThat(resources).isNotNull();

        // Check for >= since we may get resources created in other tests in this class
        assertThat(resources.size()).isGreaterThanOrEqualTo(2);
        Set<String> gotNames = resources.stream().map(SampleService.SampleResource::getName).collect(Collectors.toSet());
        assertThat(gotNames).containsAll(expectedNames);
    }

    @Test
    public void testDelete() throws Exception {
        MvcResult result = mockMvc.perform(post(UriPaths.SAMPLE_RESOURCE_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\": \"delete-test\"}"))
                .andReturn();

        SampleService.SampleResource created =
                objectMapper.readValue(result.getResponse().getContentAsString(), SampleService.SampleResource.class);

        result = mockMvc.perform(delete(UriPaths.SAMPLE_RESOURCE_URI + "/" + created.getId().toString()))
                .andExpect(status().isOk())
                .andReturn();

        SampleService.SampleResource resource =
                objectMapper.readValue(result.getResponse().getContentAsString(), SampleService.SampleResource.class);
        assertThat(resource).isNotNull();
        assertThat(resource.getId()).isEqualTo(resource.getId());
        assertThat(resource.getName()).isEqualTo(resource.getName());
    }
}
