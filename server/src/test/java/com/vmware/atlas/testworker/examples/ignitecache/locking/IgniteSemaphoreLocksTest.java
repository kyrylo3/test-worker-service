/*
 * Copyright (c) 2021 VMware, Inc. All rights reserved. VMware Confidential
 */

package com.vmware.atlas.testworker.examples.ignitecache.locking;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteSemaphore;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;

import com.vmware.symphony.cache.ignite.IgniteSpringAutoConfiguration;

/**
 * This test demonstrates the usage of Ignite distributed semaphores to satisfy distributed locking requirement, where
 * the scope of the locks can spawn across multiple threads/Nodes/JVM instances. Please check @see
 * <a href="https://ignite.apache.org/releases/latest/javadoc/org/apache/ignite/IgniteSemaphore.html">IgniteSemaphore</a>
 * for more details on Ignite semaphores. Use this if the scope of the lock needs to be across the nodes/threads.  See
 * {@link IgniteReentrantLocksTest} for locking on a thread across the entire service cluster.
 */
@SpringBootTest(classes = IgniteSemaphoreLocksTest.SemaphoreLockConfiguration.class)
public class IgniteSemaphoreLocksTest {

    private final int PERMIT_COUNT = 1;
    private static final String LOCK_KEY_PATTERN = "lock.%s.%s";
    @Autowired
    private Ignite ignite;

    /**
     * This test demonstrates procuring ignite semaphore lock.
     */
    @Test
    public void testSemaphoreLocks() {
        String taskId = UUID.randomUUID().toString();
        String lockKey = String.format(LOCK_KEY_PATTERN, "add-host", taskId);
        boolean isProcessed = false;
        //this call will create a semaphore lock key that is fail over safe, and fair if it is not present in ignite cache.
        //Please refer to Ignite docs @ https://ignite.apache.org/docs/latest/data-structures/semaphore for more details.
        IgniteSemaphore igniteLock = ignite.semaphore(lockKey, PERMIT_COUNT, true, true);
        //this will lead to else block if the lock cannot be procured immediately.
        //This lock can be controlled beyond the scope of the java thread which procured the lock.
        if (igniteLock.tryAcquire()) {
            try {
                isProcessed = true;
                //execute conditional logic under lock in an exclusive fashion.
            } finally {
                //release the lock
                igniteLock.release();
            }
        } else {
            //take alternative step if the lock cannot be procured.
        }
        assertThat(isProcessed).isTrue();

    }

    /**
     * This test demonstrates procuring ignite semaphore lock with waiting time, in case the lock is not immediately
     * available.
     */
    @Test
    public void testSemaphoreLocksWaitedProcurement() {
        String taskId = UUID.randomUUID().toString();
        String lockKey = String.format(LOCK_KEY_PATTERN, "add-host", taskId);
        boolean isProcessed = false;
        //this call will create a semaphore lock key that is fail over safe, and fair if it is not present in ignite cache.
        //Please refer to Ignite java docs for more details or failoverSafe and fair properties.
        IgniteSemaphore igniteLock = ignite.semaphore(lockKey, PERMIT_COUNT, true, true);
        //this will lead to else block if the lock cannot be procured in 1 sec.
        if (igniteLock.tryAcquire(1, TimeUnit.SECONDS)) {
            try {
                isProcessed = true;
                //execution conditional logic under lock in an exclusive fashion.
            } finally {
                //release the lock
                igniteLock.release();
            }
        } else {
            // take alternative step if the lock cannot be procured.
        }
        assertThat(isProcessed).isTrue();

    }

    @Configuration
    @ImportAutoConfiguration(IgniteSpringAutoConfiguration.class)
    static class SemaphoreLockConfiguration {

    }
}
