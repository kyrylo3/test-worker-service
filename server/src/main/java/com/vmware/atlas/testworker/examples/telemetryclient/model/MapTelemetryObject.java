package com.vmware.atlas.testworker.examples.telemetryclient.model;

import com.vmware.atlas.telemetry.client.annotation.TelemetryNoValidation;
import com.vmware.atlas.telemetry.client.annotation.TelemetryTable;
import lombok.Data;
import lombok.ToString;

import java.util.Map;

@ToString
@Data
@TelemetryTable("service-starter-map-telemetry-object")
public class MapTelemetryObject {

    /**
     * Validation temporarily disabled until {@link com.vmware.atlas.telemetry.client.TelemetryClassValidator}
     * is extended to receive {@link Map}.
     * Described in issue: AA-7815
     * */
    @TelemetryNoValidation
    private Map<String, NestedTelemetryObject> mapOfObjects;
}
