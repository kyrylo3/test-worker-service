/*******************************************************************************
 * Copyright 2020 VMware, Inc.  All rights reserved.
 * -- VMware Confidential
 ******************************************************************************/

package com.vmware.atlas.testworker.examples.telemetryclient.model;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.vmware.atlas.telemetry.client.annotation.TelemetryId;
import com.vmware.atlas.telemetry.client.annotation.TelemetryTable;

import lombok.Data;
import lombok.ToString;

/**
 *  Telemetry objects are dedicated objects that are used in the telemetry.
 *  All telemetry objects have @TelemetryTable annotation. The value 'service_starter_conversion_examples' is the
 *  name of the table where the data will be stored. It is recommended for every service to use naming convention
 *  - <service_name>_<table_name> to name its tables, because more than one service can use one collectorId(schema).
 *  TelemetryObject is paired with the DomainObject. TelemetryObject represent data from the DomainObject that will
 *  be sent as telemetry. Name of the fields of TelemetryObject and DomainObject have to match.
 */
@ToString
@Data
@TelemetryTable("service_starter_conversion_examples")
public class TelemetryObject {
    @TelemetryId
    private UUID id;
    private String name;
    private OffsetDateTime date;
    @JsonSerialize(using = ToStringSerializer.class)
    private OffsetDateTime dateAsString;
    private int[] arrayOfPrimitives;
    private NestedTelemetryObject[] arrayOfObjects;
    private List<NestedTelemetryObject> listOfObjects;
    private NestedTelemetryObject nestedObject;
}
