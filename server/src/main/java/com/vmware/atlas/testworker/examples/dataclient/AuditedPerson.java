package com.vmware.atlas.testworker.examples.dataclient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class AuditedPerson {
    private String id;
    private String name;
    private int age;

    // In order to access the audit data that data-fabric-service gathers you need to define the following fields in
    // your type's POJO counterpart. These do not need to be defined in the schema of your type.
    private String dfCreatedBy;
    private Long dfCreatedTimestamp;
    private String dfLastUpdatedBy;
    private Long dfLastUpdatedTimestamp;
}
