/*******************************************************************************
 * Copyright 2020 VMware, Inc.  All rights reserved.
 * -- VMware Confidential
 ******************************************************************************/

package com.vmware.atlas.testworker.examples.vipclient;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vmware.atlas.vip.configuration.ErrorGroupCodes;
import com.vmware.atlas.vip.exceptions.DefaultLocalizableRestException;
import com.vmware.atlas.vip.exceptions.LocalizableValidationException;
import com.vmware.atlas.vip.validators.ValidateRest;

import lombok.extern.slf4j.Slf4j;

/**
 *  The class contains different examples of Exceptions - their behavior and wrappers.
 *  Check http://localhost:{8080 or 9080}/api/service-starter/{example}Exception
 *  to see the example.
 */
@Slf4j
@RestController
@RequestMapping("/api/starter")
public class ExamplesLocalizedExceptionsController {

    public static final String WRAPPED_EXCEPTION_ENDPOINT = "/WrappedRuntimeException";
    public static final String DEFAULT_LOCALIZABLE_REST_EXCEPTION = "/DefaultLocalizableRestException";
    public static final String VALIDATION_INTERNAL_ENDPOINT = "/ValidationInternal";
    public static final String VALIDATION_NOT_EMPTY = "/ValidationNotEmpty";

    //RuntimeException is internally wrapped in InternalErrorException that populates 'Error Reference ID'.
    //Reference ID can be associated with the log and at the same time hides the internal details on return.
    @GetMapping(value = WRAPPED_EXCEPTION_ENDPOINT)
    public void wrapped() {
        throw new RuntimeException("RuntimeException is hidden. 'Error Reference ID' is added.");
    }

    //On validation error LocalizableValidationException is returned with proper message.
    @GetMapping(value = VALIDATION_INTERNAL_ENDPOINT)
    public void validationInternalEnglish() {
        throwValidationExceptionIfValueOfNameIsNull(null, "my object");
    }

    public static void throwValidationExceptionIfValueOfNameIsNull(Object value, String name) {
        if (value == null) {
            throw new LocalizableValidationException("service-starter.input.field.not.odd",
                                                     "''{0}'' input field has to be odd number.",
                                                     name);
        }
    }

    //On validation there is support for generic validation using ValidateRest utility class.
    @GetMapping(value = VALIDATION_NOT_EMPTY)
    public void throwFromValidateRest() {
        ValidateRest.notNullOrEmpty((String) null, "test value");
    }

    //DefaultLocalizableRestException is provided by the vip-client library and
    // will return a default value if the resource cannot be translated.
    @GetMapping(value = DEFAULT_LOCALIZABLE_REST_EXCEPTION)
    public void throwDefaultLocalizableRestException() {
        throw new DefaultLocalizableRestException(HttpStatus.CONFLICT, ErrorGroupCodes.ENTITY_PRESENT,
                                                   "generic.rest.validation.exists.2.args",
                                                   "''{0}'' already exists - {1}", null,
                                                   "name", "value");
    }

}
