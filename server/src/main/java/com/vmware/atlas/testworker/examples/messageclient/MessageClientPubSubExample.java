/*******************************************************************************
 * Copyright 2019 VMware, Inc.  All rights reserved.
 *  -- VMware Confidential
 ******************************************************************************/
package com.vmware.atlas.testworker.examples.messageclient;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.vmware.atlas.message.client.exception.MessageClientException;
import com.vmware.atlas.message.client.model.enums.MessageType;
import com.vmware.atlas.message.client.service.MessageClient;

import lombok.extern.slf4j.Slf4j;

/**
 * Simple example demonstrating pub sub messaging pattern using message client. You can have this example execute
 * automatically at service startup by uncommenting the @Component line below.
 *
 * IMPORTANT USAGE NOTE: Message client connection should be established *after* application's spring context is
 * fully initialized. The recommended approach is to execute {@code messageClient.connect()} in an application listener
 * that listens to {@link ApplicationReadyEvent}, like in this example.
 */
@Slf4j
//@org.springframework.stereotype.Component
public class MessageClientPubSubExample implements ApplicationListener<ApplicationReadyEvent> {

    private MessageClient messageClient;

    @Autowired
    public MessageClientPubSubExample(MessageClient messageClient) {
        this.messageClient = messageClient;
    }

    /**
     * This method demonstrates how to connect to message broker, subscribe to a topic, publish a message to the topic
     * and verify the same message was received back.
     * <p>
     * The call to message broker will be retried forever with 2 seconds delay between the calls.
     * @param event application ready event
     */
    @Override
    @Retryable(
            maxAttempts = Integer.MAX_VALUE,
            backoff = @Backoff(delay = 2000),
            include = {HttpServerErrorException.class, ResourceAccessException.class, MessageClientException.class})
    public void onApplicationEvent(ApplicationReadyEvent event) {

        // If testing this code in docker-compose, uncomment to allow message-broker to complete initialization
//        log.info("Sleeping till message broker becomes available");
//        try {
//            Thread.sleep(60000);
//        } catch (InterruptedException e) {
//        }

        final String testTopic = "test-topic";

        // Connect and subscribe to test topic
        // In a real service, you would probably just connect here and do the rest in business logic code

        try {
            messageClient.connect();
        } catch (Exception e) {
            log.warn("Message Client cannot connect to Message Broker because of: '{}'", e.getLocalizedMessage());
            throw e;
        }

        messageClient.subscribe(testTopic);

        // Add a listener to listen for the topic
        MyMessageListener listener = new MyMessageListener(testTopic);
        messageClient.addListeners(listener);

        // Send broadcast message 'hello' on topic
        messageClient.send(MessageType.EVENT, testTopic, "Hello broadcast cloud services.!");

        // Wait 5 seconds to receive message
        CountDownLatch latch = new CountDownLatch(1);
        try {
            latch.await(5, TimeUnit.SECONDS);
            log.info("Received messages: '{}'", listener.getMessages());

        } catch (InterruptedException e) {
            log.error("Interrupted while waiting for message");
        }
    }
}
