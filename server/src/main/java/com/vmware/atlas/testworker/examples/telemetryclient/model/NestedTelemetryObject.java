/*******************************************************************************
 * Copyright 2020 VMware, Inc.  All rights reserved.
 * -- VMware Confidential
 ******************************************************************************/

package com.vmware.atlas.testworker.examples.telemetryclient.model;

import com.vmware.atlas.telemetry.client.annotation.TelemetryTable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Telemetry object that is embedded in {@link TelemetryObject}
 */
@ToString
@Data
@TelemetryTable
@NoArgsConstructor
@AllArgsConstructor
public class NestedTelemetryObject {
    private String name;
}
