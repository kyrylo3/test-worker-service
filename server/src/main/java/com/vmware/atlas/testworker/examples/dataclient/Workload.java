/*******************************************************************************
 * Copyright 2019 VMware, Inc.  All rights reserved.
 *  -- VMware Confidential
 ******************************************************************************/
package com.vmware.atlas.testworker.examples.dataclient;

import lombok.Data;

/**
 * A sample data object representing a workload.
 */
@Data
public class Workload {
    private String id;
    private long memory;
    private String host;
}
