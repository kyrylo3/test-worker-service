/*
 * Copyright (c) 2019 VMware, Inc. All rights reserved. VMware Confidential
 */

package com.vmware.atlas.testworker.examples.orchestrationworker.sampleworker;

import java.util.UUID;

import org.springframework.stereotype.Component;

import com.vmware.atlas.executor.client.exception.ExecutorClientException;
import com.vmware.atlas.executor.client.model.bpmn.execution.ExecutionContext;
import com.vmware.atlas.executor.client.service.ExecutionDelegate;

import lombok.extern.slf4j.Slf4j;

/**
 * SampleWorkflowDelegate.
 */
@Slf4j
@Component
public class SampleWorkflowDelegate implements ExecutionDelegate {

    public static final String PROVISION_ID = "provision_id";

    @Override
    @SuppressWarnings("unchecked")
    public void execute(ExecutionContext context) {
        log.info("'SddcProvisioner' delegate has been called with context '{}'", context);
        validatePayload(context);

        SampleWorkflowPayload payload = validatePayload(context);
        log.info("SDDC {} is being provisioned", payload.getSddcId());
        UUID sddcId = payload.getSddcId();
        context.setVariable(PROVISION_ID, sddcId);
    }

    private SampleWorkflowPayload validatePayload(ExecutionContext context) {
        if (!context.hasPayload()) {
            log.error("Payload must be provided");
            throw new ExecutorClientException("Payload must be provided");
        }

        SampleWorkflowPayload payload = context.getPayload(SampleWorkflowPayload.class);

        if (payload == null) {
            throw new ExecutorClientException(String.format("Payload is not in good format. Some fields are missing."
                                                            + "Received payload %s", payload));
        }
        return payload;
    }


}
