/*******************************************************************************
 * Copyright 2019 VMware, Inc.  All rights reserved.
 *  -- VMware Confidential
 ******************************************************************************/

package com.vmware.atlas.testworker;


import java.net.InetAddress;
import java.net.UnknownHostException;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import com.vmware.atlas.library.auth.EnableAtlasAuthentication;

import com.vmware.atlas.message.client.annotation.EnableMessaging;

import com.vmware.atlas.executor.client.annotation.EnableExecutorClient;
import com.vmware.atlas.feature.client.annotation.EnableFeatureClient;

@EnableAtlasAuthentication
@EnableMessaging
@EnableExecutorClient
@com.vmware.atlas.telemetry.client.annotation.EnableTelemetry
@com.vmware.atlas.vip.EnableLocalization
@EnableCaching
@EnableFeatureClient
@SpringBootApplication
@Slf4j
public class TestWorkerApplication {

    public static void main(String[] args) {
        try {
            ConfigurableApplicationContext ctx = SpringApplication.run(TestWorkerApplication.class, args);

            // Readiness and Liveness:
            // Spring Boot 2.3 added an out of the box support for readiness and liveness. They are now part of the bean
            // lifecycle.
            // Readiness is automatically set to "ACCEPTING_TRAFFIC" when application startup tasks are finished.
            // If you need to perform some actions before readiness accepts traffic, use "ApplicationRunner" or
            // "CommandLineRunner" beans.
            // Please reference the probe states documentation for further details.
            // https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#actuator.endpoints.kubernetes-probes.lifecycle

            logApplicationStartup(ctx.getEnvironment());
        } catch (Throwable t) {
            t.printStackTrace();
            log.error("problem starting service: {}", t.getMessage(), t);
            System.exit(1);
        }
    }

    /**
     * Print a banner to indicate application has started.
     * @param env spring boot environment
     */
    private static void logApplicationStartup(Environment env) {
        String protocol = "http";
        if (env.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }

        String serverPort = env.getProperty("server.port");
        if (serverPort == null) {
            serverPort = "8080";
        }

        String contextPath = env.getProperty("server.servlet.context-path");
        if (contextPath == null || contextPath.isEmpty()) {
            contextPath = "/";
        }

        String hostAddress = "localhost";
        try {
            hostAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.warn("The host name could not be determined, using `localhost` as fallback");
        }
        log.info("\n----------------------------------------------------------\n\t" +
                        "Application is running! Access URLs:\n\t" +
                        "Local: \t\t{}://localhost:{}{}\n\t" +
                        "External: \t{}://{}:{}{}\n\t" +
                        "Profile(s): \t{}\n\t" +
                        "----------------------------------------------------------",
                protocol,
                serverPort,
                contextPath,
                protocol,
                hostAddress,
                serverPort,
                contextPath,
                getActiveProfiles(env));
    }

    /**
     * Get the profiles that are applied else get default profiles.
     *
     * @param env Spring environment
     * @return active profiles
     */
    private static String[] getActiveProfiles(Environment env) {
        String[] profiles = env.getActiveProfiles();
        if (profiles.length == 0) {
            return env.getDefaultProfiles();
        }
        return profiles;
    }
}
