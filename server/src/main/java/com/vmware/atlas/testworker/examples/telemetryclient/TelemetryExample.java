/*******************************************************************************
 * Copyright 2020 VMware, Inc.  All rights reserved.
 * -- VMware Confidential
 ******************************************************************************/

package com.vmware.atlas.testworker.examples.telemetryclient;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vmware.atlas.testworker.examples.telemetryclient.model.MapTelemetryObject;
import com.vmware.atlas.testworker.examples.telemetryclient.model.NestedTelemetryObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import com.vmware.atlas.testworker.examples.telemetryclient.model.TelemetryFeatureAdoptionData;
import com.vmware.atlas.telemetry.client.TelemetryClient;
import com.vmware.atlas.telemetry.client.model.TelemetryConverter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * You can have this example execute automatically at service startup by uncommenting the @Component line below and
 * change 'telemetry-client.enabled' property to true in application.yaml.
 *
 * This example demonstrates how a service can interact with telemetry client to send telemetry data.
 * You can see data sent in https://sql.vac.vmware.com/stg/history/index.html after 1 hour and 30 minutes,
 * using the query 'select * from service_starter_telemetry_adoption order by pa__arrival_day limit 5'.
 *
 * After some amount of data is already sent to the SuperCollider tables, reports can be created to visualize this data
 * in the form of statistics and to be of help for business decisions.
 * A guide and useful resources on creating such reports can be found in the following confluence page:
 * https://confluence.eng.vmware.com/display/ATLAS/Developer+guide+for+using+SuperCollider+Atlas+
 *         integration#DeveloperguideforusingSuperColliderAtlasintegration-Createreport
 *
 * A sample report based on the data from this example class can be found under:
 * https://app.mode.com/vmware_inc/reports/2361c2feec8b
 */
@Slf4j
//@Component
public class TelemetryExample implements ApplicationRunner {

    private TelemetryClient telemetryClient;
    private TelemetryConverter telemetryConverter;

    @Autowired
    public TelemetryExample(TelemetryClient telemetryClient, TelemetryConverter telemetryConverter) {
        this.telemetryClient = telemetryClient;
        this.telemetryConverter = telemetryConverter;
    }

    /**
     * This method demonstrates an example scenario of sending telemetry data.
     * The telemetry API have synchronous and asynchronous methods to send data.
     * Data can also be send as a batch - list of objects or one object at a time.
     * Please uncomment any example if you wan't to test it.
     * @param args application arguments
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("TelemetryExample execute examples");
        oneObjectSendExample();

        listOfObjectsSendExample();

        sendAsyncExample();

        mapSendExample();

    }

    /**
     * The telemetryClient example sending only one object
     */
    private void oneObjectSendExample() {
        TelemetryFeatureAdoptionData data = new TelemetryFeatureAdoptionData();
        data.setServiceName("test-worker-service");
        data.setAdoptionDate(OffsetDateTime.now());
        data.setExample("oneObjectSendExample");
        log.info(data.toString());
        telemetryClient.send(data);
    }

    /**
     * With the telemetryClient it is possible to send list of objects simultaneously
     * Every object from the list will be one row in the table.
     */
    private void listOfObjectsSendExample() {
        TelemetryFeatureAdoptionData first = new TelemetryFeatureAdoptionData();
        first.setServiceName("test-worker-service");
        first.setAdoptionDate(OffsetDateTime.now());
        first.setExample("listOfObjectsSendExample");

        TelemetryFeatureAdoptionData second = new TelemetryFeatureAdoptionData();
        second.setServiceName("test-worker-service");
        second.setAdoptionDate(OffsetDateTime.now());
        second.setExample("listOfObjectsSendExample");

        List<TelemetryFeatureAdoptionData> listOfData = new ArrayList<>();
        listOfData.add(first);
        listOfData.add(second);
        log.info(listOfData.toString());
        telemetryClient.send(listOfData);
    }

    /**
     * The telemetryClient example for sending data asynchronously
     */
    private void sendAsyncExample() {
        TelemetryFeatureAdoptionData data = new TelemetryFeatureAdoptionData();
        data.setServiceName("test-worker-service");
        data.setAdoptionDate(OffsetDateTime.now());
        data.setExample("sendAsyncExample");
        log.info(data.toString());
        telemetryClient.sendAsync(data);
    }

    /**
     * The telemetryClient example for sending map
     * Map is represented as a table in the db where the name of each column contains the key data.
     * If Nested telemetry object is defined as values type and contains one or more fields in it then
     * additional columns will be created for each one of the fields and the values will be recorded there.
     * The name of the corresponding field will be appended to the column name.
     * When changing the key type new column/s will be added to the table with name/s containing the new key data.
     * When adding new entry new column will be created and when removing existing one new record will be created in
     * the db with data of the values from the existing entries and empty space under the column for the removed one.
     */
    private void mapSendExample() {
        Map<String, NestedTelemetryObject> exampleMap = new HashMap<>();
        exampleMap.put("example_key1", new NestedTelemetryObject("example_value1"));
        exampleMap.put("example_key2", new NestedTelemetryObject("example_value2"));

        MapTelemetryObject mapTelemetryObject = new MapTelemetryObject();
        mapTelemetryObject.setMapOfObjects(exampleMap);
        log.info(mapTelemetryObject.toString());
        telemetryClient.send(mapTelemetryObject);
    }

}
