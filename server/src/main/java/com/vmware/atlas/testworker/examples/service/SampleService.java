/*******************************************************************************
 * Copyright 2019 VMware, Inc.  All rights reserved.
 *  -- VMware Confidential
 ******************************************************************************/
package com.vmware.atlas.testworker.examples.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.stereotype.Service;

import lombok.Data;

/**
 * A sample service that supports the test controller.
 */
@Service
public class SampleService {
    // In memory storage for resources
    private Map<UUID, SampleResource> resources = new HashMap<>();

    @Data
    public static class SampleResource {
        UUID id;
        String name;

        public SampleResource() {
        }

        public SampleResource(UUID id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    @Data
    public static class SampleResourceConfig {
        String name;
    }

    public SampleResource create(SampleResourceConfig resourceConfig) {
        SampleResource resource = new SampleResource(UUID.randomUUID(), resourceConfig.name);
        resources.put(resource.getId(), resource);
        return resource;
    }

    public SampleResource get(UUID id) {
        if (resources.containsKey(id)) {
            return resources.get(id);
        }
        return null;
    }

    public Collection<SampleResource> getAll() {
        return resources.values();
    }

    public SampleResource delete(UUID id) {
        if (resources.containsKey(id)) {
            return resources.remove(id);
        }
        return null;
    }
}
