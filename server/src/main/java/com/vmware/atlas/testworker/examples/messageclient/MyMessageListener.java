/*******************************************************************************
 * Copyright 2019 VMware, Inc.  All rights reserved.
 *  -- VMware Confidential
 ******************************************************************************/
package com.vmware.atlas.testworker.examples.messageclient;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import javax.validation.constraints.NotBlank;

import com.vmware.atlas.message.client.listener.impl.EventListener;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * Sample message listener that can receive string messages.
 */
@Slf4j
@Getter
public class MyMessageListener extends EventListener<String> {
    private List<String> messages = new ArrayList<>();

    public MyMessageListener(@NotBlank String topic) {
        super(topic, String.class);
    }

    /**
     * Callback that is invoked after a message is received.
     * @param payload received message payload
     */
    @Override
    public void onMessage(String payload) {
        log.info("Event listener received message {}", payload);
        messages.add(payload);
    }
}
