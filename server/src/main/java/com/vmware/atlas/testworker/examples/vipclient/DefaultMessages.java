/*******************************************************************************
 * Copyright 2020 VMware, Inc.  All rights reserved.
 * -- VMware Confidential
 ******************************************************************************/

package com.vmware.atlas.testworker.examples.vipclient;

/**
 * Class containing all default messages for service-starter.
 */
public class DefaultMessages {
    public static final String SUCCESSFULLY_CREATED = "The resource {0} is successfully created.";
    public static final String DELETE_NOT_POSSIBLE = "The resource {0} can't be deleted.";
    public static final String NOT_FOUND = "The resource {0} is not found.";
    public static final String STATE_READY = "READY";
}
