/*******************************************************************************
 * Copyright 2019 VMware, Inc.  All rights reserved.
 *  -- VMware Confidential
 ******************************************************************************/
package com.vmware.atlas.testworker.examples.dataclient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A sample data object representing a cluster.
 */
@Data
public class Cluster {
    private String id;
    private String name;
    private int version;
    private Organization organization;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Organization {
        String id;
        String name;
    }
}
