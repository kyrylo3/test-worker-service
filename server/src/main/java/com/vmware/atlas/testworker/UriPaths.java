/*******************************************************************************
 * Copyright 2019 VMware, Inc.  All rights reserved.
 *  -- VMware Confidential
 ******************************************************************************/
package com.vmware.atlas.testworker;

/**
 * Define all URI paths used by the application as static final strings in this class.
 */
public class UriPaths {

    public static final String SAMPLE_RESOURCE_URI = "/api/noauth/sample-resources";

    // Path for a sample resource that is protected by auth but not scoped by org id
    public static final String PROTECTED_RESOURCE_URI = "C:/Program Files/Git/api/testworker/sample-resources";


}
