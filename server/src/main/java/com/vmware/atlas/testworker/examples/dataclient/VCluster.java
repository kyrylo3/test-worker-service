package com.vmware.atlas.testworker.examples.dataclient;

import lombok.Data;

/**
 * A sample data object that has version field used for optimistic locking.
 */
@Data
public class VCluster {

    private String id;
    private String name;
    private long version;
}
