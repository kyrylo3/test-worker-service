/*******************************************************************************
 * Copyright 2019 VMware, Inc.  All rights reserved.
 *  -- VMware Confidential
 ******************************************************************************/
package com.vmware.atlas.testworker.examples.controller;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vmware.atlas.testworker.UriPaths;
import com.vmware.atlas.testworker.examples.service.SampleService;

/**
 * A sample REST controller that can be accessed without authorization.
 */
@RestController
public class SampleController {

    private SampleService sampleService;

    @Autowired
    public SampleController(SampleService sampleService) {
        this.sampleService = sampleService;
    }

    @PostMapping(UriPaths.SAMPLE_RESOURCE_URI)
    public ResponseEntity<SampleService.SampleResource> create(@RequestBody SampleService.SampleResourceConfig resourceConfig) {
        return new ResponseEntity<>(sampleService.create(resourceConfig), HttpStatus.OK);
    }

    @GetMapping(UriPaths.SAMPLE_RESOURCE_URI + "/{id}")
    public ResponseEntity<SampleService.SampleResource> get(@PathVariable UUID id) {
        SampleService.SampleResource result = sampleService.get(id);
        return result != null ? new ResponseEntity<>(result, HttpStatus.OK) : ResponseEntity.notFound().build();
    }

    @GetMapping(UriPaths.SAMPLE_RESOURCE_URI)
    public ResponseEntity<Collection<SampleService.SampleResource>> getAll() {
        return new ResponseEntity<>(sampleService.getAll(), HttpStatus.OK);
    }

    @DeleteMapping(UriPaths.SAMPLE_RESOURCE_URI + "/{id}")
    public ResponseEntity<SampleService.SampleResource> delete(@PathVariable UUID id) {
        SampleService.SampleResource result = sampleService.delete(id);
        return result != null ? new ResponseEntity<>(result, HttpStatus.OK) : ResponseEntity.notFound().build();
    }
}
