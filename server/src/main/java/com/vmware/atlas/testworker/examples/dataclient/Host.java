/*******************************************************************************
 * Copyright 2019 VMware, Inc.  All rights reserved.
 *  -- VMware Confidential
 ******************************************************************************/
package com.vmware.atlas.testworker.examples.dataclient;

import java.util.Collection;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * A sample data object representing a host.
 */
@Data
public class Host {
    // id is the identifier for the host
    @EqualsAndHashCode.Include
    private String id;
    private String os;
    private String app;
    private String metadata;
}
