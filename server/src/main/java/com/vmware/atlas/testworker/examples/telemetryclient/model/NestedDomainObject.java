/*******************************************************************************
 * Copyright 2020 VMware, Inc.  All rights reserved.
 * -- VMware Confidential
 ******************************************************************************/

package com.vmware.atlas.testworker.examples.telemetryclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Object that is embedded in {@link DomainObject}
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NestedDomainObject {
    @JsonProperty("name")
    private String name;
}
