/*******************************************************************************
 * Copyright 2020 VMware, Inc.  All rights reserved.
 * -- VMware Confidential
 ******************************************************************************/

package com.vmware.atlas.testworker.examples.telemetryclient.model;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

import lombok.Builder;
import lombok.Getter;

/**
 * The domain object is object already used in the service.
 * Some of the data of this object can be sent as telemetry, by converting this to a dedicated TelemetryObject.
 */
@Getter
@Builder
public class DomainObject {
    private UUID id;
    private String name;
    private OffsetDateTime date;
    private OffsetDateTime dateAsString;
    private int[] arrayOfPrimitives;
    private NestedDomainObject[] arrayOfObjects;
    private List<NestedDomainObject> listOfObjects;
    private NestedDomainObject nestedObject;
}
