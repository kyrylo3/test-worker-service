/*******************************************************************************
 * Copyright 2020 VMware, Inc.  All rights reserved.
 * -- VMware Confidential
 ******************************************************************************/

package com.vmware.atlas.testworker.examples.telemetryclient;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import com.vmware.atlas.testworker.examples.telemetryclient.model.DomainObject;
import com.vmware.atlas.testworker.examples.telemetryclient.model.NestedDomainObject;
import com.vmware.atlas.testworker.examples.telemetryclient.model.TelemetryObject;
import com.vmware.atlas.telemetry.client.TelemetryClient;
import com.vmware.atlas.telemetry.client.model.TelemetryConverter;

import lombok.extern.slf4j.Slf4j;

/**
 * You can have this example execute automatically at service startup by uncommenting the @Component line below and
 * change 'telemetry-client.enabled' property to true in application.yaml.
 *
 * The recommended pattern is to have separated domain and telemetry objects and to not use your domain objects directly in the telemetry.
 * For that reason a dedicated TelemetryObject is introduced, having all data that has to be send as telemetry.
 * TelemetryObject fields that have same names as domain object fields. To facilitate this separation, the telemetry-library
 * has 'telemetryConverter' that can automatically convert domain object to specified telemetry object.
 * This example demonstrates:
 *  - how to use 'telemetryConverter' to convert existing domain object to specific telemetry object.
 *  - how different types of data can be sent and how they will be represented as tables and columns in SuperCollider system.
 *
 * One can see data sent in https://sql.vac.vmware.com/stg/history/index.html after 1 hour and 30 minutes, using the queries:
 * 'select * from service_starter_conversion_examples order by pa__arrival_day order by pa__arrival_day limit 5' - that is the root table.
 * 'select * from service_starter_conversion_examples_array_of_objects order by pa__arrival_day limit 5'
 * 'select * from service_starter_conversion_examples_list_of_objects order by pa__arrival_day limit 5'
 *
 * After some amount of data is already sent to the SuperCollider tables, reports can be created to visualize this data
 * in the form of statistics and to be of help for business decisions.
 * A guide and useful resources on creating such reports can be found in the following confluence page:
 * https://confluence.eng.vmware.com/display/ATLAS/Developer+guide+for+using+SuperCollider+Atlas+
 *         integration#DeveloperguideforusingSuperColliderAtlasintegration-Createreport
 *
 * A sample report based on the data from this example class can be found under:
 * https://app.mode.com/vmware_inc/reports/4dd90554ef60
 */
@Slf4j
//@org.springframework.stereotype.Component
public class TelemetryConversionExamples implements ApplicationRunner {

    private TelemetryClient telemetryClient;
    private TelemetryConverter telemetryConverter;

    @Autowired
    public TelemetryConversionExamples(TelemetryClient telemetryClient, TelemetryConverter telemetryConverter) {
        this.telemetryClient = telemetryClient;
        this.telemetryConverter = telemetryConverter;
    }

    /**
     * This method demonstrates an example scenario of:
     *  - how to use 'telemetryConverter' to convert existing domain object to specific telemetry object.
     *  - how different types of data can be sent and how they will be represented as tables and columns in SuperCollider system.
     * @param args application arguments
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("TelemetryConversionExamples execute examples");

        DomainObject.DomainObjectBuilder domainObject = DomainObject.builder();
        domainObject.id(UUID.randomUUID());
        OffsetDateTime now = OffsetDateTime.now();
        // date will be sent as timestamp in milliseconds by default, calculated based on current timezone
        domainObject.date(now);
        // date will be sent as string, this is not the default behaviour and the telemetry object field has @JsonSerialize(using = ToStringSerializer.class)
        domainObject.dateAsString(now);
        // array of primitives will be flattened and stored in the same table as the root object
        domainObject.arrayOfPrimitives(new int[]{1,2,3});
        // array of objects will be stored in new table, that will have reference to the root object table
        domainObject.arrayOfObjects(new NestedDomainObject[]{new NestedDomainObject("arrayObject1"), new NestedDomainObject("arrayObject2")});
        // the data of the nested object will be flattened and stored in the same table as the root object
        domainObject.nestedObject(new NestedDomainObject("nestedDomainObject"));
        // list of objects will be stored in a new table, that will have reference to the root object table
        List<NestedDomainObject> listOfObjects = new ArrayList<>();
        listOfObjects.add(new NestedDomainObject("listObject1"));
        listOfObjects.add(new NestedDomainObject("listObject2"));
        domainObject.listOfObjects(listOfObjects);

        // automatic conversion from DomainObject to TelemetryObject
        // Note : The domainObject has to have @Getter for automatic conversion to work properly
        TelemetryObject telemetryObject = telemetryConverter.convertTo(domainObject.build(), TelemetryObject.class);

        log.info(telemetryObject.toString());
        telemetryClient.sendAsync(telemetryObject);

    }

}

