/*
 * Copyright (c) 2019 VMware, Inc. All rights reserved. VMware Confidential
 */

package com.vmware.atlas.testworker.examples.orchestrationworker.sampleworker;

import java.io.Serializable;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * Sample Payload class.
 */
@Data
public class SampleWorkflowPayload implements Serializable {

    @NotNull
    UUID orgId;

    @NotNull
    UUID sddcId;

    @NotNull
    SddcConfig sddcConfig;

    /**
     * Location.
     */
    @Data
    public static class Location implements Serializable {

        String code;
    }

    /**
     * SddcConfig.
     */
    @Data
    public static class SddcConfig implements Serializable {

        @NotNull
        Location location;

        @NotNull
        String name;

        @NotNull
        String deploymentType;

        @NotNull
        String providerType;

        @NotNull
        String hostType;

        @NotNull
        Integer hostCount;
    }
}
