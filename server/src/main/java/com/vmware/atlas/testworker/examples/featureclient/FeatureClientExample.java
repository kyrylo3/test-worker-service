/*******************************************************************************
 * Copyright 2020 VMware, Inc.  All rights reserved.
 *  -- VMware Confidential
 ******************************************************************************/
package com.vmware.atlas.testworker.examples.featureclient;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import com.vmware.atlas.feature.client.FeatureClient;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * This example demonstrates how a service can interact with feature service to read applicability status of the features.
 *
 * You can have this example execute automatically at service startup by uncommenting the @Component line below and
 * updating the CHANGEME values in application.yaml.
 */
@Slf4j
//@org.springframework.stereotype.Component
@RequiredArgsConstructor
public class FeatureClientExample implements ApplicationRunner {

    private static final String SAMPLE_FEATURE = "sample_feature";

    private final FeatureClient featureClient;

    /**
     * This method demostrates an example of control of the method execution flow, based on the feature applicability.
     * @param args application arguments
     */
    @Override
    public void run(ApplicationArguments args) {
        log.info("Start feature client example method execution.");

        log.info("Will check applicability of the feature {}", SAMPLE_FEATURE);

        if (featureClient.isFeatureApplicable(SAMPLE_FEATURE)) {
            log.info("Feature {} is applicable.", SAMPLE_FEATURE);
        } else {
            log.info("Feature {} is not applicable.", SAMPLE_FEATURE);
        }
    }
}
