/*******************************************************************************
 * Copyright 2020 VMware, Inc.  All rights reserved.
 * -- VMware Confidential
 ******************************************************************************/

package com.vmware.atlas.testworker.examples.telemetryclient.model;

import java.time.OffsetDateTime;
import java.util.UUID;

import com.vmware.atlas.telemetry.client.annotation.TelemetryId;
import com.vmware.atlas.telemetry.client.annotation.TelemetryTable;

import lombok.Data;

/**
 * Telemetry data stored for telemetry adoption.
 */
@Data
@TelemetryTable("service_starter_telemetry_adoption")
public class TelemetryFeatureAdoptionData {
    @TelemetryId
    private UUID id = UUID.randomUUID();
    private String serviceName;
    private OffsetDateTime adoptionDate;
    private String example;
}
