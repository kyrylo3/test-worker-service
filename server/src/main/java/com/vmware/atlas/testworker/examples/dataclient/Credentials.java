/*
 * Copyright (c) 2021 VMware, Inc. All rights reserved. VMware Confidential
 */

package com.vmware.atlas.testworker.examples.dataclient;

import lombok.Data;

@Data
public class Credentials {
    private String id;
    private String username;
    private String orgId;
    private String password;
}
