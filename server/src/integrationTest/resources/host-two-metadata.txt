{
   "id": "enableUniqueClusterName",
   "enable": false,
   "description": "Enables unique cluster names across SDDCs within an Org",
   "applicable_scope": {
       "allowed_keys": [
           "ef73d77f-0e0d-4d7e-9119-8f3636bacc92",
           "870a65d0-e752-4ea0-be73-8d81ea2ca30f",
           "68142688-7dec-4e85-a968-c5f402e2e86c",
           "28665df2-5c74-41fa-8ccb-0cd96d02d9d8",
           "e34bb619-72ed-43e1-85a3-72312ca88c77",
           "0b50fcc6-d736-4588-9495-db0838c3c161",
           "494bcbe6-4126-4822-94a7-0886cd7e65c7",
           "439d2c5a-b5db-4ce1-bc15-b713470f53ac",
           "ac976dd6-8651-4de2-b13e-cebae53dcfb4",
           "0b6069b5-bd8f-4d55-8d14-c393995edc93",
           "b7c97de4-082b-45eb-82ee-254480bb36ab"
       ],
       "denied_keys": [
           "e8ee77a0-5185-402d-8f62-87e491704df7"
       ],
       "rollout_percent": 100
   },
   "type": "CUSTOMER",
   "category": "SERVICE",
   "toggle_type": "TARGET_BY_SCOPE",
   "namespace": "ss",
   "state": "ACTIVE",
   "approvers": null
}
