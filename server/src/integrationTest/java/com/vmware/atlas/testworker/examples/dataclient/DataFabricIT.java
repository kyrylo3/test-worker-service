package com.vmware.atlas.testworker.examples.dataclient;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.data.domain.Sort.Direction.ASC;

import java.time.Instant;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import com.vmware.symphony.csp.auth.WithCspUser;
import com.vmware.symphony.csp.auth.model.CspAuthToken;
import com.vmware.symphony.csp.auth.service.CspClient;
import com.vmware.symphony.testing.CspSecurityContextBuilder;
import com.vmware.symphony.testing.JwtTestUtils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.util.Assert;
import org.springframework.util.StreamUtils;
import org.testcontainers.containers.Network;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.vmware.atlas.containers.CredentialServiceContainer;
import com.vmware.atlas.containers.DataFabricContainer;
import com.vmware.atlas.containers.MariaDBContainer;
import com.vmware.atlas.data.client.DataFabric;
import com.vmware.atlas.data.client.SearchRequestBuilder;
import com.vmware.atlas.data.client.exception.DataFabricClientException;
import com.vmware.atlas.data.client.exception.DataFabricClientOptimisticLockingException;
import com.vmware.atlas.data.client.model.BulkUpdateRequest;
import com.vmware.atlas.data.client.model.BulkUpdateResponse;
import com.vmware.atlas.data.client.model.Criterion;
import com.vmware.atlas.data.client.model.Filter;
import com.vmware.atlas.data.client.spring.DataFabricClientFactory;

import lombok.extern.slf4j.Slf4j;

/**
 * DataFabricIT (IntegrationTest) is a fully embedded Data Fabric test.  Data Fabric is launched through
 * testcontainers integration using 'dev-deployed' imageTag by default
 * <br><br>
 * Notes:
 * <ul>
 *     <li>All logging associated with the container is driven by logback-test.xml and is prepended with 'data-fabric-container -'</li>
 *     <li>To add debugging in the data-fabric-container use logLevel("DEBUG") in DataFabricOptions</li>
 *     <li>There are several options to mutate data-fabric within the container, if you feel something is missing please let us know - #atlas-data-fabric</li>
 * </ul>
 */
@Slf4j
@Testcontainers
@SpringBootTest(classes = DataFabricIT.MyConfiguration.class)
@TestPropertySource(properties = {
    "atlas.data.auth-enabled=true",
    "atlas.data.enable-schema-init=true",
    "atlas.data.enable-setup=true",
    "atlas.data.namespace=test_it",
    "atlas.data.type-locations[0].package-name=com.vmware.atlas.testworker.examples.dataclient",
    "atlas.data.type-locations[0].naming-policy=ClassName",
    "atlas.data.type-locations[0].includes[0]=.*Cluster.*|.*Host.*|.*Workload.*|.*Credentials.*|.*AuditedPerson.*",
    "atlas.data.type-locations[0].excludes[0]=.*DataOperationsExample$",
    "atlas.data.encryption.enabled=true",
    "atlas.data.encryption.auth-enabled=false",
    "atlas.messages.enabled=false",
    "atlas.orchestration.enabled=false"
})
@EnableAutoConfiguration
@WithCspUser(value = "defaultUser", cspOrgId = "orgId")
class DataFabricIT {
    private final static Network network = Network.newNetwork();

    private static final int DATA_FABRIC_PORT = 9090;
    private static final int CREDENTIAL_SERVICE_PORT = 8091;
    private static final String CREDENTIAL_SERVICE_IMAGE = "dev-deployed";

    @Container
    private final static MariaDBContainer mariaDBContainer = new MariaDBContainer(network);

    @Container
    private final static DataFabricContainer dataFabricContainer = new DataFabricContainer(
        DataFabricContainer.DataFabricOptions.builder()
                .serverPort(DATA_FABRIC_PORT)
                .network(network)
                .dependencies(Arrays.asList(mariaDBContainer))
                .outputLogger(LoggerFactory.getLogger("com.vmware.atlas.data.container"))
                .build()
    );

    @Container
    private static CredentialServiceContainer credentialServiceContainer = new CredentialServiceContainer(
            CredentialServiceContainer.CredentialServiceOptions.builder()
                    .outputLogger(LoggerFactory.getLogger("com.vmware.atlas.credentialservice.container"))
                    .imageTag(CREDENTIAL_SERVICE_IMAGE)
                    .serverPort(CREDENTIAL_SERVICE_PORT)
                    .network(network)
                    .dependencies(Collections.emptyList())
                    .build()
    ).withEnv("SECRETS_PROVIDER", "PSUEDO").withEnv("SPRING_PROFILES_ACTIVE", "local");

    @Autowired
    private ResourceLoader resourceLoader;

    private DataFabric dataFabric;

    @DynamicPropertySource
    static void dataFabricProperties(DynamicPropertyRegistry registry) {
        registry.add("atlas.data.url", () -> "http://localhost:" +
                dataFabricContainer.getMappedPort(DATA_FABRIC_PORT));
        registry.add("atlas.data.encryption.url",
                     () -> "http://localhost:" + credentialServiceContainer.getMappedPort(CREDENTIAL_SERVICE_PORT) + "/api/credentials/v1");
    }

    @BeforeEach
    void beforeEach(@Autowired DataFabricClientFactory dataFabricClientFactory) {
        this.dataFabric = dataFabricClientFactory.getWithAuthModeRequest();
    }

    /**
     * This method demonstrates an example scenario of creating, updating and deleting various types of data objects
     * using data fabric client library.
     */
    @Test
    void testIt(@Autowired RetryableUpdate retryableUpdate) throws Exception {
        basicOperations();
        transactionalBulkOperations();
        nonTransactionalBulkOperations();
        optimisticLockingExample(retryableUpdate);
        jsonSearch();
        secretEncryptionExample();
        examplesOfHandlingAuditFieldsWhenAuditDataIsEnabled();
    }

    /**
     * These examples showcase the Audit Data feature. For the feature to be enabled you need to set the
     * "auditing_enabled" flag to true in your type's schema definition. See the example schema definition in
     * 'server/src/main/resources/datafabric-schema/06_auditedperson.json'
     *
     * Detailed documentation of the Audit Data feature can be found here
     * https://gitlab.eng.vmware.com/atlas/atlas-framework/-/blob/master/data-fabric-client/README.md
     */
    public void examplesOfHandlingAuditFieldsWhenAuditDataIsEnabled() {
        testAuditDataOnCreate();
        testAuditDataOnUpdate();
        testAuditDataCannotManuallyUpdateAuditFields();
        testAuditDataAuditFieldsAreSearchableAndSortable();
    }

    void testAuditDataOnCreate() {
        Instant beforeCreate = Instant.now();
        AuditedPerson person = new AuditedPerson();
        person.setName("Luffy");
        person.setAge(19);
        String username = "someone.very.important@vmware.com";
        authenticateAs(username);

        person = dataFabric.createAndGet(person);
        Instant afterCreate = Instant.now();

        assertThat(person.getDfCreatedBy()).isEqualTo(person.getDfLastUpdatedBy()).isEqualTo(username);
        assertThat(person.getDfCreatedTimestamp()).isEqualTo(person.getDfLastUpdatedTimestamp());
        Instant createTime = Instant.ofEpochMilli(person.getDfCreatedTimestamp());
        assertThat(beforeCreate).isBeforeOrEqualTo(createTime).isBeforeOrEqualTo(afterCreate);
    }

    void testAuditDataOnUpdate() {
        Instant beforeCreate = Instant.now();
        AuditedPerson person = new AuditedPerson();
        person.setName("Luffy");
        person.setAge(19);
        String creator = "creator@vmware.com";
        authenticateAs(creator);
        person = dataFabric.createAndGet(person);
        Instant afterCreate = Instant.now();
        person.setAge(20);
        String updater = "updater@vmware.com";
        authenticateAs(updater);

        person = dataFabric.update(person);
        Instant afterUpdate = Instant.now();

        assertThat(person.getDfCreatedBy()).isNull();
        assertThat(person.getDfCreatedTimestamp()).isNull();
        assertThat(person.getDfLastUpdatedBy()).isEqualTo(updater);
        Instant lastUpdateTime = Instant.ofEpochMilli(person.getDfLastUpdatedTimestamp());
        assertThat(afterCreate).isBeforeOrEqualTo(lastUpdateTime).isBeforeOrEqualTo(afterUpdate);

        person = dataFabric.get(AuditedPerson.class, person.getId());
        assertThat(person.getDfCreatedBy()).isEqualTo(creator);
        assertThat(person.getDfLastUpdatedBy()).isEqualTo(updater);
        Instant createTime = Instant.ofEpochMilli(person.getDfCreatedTimestamp());
        lastUpdateTime = Instant.ofEpochMilli(person.getDfLastUpdatedTimestamp());
        assertThat(beforeCreate).isBeforeOrEqualTo(createTime).isBeforeOrEqualTo(afterCreate);
        assertThat(afterCreate).isBeforeOrEqualTo(lastUpdateTime).isBeforeOrEqualTo(afterUpdate);
    }

    void testAuditDataCannotManuallyUpdateAuditFields() {
        Instant beforeCreate = Instant.now();
        AuditedPerson person = new AuditedPerson();
        person.setName("Luffy");
        person.setAge(19);
        String creator = "creator@vmware.com";
        authenticateAs(creator);
        person = dataFabric.createAndGet(person);
        Instant afterCreate = Instant.now();
        person.setAge(20);
        String updater = "updater@vmware.com";
        authenticateAs(updater);

        // manually set audit fields
        person.setDfCreatedBy("hacker@anonymous.io");
        person.setDfCreatedTimestamp(1l);
        person.setDfLastUpdatedBy("Zero Cool");
        person.setDfLastUpdatedTimestamp(-1005l);

        dataFabric.update(person);
        Instant afterUpdate = Instant.now();

        // manually set values of audit fields have no effect
        person = dataFabric.get(AuditedPerson.class, person.getId());
        assertThat(person.getDfCreatedBy()).isEqualTo(creator);
        assertThat(person.getDfLastUpdatedBy()).isEqualTo(updater);
        Instant createTime = Instant.ofEpochMilli(person.getDfCreatedTimestamp());
        Instant lastUpdateTime = Instant.ofEpochMilli(person.getDfLastUpdatedTimestamp());
        assertThat(beforeCreate).isBeforeOrEqualTo(createTime).isBeforeOrEqualTo(afterCreate);
        assertThat(afterCreate).isBeforeOrEqualTo(lastUpdateTime).isBeforeOrEqualTo(afterUpdate);
    }

    void testAuditDataAuditFieldsAreSearchableAndSortable() {
        String creator = "creator@vmware.com";
        authenticateAs(creator);
        Instant middleOfCreates = null;
        for (int i = 0; i < 50; i++) {
            AuditedPerson person = new AuditedPerson();
            person.setName("Nameless");
            person.setAge(36);
            dataFabric.create(person);
            if (i == 24) {
                middleOfCreates = Instant.now();
            }
        }

        SearchRequestBuilder searchBuilder = SearchRequestBuilder.builder()
                .filter(new Filter(Criterion.equals("df_created_by", creator),
                        Criterion.greaterThan("df_created_timestamp", middleOfCreates.toEpochMilli()),
                        Criterion.equals("df_last_updated_by", creator),
                        Criterion.greaterThan("df_last_updated_timestamp", middleOfCreates.toEpochMilli())))
                .sort(Sort.by("df_created_by", "df_created_timestamp", "df_last_updated_by",
                        "df_last_updated_timestamp"))
                .paginate(0, 50);
        Slice<AuditedPerson> persons = dataFabric.search(AuditedPerson.class, searchBuilder.buildSearchRequest());

        assertThat(persons.getContent().size()).isEqualTo(25);
    }

    private void basicOperations() throws Exception {
        Cluster cluster = new Cluster();
        cluster.setName("cluster-abc");
        cluster.setOrganization(new Cluster.Organization("org-id-1", "Org 1"));
        cluster.setVersion(1);

        // Cluster object type has a generated id field "id". create() will return the generated id
        String clusterId = this.dataFabric.create(cluster).orElseThrow(RuntimeException::new);
        cluster.setId(clusterId);

        Cluster read = this.dataFabric.get(Cluster.class, clusterId);
        log.info("Created cluster 1: {}", read);

        Host host = new Host();
        host.setId(createNewId());
        host.setOs("CentOS");
        host.setApp("httpd");

        // Host object type has a provided id field "id". create() will not return this id
        this.dataFabric.create(cluster, host);

        Host byId = this.dataFabric.get(Host.class, host.getId());
        log.info("Created host 1: {}", byId);

        Slice<Host> childHostsOfACluster = this.dataFabric.getChildren(Host.class, cluster, PageRequest.of(0, 2));
        log.info("Children of cluster 1: {}", childHostsOfACluster);

        Cluster parentForHost = this.dataFabric.getParent(Cluster.class, host);
        log.info("Parent cluster of host 1: {}", parentForHost);

        cluster.setVersion(2);
        this.dataFabric.update(cluster);
        read = this.dataFabric.get(Cluster.class, clusterId);
        log.info("Updated cluster 1: {}", read);

        Filter filter = new Filter(Criterion.equals("app", "httpd"));
        // Page search is slower since it executes a count query in addition to retrieving the page elements.
        // This method should be used only if retrieving total count is required, otherwise use Slice search.
        int pageSize = 10;
        int page = 0;
        Collection<Host> hosts = new ArrayList<>();
        Page<Host> hostResults;
        do {
            hostResults = this.dataFabric.search(Host.class,
                SearchRequestBuilder.builder()
                    .filter(filter)
                    .parents(cluster)
                    .sort(Sort.by(ASC, "os"))
                    .paginate(page++, pageSize)
                    .buildPageSearchRequest());
            hosts.addAll(hostResults.getContent());
        } while(hostResults.hasNext());
        log.info("Host search result: {}", hosts);

        // we can perform one level of join by passing <child>.<field>=<value> in the filter
        Slice<Cluster> joinQueryResult = this.dataFabric.search(Cluster.class,
                                     SearchRequestBuilder.builder()
                                             .filter(new Filter(Criterion.equals("hosts.app","httpd")))
                                             .paginate(0, 2)
                                             .buildPageSearchRequest());
        log.info("clusters with children host having field app {} : {}", "httpd", joinQueryResult.get().collect(toList()));
        assertThat(joinQueryResult.get().count()).isEqualTo(1);
        assertThat(joinQueryResult.getContent().get(0)).isEqualTo(cluster);

        this.dataFabric.delete(host);
        log.info("Deleted host 1");

        // Slice search is generally recommended to use, because it is faster and more efficient than Page search.
        Collection<Workload> workloads = new ArrayList<>();
        Slice<Workload> workloadResults;
        pageSize = 100;
        page = 0;
        do {
            workloadResults = this.dataFabric.search(Workload.class,
                    SearchRequestBuilder.builder()
                            .parents(cluster)
                            .paginate(page++, pageSize)
                            .buildSearchRequest());
            workloads.addAll(workloadResults.getContent());
        } while (workloadResults.hasNext());
        log.info("Workloads under cluster 1: {}", workloads);

        Cluster clusterTwo = new Cluster();
        clusterTwo.setName("cluster-two");
        cluster.setOrganization(new Cluster.Organization("org-id-2", "Org 2"));
        clusterTwo.setVersion(1);
        // Save an object and get the created record at once
        clusterTwo = this.dataFabric.createAndGet(clusterTwo);
        log.info("Created cluster 2: {}", clusterTwo);
        Cluster cluster2 = this.dataFabric.get(Cluster.class, clusterTwo.getId());
        Assert.notNull(cluster2, "cluster should not be null");

        Resource resource;
        resource = this.resourceLoader.getResource("classpath:host-two-metadata.txt");
        String hostTwoMetadata = StreamUtils.copyToString(resource.getInputStream(), StandardCharsets.UTF_8);

        resource = this.resourceLoader.getResource("classpath:host-three-metadata.txt");
        String hostTheeMetadata = StreamUtils.copyToString(resource.getInputStream(), StandardCharsets.UTF_8);

        Host hostTwo = new Host();
        hostTwo.setId(createNewId());
        hostTwo.setOs("Ubuntu");
        hostTwo.setApp("tester");
        hostTwo.setMetadata(hostTwoMetadata);
        this.dataFabric.create(clusterTwo, "hosts", hostTwo);
        log.info("Created host 2: {}", hostTwo);

        Host hostThree = new Host();
        hostThree.setId(createNewId());
        hostThree.setOs("Linux");
        hostThree.setApp("tester");
        hostThree.setMetadata(hostTheeMetadata);
        this.dataFabric.create(hostThree);
        log.info("Created host 3: {}", hostThree);

        // Get the count of Hosts by app using the CountRequest
        long countRequestResult = this.dataFabric.count(Host.class,
                                       SearchRequestBuilder.builder()
                                               .filter(new Filter(Criterion.equals("app","tester")))
                                               .paginate(0, 2)
                                               .buildCountRequest());

        log.info("Hosts by app {}: {}", "tester", countRequestResult);
        assertThat(countRequestResult).isEqualTo(2);

        Slice<Host> hostChildren = this.dataFabric.getChildren(Host.class, clusterTwo, PageRequest.of(0, 2));
        Assert.isTrue(hostChildren.getContent().contains(hostTwo), "hostChildren should contain hostTwo");
        log.info("Child hosts of cluster 2: {}", hostChildren);

        hostChildren = this.dataFabric.getChildren(Host.class, cluster, PageRequest.of(0, 1));
        log.info("Child hosts for cluster1 (should be empty): {}", hostChildren);

        // NOTE - attach() operation has very high overhead compared to
        // create(parent, parentFieldName, entity) (or any equivalent).  DO NOT use attach() for any code that
        // requires a high rate of throughput
        this.dataFabric.attach(cluster, hostTwo);
        log.info("Attached host 2 to cluster 1");

        Workload workload = new Workload();
        workload.setMemory(ThreadLocalRandom.current().nextInt(256));

        // Workload object type has a generated id field "id". create() will return the generated id
        workload.setId(this.dataFabric.create(hostTwo, workload).orElseThrow(RuntimeException::new));
        log.info("Created workload child for host 2: {}", workload);

        workloadResults = this.dataFabric.search(Workload.class,
                SearchRequestBuilder.builder()
                        .parents(clusterTwo)
                        .paginate(0, 2)
                        .buildSearchRequest());
        log.info("Descendent workloads of cluster 2: {}", workloadResults.get().collect(toList()));

        workloadResults = this.dataFabric.search(Workload.class,
                SearchRequestBuilder.builder()
                        .parents(cluster)
                        .paginate(0, 1)
                        .buildSearchRequest());
        log.info("Descendent workloads of cluster 1 (should be empty): {}", workloadResults.get().collect(toList()));

        Slice<Cluster> clusters = this.dataFabric.search(Cluster.class,
                SearchRequestBuilder.builder()
                        .filter(new Filter(Criterion.equals("name", "cluster-two")))
                        .paginate(0, 2)
                        .buildSearchRequest());
        log.info("clusters by name {}: {}", "cluster-two", clusters.get().collect(toList()));
        clusters = this.dataFabric.search(Cluster.class,
                SearchRequestBuilder.builder()
                        .filter(new Filter(Criterion.equals("org.id", "org-id-1")))
                        .paginate(0, 2)
                        .buildSearchRequest());
        log.info("clusters by org.id {}: {}", "org-id-1", clusters.get().collect(toList()));

        // The following two searches will throw DataFabricClientException!
        // Reason for failure is one of the following conditions
        // 1. Filtering is done over a field that is not marked as "searchable: true"
        // 2. Sorting is done over a field that is not marked as "sortable: true"

        assertThatThrownBy(() -> this.dataFabric.search(Cluster.class,
                SearchRequestBuilder.builder()
                        .filter(new Filter(Criterion.equals("valuer", "whateverValue")))
                        .buildSearchRequest())
                ).isInstanceOf(DataFabricClientException.class);

        assertThatThrownBy(() -> this.dataFabric.search(Cluster.class,
                SearchRequestBuilder.builder()
                        .sort(Sort.by("id"))
                        .buildSearchRequest())
                ).isInstanceOf(DataFabricClientException.class);

        // The following is an example for values containing ':' or other special characters in the search criteria.
        // The value in the Criterion is encoded by the data-fabric client.
        Cluster clusterThree = new Cluster();
        clusterThree.setName("cluster-three");
        clusterThree.setOrganization(new Cluster.Organization("org-id:1", "org-1"));
        clusterThree.setVersion(1);
        clusterThree = this.dataFabric.createAndGet(clusterThree);
        log.info("Created cluster 3: {}", clusterThree);

        clusters = this.dataFabric.search(Cluster.class,
                                                       SearchRequestBuilder
                                                               .builder()
                                                               .filter(new Filter(Criterion.equals("org.id", "org-id:1")))
                                                               .buildSearchRequest());

        log.info("clusters by org.id {}: {}", "org-id:1", clusters.get().collect(toList()));
        assertThat(clusters.getContent()).contains(clusterThree);
    }

    private String createNewId() {
        return UUID.randomUUID().toString();
    }

    private void transactionalBulkOperations() {
        Cluster abomination = new Cluster();
        abomination.setName("abomination");
        abomination.setOrganization(new Cluster.Organization("org-id-abomination", "Emil"));
        abomination.setVersion(0);
        String id = this.dataFabric.create(abomination).orElseThrow(IllegalStateException::new);
        abomination.setId(id);
        abomination.setVersion(1);
        abomination.setOrganization(new Cluster.Organization("org-id-abomination", "Emil Blonsky"));

        Cluster sheHulk = new Cluster();
        sheHulk.setName("sheHulk");
        sheHulk.setOrganization(new Cluster.Organization("org-id-she-hulk", "Jennifer"));
        sheHulk.setVersion(0);
        id = this.dataFabric.create(sheHulk).orElseThrow(IllegalStateException::new);
        sheHulk.setId(id);
        sheHulk.setVersion(1);

        Cluster redHulk = new Cluster();
        redHulk.setName("redHulk");
        redHulk.setOrganization(new Cluster.Organization("org-id-red-hulk", "Thaddeus"));
        redHulk.setVersion(0);
        id = this.dataFabric.create(sheHulk).orElseThrow(IllegalStateException::new);
        redHulk.setId(id);
        redHulk.setVersion(1);

        Cluster hulk = new Cluster();
        hulk.setName("hulk");
        hulk.setOrganization(new Cluster.Organization("org-id-hulk", "Bruce"));
        hulk.setVersion(0);

        Host leader = new Host();
        leader.setApp("Leader");
        leader.setId(createNewId());
        leader.setOs("Solaris 2.6.1");

        Host thanos = new Host();
        thanos.setApp("Thanos");
        thanos.setId(createNewId());
        thanos.setOs("Solaris 8");

        BulkUpdateRequest bulkUpdateRequest = new BulkUpdateRequest(createNewId())
            .create(Cluster.class.getSimpleName(), hulk)
            .createOrUpdate(thanos)
            .createChild(abomination, leader)
            .update(sheHulk)
            .delete(redHulk);
        BulkUpdateResponse response = this.dataFabric.bulkUpdate(bulkUpdateRequest);
        List<BulkUpdateResponse.CreateResult> createResults = response.getCreateResults();
        hulk = this.dataFabric.get(Cluster.class, createResults.get(0).getId());
        assertThat(hulk.getName()).as("The hulk has a name and it is Bruce Banner").isEqualTo("hulk");
        log.info("hulk: {}", hulk);

        leader = this.dataFabric.get(Host.class, createResults.get(1).getId());
        assertThat(leader.getApp()).as("This thing is the Leader").isEqualTo("Leader");
        log.info("leader: {}", leader);

        List<BulkUpdateResponse.CreateOrUpdateResult> createOrUpdateResults = response.getCreateOrUpdateResults();
        thanos = this.dataFabric.get(Host.class, createOrUpdateResults.get(0).getId());
        assertThat(thanos.getApp()).as("Josh Brolin played Thanos").isEqualTo("Thanos");
        log.info("thanos: {}", thanos);

        List<BulkUpdateResponse.UpdateResult> updateResults = response.getUpdateResults();
        sheHulk = this.dataFabric.get(Cluster.class, updateResults.get(0).getId());
        assertThat(sheHulk.getName()).as("Jennifer Susan Walters is Bruce's cousin").isEqualTo("sheHulk");
        log.info("sheHulk: {}", sheHulk);

        List<BulkUpdateResponse.DeleteResult> deleteResults = response.getDeleteResults();
        redHulk = this.dataFabric.get(Cluster.class, deleteResults.get(0).getId());
        assertThat(redHulk).as("General Thaddeus Ross").isNull();

        Host thor = new Host();
        thor.setApp("Thor");
        thor.setId(createNewId());
        thor.setOs("Solaris 8");

        // Create or update a new entity thor
        // Try creating an existing entity in thanos
        // Delete hulk which is already present
        BulkUpdateRequest transactionalBulkUpdateRequest = new BulkUpdateRequest(createNewId())
                .createOrUpdate(Host.class.getSimpleName(), thor)
                .create(thanos)
                .delete(hulk);

        // Bulk update in default transactional mode
        // Creation of entity thanos failed so the entire transaction is rolled back; no update happens.
        // Check examples in nonTransactionalBulkOperations() to see how the same works in non-transactional mode
        assertThatThrownBy(() -> this.dataFabric.bulkUpdate(transactionalBulkUpdateRequest))
                .isInstanceOf(DataFabricClientException.class)
                .hasFieldOrPropertyWithValue("bulkOperationIndex", 1);
        assertThat(this.dataFabric.get(Host.class, thor.getId())).isNull();
        assertThat(this.dataFabric.get(Cluster.class, hulk.getId())).isNotNull();
    }

    public void nonTransactionalBulkOperations() {
        Cluster hulk = new Cluster();
        hulk.setName("hulk");
        hulk.setOrganization(new Cluster.Organization("org-id-hulk", "Bruce"));
        hulk.setVersion(0);
        String id = this.dataFabric.create(hulk).orElseThrow(IllegalStateException::new);
        hulk.setId(id);
        hulk.setVersion(1);

        Host thanos = new Host();
        thanos.setApp("Thanos");
        thanos.setId(createNewId());
        thanos.setOs("Solaris 8");

        Host thor = new Host();
        thor.setApp("Thor");
        thor.setId(createNewId());
        thor.setOs("Solaris 8");
        this.dataFabric.create(thor);

        //Bulk update in non-transactional mode when all operations succeed
        BulkUpdateRequest bulkUpdateRequest = new BulkUpdateRequest(createNewId())
                .transactional(false)
                .create(thanos)
                .delete(thor);
        BulkUpdateResponse response = this.dataFabric.bulkUpdate(bulkUpdateRequest);
        assertThat(response.getStatus()).isEqualTo("success");
        List<BulkUpdateResponse.CreateResult> createResults = response.getCreateResults();
        assertThat(createResults.get(0).isSuccess()).isTrue();
        List<BulkUpdateResponse.DeleteResult> deleteResults = response.getDeleteResults();
        assertThat(deleteResults.get(0).isDeleted()).isTrue();
        assertThat(deleteResults.get(0).isSuccess()).isTrue();

        // Bulk update in non-transactional mode
        // Creation of entity thanos failed but other operations are executed and committed
        // i.e creates thor and deletes hulk
        // Check examples in transactionalBulkOperations() to see how the same works in transactional mode
        bulkUpdateRequest = new BulkUpdateRequest(createNewId())
                .transactional(false)
                .createOrUpdate(Host.class.getSimpleName(), thor)
                .create(thanos)
                .delete(hulk);
        response = this.dataFabric.bulkUpdate(bulkUpdateRequest);

        //Response status is failed since one of the operations failed
        assertThat(response.getStatus()).isEqualTo("failed");
        createResults = response.getCreateResults();
        assertThat(createResults.get(0).isSuccess()).isFalse();
        List<BulkUpdateResponse.CreateOrUpdateResult> createOrUpdateResults = response.getCreateOrUpdateResults();
        assertThat(createOrUpdateResults.get(0).isSuccess()).isTrue();
        deleteResults = response.getDeleteResults();
        assertThat(deleteResults.get(0).isDeleted()).isTrue();
        assertThat(deleteResults.get(0).isSuccess()).isTrue();

        assertThat(this.dataFabric.get(Host.class, thor.getId())).isNotNull();
        assertThat(this.dataFabric.get(Cluster.class, hulk.getId())).isNull();
    }

    private void optimisticLockingExample(RetryableUpdate retryableUpdate) {
        VCluster clusterOne = createVCluster("cluster-one");
        VCluster clusterTwo = createVCluster("cluster-two");

        // example how to do consecutive updates
        for (int i = 0; i < 5; i++) {
            // assign to clusterOne the result of the update method in order to get
            // the incremented version and avoid failing with DataFabricClientOptimisticLockingException
            // during the next update
            clusterOne = this.dataFabric.update(clusterOne);
            assertThat(clusterOne.getVersion()).as("optimistic locking version is not incremented for clusterOne.")
                    .isEqualTo(i + 1);

            BulkUpdateResponse bulkUpdateResponse = this.dataFabric.bulkUpdate(
                new BulkUpdateRequest(createNewId()).update(clusterTwo));
            BulkUpdateResponse.UpdateResult updateResult = bulkUpdateResponse.getUpdateResults().get(0);
            // assign to clusterTwo the result of the update operation in order to get
            // the incremented version and avoid failing with DataFabricClientOptimisticLockingException
            // during the next update
            clusterTwo = (VCluster) updateResult.getUpdatedEntity();
            assertThat(clusterTwo.getVersion()).as("optimistic locking version is not incremented for clusterTwo.")
                    .isEqualTo(i + 1);
        }

        // example how to retry failed update because of DataFabricClientOptimisticLockingException
        String clusterOneId = clusterOne.getId();
        retryableUpdate.update(() -> {
            VCluster cluster = this.dataFabric.get(VCluster.class, clusterOneId);
            cluster.setName(cluster.getName() + "updated");
            this.dataFabric.update(cluster);
        });

        this.dataFabric.delete(this.dataFabric.get(VCluster.class, clusterOne.getId()));
        assertThat(this.dataFabric.get(VCluster.class, clusterOne.getId())).as("clusterOne must be deleted").isNull();

        this.dataFabric.delete(this.dataFabric.get(VCluster.class, clusterTwo.getId()));
        assertThat(this.dataFabric.get(VCluster.class, clusterTwo.getId())).as("clusterTwo must be deleted").isNull();
    }

    private void jsonSearch() {
        /* NOTE - JSON searches are very expensive for medium-large tables (over 10k rows)
         * and lead to high resource consumption on the DB and may impact other services.
         * Make sure to add indexes on fields you want to search on. Searching on non-indexed
         * JSON fields will may lead to high latency.
         */
        Slice<Host> hosts = this.dataFabric.search(Host.class,
                SearchRequestBuilder.builder()
                        .filter(new Filter(Criterion.lessThan("metadata.applicable_scope.rollout_percent", 101)))
                        .paginate(0, 2)
                        .buildSearchRequest());
        log.info("hosts less than 101 rollout {}", hosts.get().collect(toList()));
        assertThat(hosts.get().count()).isEqualTo(2);
        hosts = this.dataFabric.search(Host.class,
                SearchRequestBuilder.builder()
                        .filter(new Filter(Criterion.greaterThan("metadata.applicable_scope.rollout_percent", 101)))
                        .paginate(0, 2)
                        .buildSearchRequest());
        log.info("hosts greater than 101 rollout {}", hosts.get().collect(toList()));
        hosts = this.dataFabric.search(Host.class,
                SearchRequestBuilder.builder()
                        .filter(new Filter(Criterion.equals("metadata.state", "ACTIVE")))
                        .paginate(0, 2)
                        .buildSearchRequest());
        log.info("hosts state active {}", hosts.get().collect(toList()));
        assertThat(hosts.get().count()).isEqualTo(1);
        hosts = this.dataFabric.search(Host.class,
                SearchRequestBuilder.builder()
                        .filter(new Filter(Criterion.notEquals("metadata.state", "ACTIVE")))
                        .paginate(0, 2)
                        .buildSearchRequest());
        log.info("hosts state not active {}", hosts.get().collect(toList()));
        assertThat(hosts.get().count()).isEqualTo(0);
        hosts = this.dataFabric.search(Host.class,
                SearchRequestBuilder.builder()
                        .filter(new Filter(Criterion.notIn("metadata.state", Arrays.asList("INACTIVE", "DELETED"))))
                        .paginate(0, 2)
                        .buildSearchRequest());
        log.info("hosts state not in INACTIVE {}", hosts.get().collect(toList()));
        assertThat(hosts.get().count()).isEqualTo(1);
        hosts = this.dataFabric.search(Host.class,
                SearchRequestBuilder.builder()
                        .filter(new Filter(Criterion.isNull("metadata.approvers")))
                        .paginate(0, 2)
                        .buildSearchRequest());
        log.info("hosts approvers null {}", hosts.get().collect(toList()));
        assertThat(hosts.get().count()).isEqualTo(2);
        hosts = this.dataFabric.search(Host.class,
                SearchRequestBuilder.builder()
                        .filter(new Filter(Criterion.isNotNull("metadata.namespace")))
                        .paginate(0, 2)
                        .buildSearchRequest());
        log.info("hosts namespace not null {}", hosts.get().collect(toList()));
        assertThat(hosts.get().count()).isEqualTo(2);
        hosts = this.dataFabric.search(Host.class,
                SearchRequestBuilder.builder()
                        .filter(new Filter(Criterion.isNotNull("metadata.approvers")))
                        .paginate(0, 2)
                        .buildSearchRequest());
        log.info("hosts approvers not null {}", hosts.get().collect(toList()));
        assertThat(hosts.get().count()).isEqualTo(0);
        hosts = this.dataFabric.search(Host.class,
                SearchRequestBuilder.builder()
                        .filter(new Filter(Criterion.contains("metadata.applicable_scope.allowed_orgs", "439d2c5a-b5db-4ce1-bc15-b713470f53ac")))
                        .paginate(0, 2)
                        .buildSearchRequest());
        log.info("hosts contains allowed orgs {}", hosts.get().collect(toList()));
        assertThat(hosts.get().count()).isEqualTo(1);
    }

    private void secretEncryptionExample() {
        String pass1 = "foo";
        String pass2 = "bar";
        Credentials credentialsOne = createCredentials("creds-1", pass1);
        Credentials credentialsTwo = createCredentials("creds-2", pass2);

        this.dataFabric.create(credentialsOne).ifPresent(credentialsOne::setId);
        this.dataFabric.create(credentialsTwo).ifPresent(credentialsTwo::setId);
        // Fields of type encrypted will be encrypted before being sent to the server for persistence
        assertThat(credentialsOne.getPassword()).isNotEqualTo(pass1);
        assertThat(credentialsTwo.getPassword()).isNotEqualTo(pass2);

        Credentials credentials = this.dataFabric.get(Credentials.class, credentialsOne.getId());
        assertThat(credentials.getPassword()).isNotEqualTo(pass1);
        // Entity returned with encrypted fields is decrypted
        this.dataFabric.decryptEncryptedFields(credentials);
        assertThat(credentials.getPassword()).isEqualTo(pass1);

        // By default decrypt is false and would return the encrypted data
        List<Credentials> credentialsResult = this.dataFabric.search(Credentials.class,
                                                    SearchRequestBuilder.builder()
                                                        .filter(new Filter(Criterion.equals("username", credentialsOne.getUsername())))
                                                        .buildSearchRequest()).getContent();
        assertThat(credentialsResult).hasSize(1);
        assertThat(credentialsResult.get(0).getPassword()).isNotEqualTo(pass1);
        this.dataFabric.decryptEncryptedFields(Credentials.class, credentialsResult);
        assertThat(credentialsResult.get(0).getPassword()).isEqualTo(pass1);

        credentialsResult = this.dataFabric.search(Credentials.class,
                                              SearchRequestBuilder.builder()
                                                      .decrypt(true)
                                                      .filter(new Filter(Criterion.equals("username", credentialsTwo.getUsername())))
                                                      .buildSearchRequest()).getContent();
        assertThat(credentialsResult).hasSize(1);
        assertThat(credentialsResult.get(0).getPassword()).isEqualTo(pass2);
    }

    private void authenticateAs(String username) {
        CspSecurityContextBuilder.builder().withUserName(username).withOrgId("orgId").login();
    }

    private VCluster createVCluster(String name) {
        VCluster cluster = new VCluster();
        cluster.setName(name);
        cluster.setId(this.dataFabric.create(cluster).orElseThrow(RuntimeException::new));
        return cluster;
    }

    private Credentials createCredentials(String username, String password) {
        Credentials credentials = new Credentials();
        credentials.setUsername(username);
        credentials.setOrgId(createNewId());
        credentials.setPassword(password);
        return credentials;
    }

    /**
     * Use {@link Retryable} with optimistic locking mainly for background workflows. For these background workflows
     * make sure you retry in such a way that the parameters are fully re-evaluated before an update is made. This
     * should not be used for user facing apis, ultimately users should retry their own operations based on updated
     * inputs
     * <p>
     * Alternative of using {@link Retryable} is to use {@link org.springframework.retry.support.RetryTemplate}
     */
    @Slf4j
    static class RetryableUpdate {

        @Retryable(include = {DataFabricClientOptimisticLockingException.class},
            backoff = @Backoff(delay = 500, maxDelay = 30000, multiplier = 4, random = true),
            maxAttempts = 4)
        public void update(Runnable runner) {
            runner.run();
        }

        @Recover
        private void recover(Exception e, String clusterId) {
            // callback method that will be called if the updateCluster fails more than 'maxAttempts' times
            // put here any logic that will handle this case
            log.error(
                "uh oh, something has failed after several retries, here is the error: '{}' - how do i want to handle this?",
                e.getMessage(), e);
        }
    }

    @Configuration(proxyBeanMethods = false)
    static class MyConfiguration {
        @Bean
        RetryableUpdate retryableUpdate() {
            return new RetryableUpdate();
        }

        @Bean
        CspClient createCspClient() {
            CspClient cspClient = mock(CspClient.class);
            CspAuthToken serviceToken = mock(CspAuthToken.class);
            when(cspClient.getCspClientCredentials(null)).thenReturn(serviceToken);
            when(cspClient.getCspClientCredentials()).thenReturn(serviceToken);
            String serviceJwt = JwtTestUtils.getServiceJwt("test-service", "org_id");
            when(serviceToken.getAccessToken()).thenReturn(serviceJwt);
            return cspClient;
        }
    }
}
