/*
 * Copyright (c) 2019 VMware, Inc. All rights reserved. VMware Confidential
 */

package com.vmware.atlas.testworker.integration.examples.atlasauth;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.vmware.atlas.testworker.Constants;
import com.vmware.atlas.testworker.configuration.TesterConfiguration;
import com.vmware.atlas.testworker.examples.ServiceResource;
import com.vmware.saas.testfw.api.StfRestClient;
import com.vmware.saas.testfw.common.annotations.StfTest;
import com.vmware.saas.testfw.common.annotations.TestCaseIdentifier;
import com.vmware.saas.testfw.common.exceptions.ConfigException;
import com.vmware.saas.testfw.common.utilities.DataProviderUtils;
import com.vmware.saas.testfw.common.utilities.FrameworkUtils;
import com.vmware.saas.testfw.commons.csp.CspUtils;

import lombok.extern.slf4j.Slf4j;


/**
 * Integration test for testing CRUD operations on a resource protected by authorization.
 */
@Slf4j
@StfTest
public class ProtectedResourceITest extends AbstractTestNGSpringContextTests {

    @Autowired
    @Qualifier(value = "cspTokenIntercepted")
    private StfRestClient stfRestClient;

    @Autowired
    @Qualifier(value = "default")
    private StfRestClient stfDefaultRestClient;

    @Autowired
    @Qualifier(value = "cspTokenInterceptedOperatorRo")
    private StfRestClient stfRestClientRoleRo;

    @Autowired
    @Qualifier(value = "cspTokenInterceptedCustom")
    private StfRestClient stfRestClientCustom;

    /**
     * TODO will read from the Framework
     * Basic functional test configuration.
     */
    @Autowired
    private TesterConfiguration testConfig;

    /**
     * Parses the JSON file (src/test/resources/testdata/json/*.json) as a 2D Object array for TestNg dataprovider
     * usage.<br>
     * <pre>
     *   <i>Array Of Objects mapped to a user defined type:</i>
     *   {
     *   "TC001": {
     *     "datasets": [
     *       {
     *         "id": "123-1",
     *         "name": "ResourceName 123-1"
     *       },
     *       {
     *         "id": "123-2",
     *         "name": "ResourceName 123-2"
     *       }
     *     ]
     *   },
     *   "TC002": {
     *     "datasets": [
     *       {
     *         "id": "123-3",
     *         "name": "ResourceName 123-3"
     *       }
     *     ]
     *   }
     * }
     *
     * </pre>
     *
     * @param method the method using reflection
     * @return the object [ ] [ ]
     * @throws ConfigException the config exception
     */
    @DataProvider(name = "createSampleModel")
    public Object[][] provider(Method method) throws ConfigException {
        return DataProviderUtils.getData(method, "testdata/json/sample-td.json");
    }

    /**
     * Test that verifies CRUD operations on a protected resource. Since its using TestCase TC001 it has datasets with
     * two test values, so the test will run twice once with value "id": "123-1" and another for "id": "123-2"
     *
     * @param testData the test data
     * @throws IOException the io exception
     */
    @Test(dataProvider = "createSampleModel", groups = {
            "smoke"}, description = "Verify CRUD operations on a protected resource")
    @TestCaseIdentifier(uid = "TC001")
    public void testServiceResourceCRUDWithAuth(String testData) throws IOException {
        log.info("TestCase1. testServiceResourceCRUDWithAuth : testConfig.isDisableAuth() === "+testConfig.isDisableAuth());
        if (!testConfig.isDisableAuth()) {
            //Read the value from the testdata file using the uid="TC001"
            ServiceResource postRequestData = FrameworkUtils.fromJson(testData, ServiceResource.class);
            // We can either get the values for the testdata from the the framework as above or set value manually
            // as below.
            postRequestData.setId(UUID.randomUUID().toString());
            postRequestData.setName("testServiceResourceCRUDWithAuth");

            // Create
            ResponseEntity<ServiceResource> response = stfRestClient
                    .exchange(Constants.BASE_URI_AUTH.concat(Constants.SAMPLE), HttpMethod.POST,
                              postRequestData, ServiceResource.class);
            Assert.assertEquals(response.getStatusCode(), HttpStatus.OK, "ServiceResource not created");
            Assert.assertEquals(postRequestData.getName(), response.getBody().getName());

            String resourceUrl = Constants.BASE_URI_AUTH.concat(Constants.SAMPLE).concat("/")
                    .concat(response.getBody().getId().toString());
            // Read
            ResponseEntity<ServiceResource> getSampleResponse =
                    stfRestClient.exchange(resourceUrl, HttpMethod.GET, "", ServiceResource.class);
            Assert.assertEquals(response.getBody().getId(), getSampleResponse.getBody().getId(), "ID does not matched");
            Assert.assertEquals(postRequestData.getName(), getSampleResponse.getBody().getName(),
                                "ServiceResource Name does not matched");

            // Delete
            ResponseEntity<ServiceResource> deleteResourceResponse =
                    stfRestClient.exchange(resourceUrl, HttpMethod.DELETE, "", ServiceResource.class);
            Assert.assertEquals(deleteResourceResponse.getStatusCode(), HttpStatus.OK,
                                "ServiceResource Id " + response
                                        .getBody().getId() + " + not deleted");
        }
    }

    /**
     * Verify that protected resource cannot be created without authorization. Since its using TestCase TC002 it has
     * datasets with one test values, so the test will run only once with value "id": "123-3"
     *
     * @param testData the test data
     * @throws IOException the io exception
     */
    @Test(dataProvider = "createSampleModel", groups = {
            "smoke"}, description = "Verify that protected resource cannot be created without authorization")
    @TestCaseIdentifier(uid = "TC002")
    public void testServiceResourceCreateWithoutAuth(String testData) throws IOException {
        log.info("TestCase2. testServiceResourceCreateWithoutAuth : testConfig.isDisableAuth() === "+testConfig.isDisableAuth());
        if (!testConfig.isDisableAuth()) {
            //Read the value from the testdata file using the uid="TC002"
            ServiceResource postRequestData = FrameworkUtils.fromJson(testData, ServiceResource.class);
            // We can either get the values for the testdata from the the framework as above or set value manually
            // as below.
            postRequestData.setId(UUID.randomUUID().toString());
            postRequestData.setName("testServiceResourceCreateWithoutAuth");
            // Post API call to disable Auth , so passing empty header sampleHeader without bearer token.
            Map<String, String> sampleHeader = new HashMap<>();
            sampleHeader.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

            ResponseEntity<ServiceResource> response = stfDefaultRestClient
                    .exchange(Constants.BASE_URI_AUTH.concat(Constants.SAMPLE),
                              sampleHeader,
                              postRequestData,
                              HttpMethod.POST,
                              ServiceResource.class);
            Assert.assertEquals(response.getStatusCode(), HttpStatus.UNAUTHORIZED, "SampleModel not created");
        }

    }

    /**
     * Verify that User with Operator RO role should not be allowed to do Post operation as defined in the Policy file.
     * Since its using TestCase TC002 it has
     * datasets with one test values, so the test will run only once with value "id": "123-3"
     *
     * @param testData the test data
     * @throws IOException the io exception
     */
    @Test(dataProvider = "createSampleModel", groups = {
            "smoke"}, description = "Verify that protected resource cannot be created without authorization")
    @TestCaseIdentifier(uid = "TC002")
    public void testServiceResourceCreateWithAuthRoleOperatorRo(String testData) throws IOException {
        log.info("TestCase3. testServiceResourceCreateWithAuthRoleOperatorRo : testConfig.isDisableAuth() === "+testConfig.isDisableAuth());
        if (!testConfig.isDisableAuth()) {
            //Read the value from the testdata file using the uid="TC002"
            ServiceResource postRequestData = FrameworkUtils.fromJson(testData, ServiceResource.class);
            // We can either get the values for the testdata from the the framework as above or set value manually
            // as below.
            postRequestData.setId(UUID.randomUUID().toString());
            postRequestData.setName("testServiceResourceCreateWithAuthRoleOperatorRo");

            // WE can either use SaasTestFw bean stfRestClientRoleRo to set Rolebase Operator token .
            ResponseEntity<ServiceResource> response1 = stfRestClientRoleRo
                    .exchange(Constants.BASE_URI_AUTH.concat(Constants.SAMPLE), HttpMethod.POST,
                              postRequestData, ServiceResource.class);
            Assert.assertEquals(response1.getStatusCode(), HttpStatus.FORBIDDEN, "ServiceResource not created");

            // OR we can either use SaasTestFw bean stfDefaultRestClient and set your token as below.
            // Post API call for Operate Role is not Authorized, So the test should give Forbidden error.
            Map<String, String> sampleHeader = new HashMap<>();
            sampleHeader.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            //Set the Refesh Token with Operate Ro role.
            String cspAuthToken = new CspUtils().getCspAuthToken(testConfig.getCspUrl(), testConfig.getCspRefreshTokenOperatorRo());
            sampleHeader.put(HttpHeaders.AUTHORIZATION, "Bearer ".concat(cspAuthToken));

            ResponseEntity<ServiceResource> response = stfDefaultRestClient
                    .exchange(Constants.BASE_URI_AUTH.concat(Constants.SAMPLE),
                              sampleHeader,
                              postRequestData,
                              HttpMethod.POST,
                              ServiceResource.class);
            Assert.assertEquals(response.getStatusCode(), HttpStatus.FORBIDDEN, "ServiceResource not created");
        }

    }

    /**
     * Verify that User with CustomToken should be allowed to do Post operation provided he has vmc-user:fullRole.
     * Since its using TestCase TC002 it has
     * datasets with one test values, so the test will run only once with value "id": "123-3"
     *
     * @param testData the test data
     * @throws IOException the io exception
     */
    @Test(dataProvider = "createSampleModel", groups = {
            "smoke"}, description = "Verify that protected resource cannot be created without authorization")
    @TestCaseIdentifier(uid = "TC002")
    public void testServiceResourceCreateWithCustomAuthToken(String testData) throws IOException {
        log.info("TestCase4. testServiceResourceCreateWithCustomAuthToken : testConfig.isDisableAuth() === "+testConfig.isDisableAuth());
        if (!testConfig.isDisableAuth()) {
            //Read the value from the testdata file using the uid="TC002"
            ServiceResource postRequestData = FrameworkUtils.fromJson(testData, ServiceResource.class);
            // We can either get the values for the testdata from the the framework as above or set value manually
            // as below.
            postRequestData.setId(UUID.randomUUID().toString());
            postRequestData.setName("testServiceResourceCreateWithCustomAuthToken");

            // WE can use SaasTestFw bean stfDefaultRestClient and set your token as below.
            // Post API call for Operate Role is not Authorized, So the test should give Forbidden error.
            Map<String, String> sampleHeader = new HashMap<>();
            sampleHeader.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            //Set the Refesh Token with Operate Ro role.
            String cspAuthToken = new CspUtils().getCspAuthToken(testConfig.getCspUrl(), testConfig.getCspRefreshTokenCustom());
            sampleHeader.put(HttpHeaders.AUTHORIZATION, "Bearer ".concat(cspAuthToken));

            ResponseEntity<ServiceResource> response = stfRestClientCustom
                    .exchange(Constants.BASE_URI_AUTH.concat(Constants.SAMPLE),
                              sampleHeader,
                              postRequestData,
                              HttpMethod.POST,
                              ServiceResource.class);
            Assert.assertEquals(response.getStatusCode(), HttpStatus.OK, "ServiceResource not created");
        }

    }

}
