/*
 * Copyright (c) 2019 VMware, Inc. All rights reserved. VMware Confidential
 */

package com.vmware.atlas.testworker.integration.examples.controller;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.vmware.atlas.testworker.Constants;
import com.vmware.atlas.testworker.examples.ServiceResource;
import com.vmware.saas.testfw.api.StfRestClient;
import com.vmware.saas.testfw.common.annotations.StfTest;
import com.vmware.saas.testfw.common.annotations.TestCaseIdentifier;
import com.vmware.saas.testfw.common.exceptions.ConfigException;
import com.vmware.saas.testfw.common.utilities.DataProviderUtils;
import com.vmware.saas.testfw.common.utilities.FrameworkUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * The type SampleResourceTest tests used to connect to the resource which doesnt need any authorization.
 */
@Slf4j
@StfTest
public class SampleResourceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    @Qualifier(value = "default")
    StfRestClient stfDefaultRestClient;

    /**
     * Parses the JSON file (src/test/resources/testdata/json/*.json) as a 2D Object array for TestNg dataprovider
     * usage.<br>
     * <pre>
     *   <i>Array Of Objects mapped to a user defined type:</i>
     *   {
     *   "TC001": {
     *     "datasets": [
     *       {
     *         "id": "123-1",
     *         "name": "ResourceName 123-1"
     *       },
     *       {
     *         "id": "123-2",
     *         "name": "ResourceName 123-2"
     *       }
     *     ]
     *   },
     *   "TC002": {
     *     "datasets": [
     *       {
     *         "id": "123-3",
     *         "name": "ResourceName 123-3"
     *       }
     *     ]
     *   }
     * }
     *
     * </pre>
     *
     * @param method the method using reflection
     * @return the object [ ] [ ]
     * @throws ConfigException the config exception
     */
    @DataProvider(name = "createSampleModel")
    public Object[][] provider(Method method) throws ConfigException {
        return DataProviderUtils.getData(method, "testdata/json/sample-td.json");
    }

    /**
     * Test that verifies CRUD operations on a resource without authorization. Since its using TestCase TC002 it has
     * datasets with one test values, so the test will run only once with value "id": "123-3"
     *
     * @param testData the test data
     * @throws IOException the io exception
     */
    @Test(dataProvider = "createSampleModel", groups = {
            "smoke"}, description = "Verify CRUD operations on a resource without authorization")
    @TestCaseIdentifier(uid = "TC002")
    public void createSampleModelTest(String testData) throws IOException {

        //Read the value from the testdata file using the uid="TC001"
        ServiceResource postRequestData = FrameworkUtils.fromJson(testData, ServiceResource.class);
        // Or set the value
        postRequestData.setId(UUID.randomUUID().toString());
        postRequestData.setName("myresourceB1");

        //Create Object
        ResponseEntity<ServiceResource> response = stfDefaultRestClient
                .exchange(Constants.BASE_URI.concat(Constants.SAMPLE),
                          HttpMethod.POST,
                          postRequestData,
                          ServiceResource.class);

        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK, "ServiceResource not created");
        Assert.assertEquals(postRequestData.getName(), response.getBody().getName());
        log.warn("code " + response.getStatusCode() + " : " + response.getBody().getName());

        String resourceUrl = Constants.BASE_URI.concat(Constants.SAMPLE).concat("/").
                concat(response.getBody().getId().toString());

        // Read Object
        ResponseEntity<ServiceResource> getSampleResponse =
                stfDefaultRestClient.exchange(resourceUrl, HttpMethod.GET,"", ServiceResource.class);
        Assert.assertEquals(response.getBody().getId(), getSampleResponse.getBody().getId(),
                            "ID does not matched");
        Assert.assertEquals(postRequestData.getName(), getSampleResponse.getBody().getName(),
                            "ServiceResource Name does not matched");

        // Delete Object
        ResponseEntity<ServiceResource> deleteServiceResourceResponse =
                stfDefaultRestClient.exchange(resourceUrl, HttpMethod.DELETE,"", ServiceResource.class);
        Assert.assertEquals(deleteServiceResourceResponse.getStatusCode(), HttpStatus.OK,
                            "ServiceResource Id " + response
                                    .getBody().getId() + " + not deleted");
    }


}
