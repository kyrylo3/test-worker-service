/*
 * Copyright (c) 2019 VMware, Inc. All rights reserved. VMware Confidential
 */
package com.vmware.atlas.testworker.integration.examples.orchestrationworker;

import java.lang.reflect.Method;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vmware.atlas.scheduler.service.client.api.TaskControllerApi;
import com.vmware.atlas.scheduler.service.model.Task;
import com.vmware.atlas.scheduler.service.model.TaskInput;
import com.vmware.atlas.testworker.Constants;
import com.vmware.saas.testfw.common.annotations.StfTest;
import com.vmware.saas.testfw.common.annotations.TestCaseIdentifier;
import com.vmware.saas.testfw.common.exceptions.ConfigException;
import com.vmware.saas.testfw.common.utilities.DataProviderUtils;

import lombok.extern.slf4j.Slf4j;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * SampleWorkflowFunctionalTest.
 *
 * @author anagarwal
 */
@Slf4j
@StfTest
public class SampleWorkflowTest extends AbstractTestNGSpringContextTests {

    /**
     * TaskControllerApi sdk.
     */
    @Autowired
    public TaskControllerApi taskControllerApi;

    public static final String TEST_DATA_JSON_DEPLOYMENT_GROUPS =
            Constants.TEST_DATA_JSON + "/task.json";
    public static final int MAX_RETRY_ATTEMPTS = 30;


    /**
     * Reads the testData JSON in a format specified by Test Saas framework.
     *
     * @param method the method using reflection
     * @return the json object array
     * @throws ConfigException in case json file could not be read or parsed
     */
    @DataProvider(name = "getTaskData")
    public Object[][] provider(Method method) throws ConfigException {
        return DataProviderUtils.getData(method, TEST_DATA_JSON_DEPLOYMENT_GROUPS);
    }

    /**
     * Test to verify if Sample SDDC provisioning workflow can be successfully completed.
     *
     * @param sampleTaskJSONString sample task JSON string
     * @throws Exception the * exception in case sampleTask data json can't be parsed
     */

    @Test(dataProvider = "getTaskData", groups = {
            "smoke"}, description = "Verify sample SDDC provisioning workflow can be successfully completed")
    @TestCaseIdentifier(uid = "TC001")
    public void verifyWorkflowExecution(String sampleTaskJSONString) throws Exception {
        TaskInput sampleTask = new ObjectMapper().readValue(sampleTaskJSONString, TaskInput.class);

        Task createdTask = taskControllerApi.createTask(sampleTask);

        assertThat(createdTask.getId()).isNotNull();
        Task task = taskControllerApi.getTask(createdTask.getId(), false, false, false, false);
        assertThat(task).isNotNull();

        int retryCount = 0;
        Task.StatusEnum statusEnum = task.getStatus();
        while (retryCount <= MAX_RETRY_ATTEMPTS && statusEnum != Task.StatusEnum.COMPLETED
               && statusEnum != Task.StatusEnum.FAILED) {
            log.info("retryCount is:" + retryCount + " Task status is: " + task.getStatus() + " task: " + task);

            statusEnum = Task.StatusEnum.valueOf(taskControllerApi.getTaskStatus(createdTask.getId()));
            Thread.sleep(3 * 1000);

            ++retryCount;
        }

        task = taskControllerApi.getTask(createdTask.getId(), false, false, false, false);
        log.info("Last retryCount is:" + retryCount + " Task status is: " + task.getStatus() + " task: " + task);

        assertThat(statusEnum).isEqualTo(Task.StatusEnum.COMPLETED);
    }
}
