/*
 * Copyright (c) 2019 VMware, Inc. All rights reserved. VMware Confidential
 */

package com.vmware.atlas.testworker.performance.examples;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.vmware.atlas.testworker.Constants;
import com.vmware.atlas.testworker.examples.ServiceResource;
import com.vmware.saas.testfw.api.StfRestClient;
import com.vmware.saas.testfw.common.annotations.StfTest;
import com.vmware.saas.testfw.common.annotations.TestCaseIdentifier;
import com.vmware.saas.testfw.common.exceptions.ConfigException;
import com.vmware.saas.testfw.common.utilities.DataProviderUtils;
import com.vmware.saas.testfw.common.utilities.FrameworkUtils;
import com.vmware.saas.testfw.perf.PerfListeners;
import com.vmware.saas.testfw.perf.impl.task.LocustTask;

import lombok.extern.slf4j.Slf4j;

/**
 * The type load tests.
 */
@Slf4j
@StfTest
@Listeners(PerfListeners.class)
public class SampleLoadTests extends AbstractTestNGSpringContextTests {

    /**
     * The Stf rest client.
     */
    @Autowired
    @Qualifier(value = "cspTokenIntercepted")
    StfRestClient stfRestClient;

    @Autowired
    private LocustTask locustTask;

    /**
     * Parses the JSON file (src/test/resources/testdata/json/*.json) as a 2D Object array for TestNg dataprovider
     * usage.<br>
     * <pre>
     *   <i>Array Of Objects mapped to a user defined type:</i>
     *   {
     *   "TC001": {
     *     "datasets": [
     *       {
     *         "id": "123-1",
     *         "name": "ResourceName 123-1"
     *       },
     *       {
     *         "id": "123-2",
     *         "name": "ResourceName 123-2"
     *       }
     *     ]
     *   },
     *   "TC002": {
     *     "datasets": [
     *       {
     *         "id": "123-3",
     *         "name": "ResourceName 123-3"
     *       }
     *     ]
     *   }
     * }
     *
     * </pre>
     *
     * @param method the method using reflection
     * @return the object [ ] [ ]
     * @throws ConfigException the config exception
     */
    @DataProvider(name = "createSampleModel")
    public Object[][] provider(Method method) throws ConfigException {
        return DataProviderUtils.getData(method, "testdata/json/sample-td.json");
    }

    /**
     * Sample service get sample service load test.
     *
     * @param sampleTestData the sample test data
     * @throws IOException the io exception
     */
    @Test(dataProvider = "createSampleModel", description = "Past call load test.")
    @TestCaseIdentifier(uid = "TC001")
    public void sampleServiceGetSampleServiceLoadTest(String sampleTestData) throws IOException {
        //Read the value from the testdata file using the uid="TC001"
        ServiceResource postRequestData = FrameworkUtils.fromJson(sampleTestData, ServiceResource.class);
        // Or set the value
        postRequestData.setId(UUID.randomUUID().toString());
        postRequestData.setName("myresource1");

        // Create
        ResponseEntity<ServiceResource> response = stfRestClient
                .exchange(Constants.BASE_URI.concat(Constants.SAMPLE), HttpMethod.POST,
                          postRequestData, ServiceResource.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK, "ServiceResource not created");
        Assert.assertEquals(postRequestData.getName(), response.getBody().getName());
        log.warn("code " + response.getStatusCode() + " : " + response.getBody().getName());

        String resourceUrl = Constants.BASE_URI.concat(Constants.SAMPLE).concat("/")
                .concat(response.getBody().getId().toString());

        LocustTask getTask = locustTask
                .create(resourceUrl, "", 10, "Get Sample Service",
                        HttpMethod.GET);
        PerfListeners.getWeighingTaskSet().addTask(getTask);
    }

}
