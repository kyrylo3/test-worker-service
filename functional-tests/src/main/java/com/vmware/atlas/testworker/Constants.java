/*
 * Copyright (c) 2019 VMware, Inc. All rights reserved. VMware Confidential
 */

package com.vmware.atlas.testworker;


/**
 * The type Constants for API Path and others use by testclasses.
 */
public class Constants {

    /**
     * The constant BASE_URI.
     */
    public static final String BASE_URI = "/api/noauth";

    /**
     * The constant BASE_URI.
     */
    public static final String BASE_URI_AUTH = "C:/Program Files/Git/api/testworker";

    /**
     * The constant sample.
     */
    public static final String SAMPLE = "/sample-resources";

    /* common file path for TestNg Data Provider files */
    public static final String TEST_DATA_JSON = "testdata/json";
}
