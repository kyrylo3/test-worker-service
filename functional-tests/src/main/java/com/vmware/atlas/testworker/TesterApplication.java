/*
 * Copyright (c) 2019 VMware, Inc. All rights reserved. VMware Confidential
 */

package com.vmware.atlas.testworker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

import com.vmware.atlas.scheduler.service.annotation.EnableSchedulerConfiguration;
import com.vmware.atlas.testworker.configuration.TesterConfiguration;
import com.vmware.saas.testfw.commons.annotations.EnableSaasTestFw;

/**
 * A bootstrap spring application for enabling all configuration annotations for integration testing.
 * We don't run this application directly; instead we run the tests from gradle - "gradlew -PrunFt functional-tests:build"
 */
@EnableSchedulerConfiguration
@EnableSaasTestFw
@EnableConfigurationProperties(TesterConfiguration.class)
@SpringBootApplication(scanBasePackages = {"com.vmware.saas.testfw.", "com.vmware.atlas.testworker"})
public class TesterApplication {
    public static void main(String[] args) {
        SpringApplication.run(TesterApplication.class, args);
    }
}
