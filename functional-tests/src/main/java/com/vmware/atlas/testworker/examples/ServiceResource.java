/*
 * Copyright (c) 2019 VMware, Inc. All rights reserved. VMware Confidential
 */

package com.vmware.atlas.testworker.examples;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This is the service resource that the integration test interacts with. We use a different representation instead of
 * reusing SampleResource class from service to maintain the separation in integration tests. In real life use cases,
 * integration tests should use model classes defined in the service SDK.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceResource {
    String id;
    String name;
}
