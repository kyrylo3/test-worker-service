/*
 * Copyright (c) 2019 VMware, Inc. All rights reserved. VMware Confidential
 */

package com.vmware.atlas.testworker.configuration;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * Basic integration test configuration.
 */
@Data
@ConfigurationProperties(prefix = "functional-test")
public class TesterConfiguration {

    @NotNull
    private String serviceUrl;

    @NotNull
    private String dataFabricUrl;

    @NotNull
    private String repoRoot;

    @NotNull
    private String cspUrl;

    @NotNull
    private String cspRefreshToken;

    private String cspRefreshTokenOperatorRo;

    private String cspRefreshTokenOperatorRw;

    private String cspRefreshTokenCustom;

    @NotNull
    private boolean disableAuth;

    @NotNull
    public PerformanceTest performanceTest;

    /**
     * Performance tests properties.
     */
    @Data(staticConstructor = "of")
    public static class PerformanceTest {
        @NotNull
        private String masterHost;

        @NotNull
        private int masterPort;

        @NotNull
        private int clients;

        @NotNull
        private int hatchRate;

        @NotNull
        private String runTime;

        @NotNull
        private String expectSlaves;

        @NotNull
        private String csvFileName;

        @NotNull
        private Boolean web;

        @NotNull
        private String locustMode;
    }

}
